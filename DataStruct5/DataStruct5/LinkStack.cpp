#include<iostream>
using namespace std;
#define OK 1
#define ERROR 0
#define OVERFLOW -2
typedef int Status;

typedef int SElemType;
typedef struct StackNode
{
	SElemType data;
	struct StackNode* next;
}StackNode, *LinkStack;

//初始化
Status InitStack(LinkStack& S)
{
	//让栈顶指针指向NULL即可
	S = NULL;
	return OK;
}

//清空栈
Status ClearStack(LinkStack& S)
{
	//创建一个临时指针，遍历该链表后依次释放各个节点
	StackNode* p;
	while (S)
	{
		p = S;
		S = S->next;
		delete p;
	}
	return OK;
}


//判空
Status StackEmpty(LinkStack S)
{
	return S == NULL;
}

//销毁
Status DestroyStack(LinkStack& S)
{
	ClearStack(S);
	S = NULL;
	return OK;
}

//入栈
Status Push(LinkStack& S, SElemType e)
{
	//在栈顶位置插入元素e
	StackNode* p = new StackNode;
	p->data = e;
	p->next = S;  //将新结点插入栈顶
	S = p;        //修改栈顶指针为p
	return OK;
}

//出栈
Status Pop(LinkStack &S, SElemType& e)
{
	//删除栈顶元素，并返回该元素
	if (S == NULL)
		return ERROR;
	StackNode * p = S;
	e = p->data;
	S = S->next;
	delete p;
	return OK;
}

//获取栈顶元素
SElemType GetTop(LinkStack S)
{
	//返回S的栈顶元素，并不改变栈顶指针的位置
	if (S != NULL)
	{
		return S->data;
	}
}

// 遍历栈并打印
void StackTraverse(LinkStack S) 
{
	StackNode* p = S;
	while (p) 
	{
		printf("%d ", p->data);
		p = p->next;
	}
	printf("\n");
}

int main()
{

	LinkStack S;
	InitStack(S);
	int e;
	Push(S, 1);
	Push(S, 2);
	Push(S, 3);
	printf("现在栈内元素为(后进先出)：");
	StackTraverse(S);
	printf("栈顶元素为：%d\n", GetTop(S));
	Pop(S, e);
	printf("现在栈内元素为(后进先出)：");
	StackTraverse(S);
	printf("弹出一个元素后，栈顶元素为：%d\n", GetTop(S));
	ClearStack(S);
	if (StackEmpty(S)) 
	{
		printf("清空栈后，栈为空\n");
	}
	else 
	{
		printf("清空栈后，栈不为空，证明有问题\n");
	}
	DestroyStack(S);
	return 0;
}
