#include<stdio.h>


#define KeyType int
//结构体
typedef struct node
{
	KeyType key;  //关键字类型
	struct node* lchild, * rchild;  //左右孩子指针
}BSTNode;

//二叉排序树的插入和创建
BSTNode* Insert(BSTNode* bt, KeyType k)
{
	//原树为空
	if (bt == NULL)
	{
		bt = (BSTNode*)malloc(sizeof(BSTNode));
		bt->key = k;
		bt->lchild = bt->rchild = NULL;
	}
	else if (k < bt->key)
	{
		bt->lchild = Insert(bt->lchild, k);  //插入左孩子中
	}
	else if (k > bt->key)
	{
		bt->rchild = Insert(bt->rchild, k); 
	}
	return bt;
}

BSTNode* CreatBST(KeyType a[], int n)
{
	BSTNode* bt = NULL;
	int i = 0;
	while (i < n)
	{
		bt = Insert(bt, a[i]);      //将关键字a[i]插入二叉排序树bt中
		i++;
	}
	return bt;    //返回建立的二叉排序树的根指针
}

//二叉排序树的查找
BSTNode* SearchBST(BSTNode* bt, KeyType k)  //递归算法：在bt中查找关键字为k的结点
{
	if (bt == NULL)
	{
		return bt;
	}
	if (bt->key == k)
	{
		return bt;
	}

	if (k < bt->key)
	{
		return SearchBST(bt->lchild, k);     //在左子树中递归查找
	}
	else
	{
		return SearchBST(bt->rchild, k);
	}
}


//二叉排序树的删除
BSTNode* DeletBST(BSTNode* bt, KeyType k)
{
	if (bt == NULL)
	{
		return bt;
	}

	BSTNode* p = bt;
	BSTNode* f = NULL;   //用来查找被删除节点
	//查找被删除结点，其双亲节点是f
	while (p != NULL)
	{
		if (p->key == k)
		{
			break;
		}
		f = p;
		if (k < p->key)
		{
			p = p->lchild;
		}
		else
		{
			p = p->rchild;
		}
	}
	//若查找后未找到该节点
	if (p == NULL)
	{
		return bt;    //返回bt
	}
	//否则就是找到被删除结点了
	//情况一：结点是叶子节点，直接删除
	if (p->lchild == NULL && p->rchild == NULL)
	{
		if (p == bt)  //结点p是根节点
		{
			bt == NULL;
		}
		else          //该结点不是根节点
		{
			if (f->lchild == p)  //那么需要把p的父节点置空
			{
				f->lchild == NULL;
			}
			else
			{
				f->rchild == NULL;
			}
		}
		free(p);
	}
	//情况二：结点p只有左子树无右子树，则需要把左子树移到该位置
	else if (p->rchild == NULL)
	{
		if (f = NULL)  //p节点没有双亲，即p节点为根节点 p == bt
		{
			bt = bt->lchild;
			//用bt的左孩子代替bt
		}
		else         //p结点存在双亲节点，即p节点不为根节点
		{
			if (f->lchild == p)  //如果p节点是节点f的左孩子
			{
				f->lchild = p->lchild;  //用节点p的左孩子代替节点p
			}
			else
			{
				f->rchild = p->lchild; //反之用左孩子代替p
			}
		}
		free(p);
	}
	//情况三：结点p只有右孩子无左子树，则需要把p的右子树移到该位置
	else if (p->lchild == NULL)
	{
		if (f == NULL)
		{
			bt = bt->rchild;
		}
		else
		{
			if (f->lchild == p)
			{
				f->lchild = p->rchild;
			}
			else
			{
				f->rchild = p->rchild;
			}
		}
		free(p);
	}
	//情况四：p结点同时有左子树和右子树
	else
	{
		BSTNode* q = p->lchild;  //q指向p的左子树，即用q寻找最大前驱
		f = p;  //f指向q的双亲节点
		while (q->rchild != NULL)  //一直在左子树中向右下寻找，找到最大值
		{
			f = q;
			q = q->rchild;
		}
		p->key = q->key;   //将p节点处的值用q节点的值代替
		if (q == f->lchild)  //删除节点q用节点q的左孩子代替节点q
		{
			f->lchild = q->lchild;
		}
		else
		{
			f->rchild = q->lchild;
		}
		free(p);
	}
	return bt;
}
