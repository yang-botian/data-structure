#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<stdbool.h>
#include<string.h>


typedef int HPDataType;
typedef struct Heap
{
	HPDataType* a;
	int size;
	int capacity;
}HP;

void HPInit(HP* php);
void HPInitArray(HP* php);

void HPDestory(HP* php);

//时间复杂度: O(logN)
void HPPush(HP* php, HPDataType x);

//时间复杂度: O(logN)
void HPPop(HP* php);//删除树根元素

HPDataType HPTop(HP* php);//取树根元素

bool HPEmpty(HP* php);

//上下调整时可以确定建大堆还是建小堆
//向上调整算法
void AdjustUp(HPDataType* a, int child);
//向下调整算法  O(logN)
void AdjustDown(HPDataType* a, int size, int parent);
void Swap(HPDataType* a, HPDataType* b);

//树高和数据数量之间的关系
//如果是满二叉树   N = 2^h - 1;   h = log(N + 1);
//如果是完全二叉树，最小为  N = 2^(h - 1);  h = logN + 1; 

//对于topK问题，二叉树具有很强的优越性