#include"Heap.h"

void HeapSort(int* a, int n)
{
	//直接对数组进行建堆
	// 时间复杂度: O(N)
	for (int i = (n - 1 - 1) / 2; i >= 0; i--)
	{
		AdjustDown(a, n, i);//从叶子的父亲结点开始调整
	}
	//建好堆之后进行排序
	//堆排序若是排升序，则要建大堆
	//堆排序若是排降序，则要建小堆
	// 时间复杂度:O(N*logN)
	int end = n - 1;
	while (end > 0)
	{
		Swap(&a[0], &a[end]);
		AdjustDown(a, end, 0);
		end--;
	}
}

int main()
{
	int b[] = { 50,100,70,65,60,32 };
	HeapSort(b, sizeof(b) / sizeof(int));
	return 0;
}

int main1()
{
	int a[6] = { 50,100,70,65,60,32 };

	HP hp;
	HPInitArray(&hp, a, sizeof(a) / sizeof(int));
	//HPInit(&hp);
	//for (int i = 0; i < sizeof(a) / sizeof(int); i++)
	//{
	//	HPPush(&hp, a[i]);
	//}
	//HPPop(&hp);
	while (!HPEmpty(&hp))
	{
		printf("%d ", HPTop(&hp));
		HPPop(&hp);
	}

	HPDestory(&hp);
	return 0;
}