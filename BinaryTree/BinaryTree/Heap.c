#include"Heap.h"

void HPInit(HP* php)
{
	assert(php);

	php->a = NULL;
	php->size = php->capacity = 0;
}

void HPInitArray(HP* php, HPDataType* a, int n)//给定数据，把数据都放入到已经创建好数组中，并且实现堆
{
	assert(php);

	//为指向的数组空间开辟空间
	php->a = (HPDataType*)malloc(n * sizeof(HPDataType));
	if (php->a == NULL)
	{
		perror("malloc fail");
		return;
	}
	//拷贝数据到数组中
	memcpy(php->a, a, sizeof(HPDataType) * n);
	php->capacity = php->size = n;
	//实现堆
	//向上调整，建堆的时间复杂度:  O(N*logN)
	//for (int i = 1; i < n; i++)
	//{
	//	//不断将数据向上调整
	//	AdjustUp(php->a, i);
	//}
	//向下调整，建堆的时间复杂度:    O(N)
	for (int i = (php->size - 1 - 1) / 2; i >= 0; i--)
	{
		AdjustDown(php->a, php->size, i);//从叶子的父亲开始调整
	}
}

void HPDestory(HP* php)
{
	assert(php);

	free(php->a);
	php->a = NULL;
	php->size = php->capacity = 0;
}

void Swap(HPDataType* a, HPDataType* b)
{
	HPDataType tmp = *a;
	*a = *b;
	*b = tmp;
}

//向上调整数据
void AdjustUp(HPDataType* a, int child)
{
	//寻找父亲树节点
	int parent = (child - 1) / 2;
	while (child > 0)//循环结束条件（即最坏情况），孩子结点到达根数位置
	{
		if (a[child] < a[parent])//如果孩子结点的数据小于父亲结点的数据，就交换
		{
			Swap(&a[child], &a[parent]);
			child = parent;
			parent = (child - 1) / 2;
		}
		else
		{
			break;
		}
	}
}

//向下调整数据
void AdjustDown(HPDataType* a, int size, int parent)//这次用来排序，选择建大堆
{
	//假设法寻找儿子结点
	int child = parent * 2 + 1;
	while (child < size)//子节点的下标大于该数组的数据大小
	{
		if (child + 1 < size && a[child + 1] > a[child])//如果旁边的子节点比假设的大，就更换子节点
		{
			child = parent * 2 + 2;
		}
		if (a[parent] < a[child])//父亲结点的值比儿子结点的值大，就交换
		{
			Swap(&a[parent], &a[child]);
			parent = child;
			child = parent * 2 + 1;
		}
		else
		{
			break;
		}
	}
}


void HPPush(HP* php, HPDataType x)
{
	assert(php);

	//判断是否扩容
	if (php->capacity == php->size)
	{
		int newCapacity = php->capacity == 0 ? 4 : php->capacity * 2;
//      HPDataType* temp = (HPDataType*)malloc(sizeof(HPDataType) * newCapacity);
		HPDataType* temp = (HPDataType*)realloc(php->a,sizeof(HPDataType) * newCapacity);
		if (temp == NULL)
		{
			perror("malloc fail");
			return;
		}
		php->capacity = newCapacity;
		php->a = temp;
	}
	//添加数据
	php->a[php->size] = x;
	php->size++;

	//向上调整数据
	AdjustUp(php->a, php->size - 1);
}

void HPPop(HP* php)//删除树根元素
{
	assert(php);
	assert(php->size > 0);

	//将第一个数据与最后一个数据交换
	Swap(&php->a[0], &php->a[php->size - 1]);
	//然后删除最后一个数据
	php->size--;
	//向下调整数据
	AdjustDown(php->a, php->size, 0);
}


HPDataType HPTop(HP* php)//取根数据
{
	assert(php);

	return php->a[0];
}

bool HPEmpty(HP* php)
{
	assert(php);

	return php->size == 0;
}