//#define _CRT_SECURE_NO_WARNINGS
//#include <stdio.h>
//
//#define MAXSIZE 20
//typedef int ElementType;
//
//typedef int Position;
//typedef struct LNode* List;
//struct LNode {
//    ElementType Data[MAXSIZE];
//    Position Last; /* 保存线性表中最后一个元素的位置 */
//};
//
//List ReadInput(); /* 裁判实现，细节不表。元素从下标0开始存储 */
//void PrintList(List L); /* 裁判实现，细节不表 */
//List Delete(List L, ElementType minD, ElementType maxD);
//
//int main()
//{
//    List L;
//    ElementType minD, maxD;
//    int i;
//
//    L = ReadInput();
//    scanf("%d %d", &minD, &maxD);
//    L = Delete(L, minD, maxD);
//    PrintList(L);
//
//    return 0;
//}
//
///* 你的代码将被嵌在这里 */
//
//List Delete(List L, ElementType minD, ElementType maxD) 
//{
//    Position i, j = 0; // i用于遍历，j用于记录非删除元素的新位置  
//    for (i = 0; i <= L->Last; i++) 
//    {
//        if (L->Data[i] <= minD || L->Data[i] >= maxD) 
//        {
//            // 如果元素不在删除范围内，则保留它  
//            if (i != j) 
//            {
//                // 如果i和j不同，说明需要移动元素  
//                L->Data[j] = L->Data[i];
//            }
//            j++; // 更新非删除元素的新位置  
//        }
//    }
//    L->Last = j - 1; // 更新最后一个元素的位置  
//    return L;
//}
