#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_STUDENTS 40000
#define MAX_COURSES 2500
#define MAX_NAME_LENGTH 5
#define MAX_QUERY_LENGTH 40000

// 定义学生结构体
typedef struct Student {
    char name[MAX_NAME_LENGTH]; 
    struct Student* next;
} Student;

// 定义课程结构体
typedef struct Course {
    int course_num;     //课程编号
    int num_students;   //选课的学生人数
    Student* students;  
} Course;

//课程使用数组形式来存储，学生姓名信息用链表形式来存储

int main() 
{
    Course courses[MAX_COURSES]; //创建课程的数组，按最大数量创建
    char query_students[MAX_QUERY_LENGTH][MAX_NAME_LENGTH];  //定义来查课的学生的二维数组
                                                            //一个char存储一个学生姓名的一个字符，需要5个是因为字符串最后有'/0'

    int N, K;
    scanf("%d %d", &N, &K);

    // 初始化课程
    for (int i = 0; i < K; i++) //这里一定要认真看（for的作用域在哪里），这里是读取一组数据后才会进行下一组读取
    {
        courses[i].course_num = i + 1;  //这里初始化与否作用不大，后续会再次输入数据覆盖
        courses[i].num_students = 0;
        courses[i].students = NULL;

        int num_students;  //声明选课的学生数量
        scanf("%d %d", &courses[i].course_num, &num_students);

        // 读取每门课的选课学生名单
        for (int j = 0; j < num_students; j++) 
        {
            char student_name[MAX_NAME_LENGTH];  //定义一个临时存储读取学生姓名的变量
            scanf("%s", student_name);

            // 创建新的学生节点
            Student* new_student = (Student*)malloc(sizeof(Student));
            strcpy(new_student->name, student_name);
            new_student->next = NULL;

            // 将学生节点添加到课程的学生链表中
            if (courses[i].students == NULL) //如果课程的学生链表为空直接作为头结点
            {
                courses[i].students = new_student;
            }
            else 
            {
                Student* current = courses[i].students; //定义一个遍历链表的临时变量
                while (current->next != NULL) //遍历链表，直到链表尾部
                {
                    current = current->next;
                }
                current->next = new_student;
            }

            courses[i].num_students++;
        }
    }

    // 读取查询学生的名字
    for (int i = 0; i < N; i++) 
    {
        scanf("%s", query_students[i]);
    }

    // 处理查询
    for (int i = 0; i < N; i++)
    {
        printf("%s ", query_students[i]);
        int selected_courses_count = 0;

        // 查找学生选修的课程
        for (int j = 0; j < K; j++) 
        {
            Student* current = courses[j].students;  //定义临时变量，来变量查课学生姓名链表
            while (current != NULL) 
            {
                if (strcmp(query_students[i], current->name) == 0) 
                {
                    selected_courses_count++;  //找到后让该学生的课程数量+1
                    break;
                }
                current = current->next;
            }
        }

        printf("%d ", selected_courses_count);  //先打印选课的数量
        
        //这一步是再次重新遍历来打印学生的选课编号
        for (int j = 0; j < K; j++) 
        {
            Student* current = courses[j].students;
            while (current != NULL) 
            {
                if (strcmp(query_students[i], current->name) == 0) 
                {
                    printf("%d ", courses[j].course_num);
                    break;
                }
                current = current->next;
            }
        }

        if (i < N - 1) //不可以输出多余空行
        {
            printf("\n");
        }
    }

    // 释放动态分配的内存
    for (int i = 0; i < K; i++) {
        Student* current = courses[i].students;
        while (current != NULL) {
            Student* temp = current;
            current = current->next;
            free(temp);
        }
    }

    return 0;
}