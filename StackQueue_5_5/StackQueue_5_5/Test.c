//#include"Stack.h"
//
//int main()
//{
//	ST s;
//	STInit(&s);
//	STPush(&s, 1);
//	STPush(&s, 2);
//	STPush(&s, 3);
//	STPush(&s, 4);
//
//	int top = STTop(&s);
//	printf("%d ", top);
//	STPop(&s);
//
//	STPush(&s, 5);
//	STPush(&s, 6);
//
//	while (!STEmpty(&s))
//	{
//		int top = STTop(&s);
//		printf("%d ", top);
//		STPop(&s);
//	}
//
//	STDistory(&s);
//	return 0;
//}


#include"Queue.h"

int main()
{
	Queue q;//创建一个结构体头尾指针变量
	QueueInit(&q);
	QueuePush(&q, 1);
	QueuePush(&q, 2);
	QueuePush(&q, 3);
	QueuePush(&q, 4);

	//队列的出入不会改变顺序
	printf("%d ", QueueFront(&q));
	QueuePop(&q);

	QueuePush(&q, 5);
	QueuePush(&q, 6);

	while (!QueueEmpty(&q))
	{
		printf("%d ", QueueFront(&q));
		QueuePop(&q);
	}
	QueueDistory(&q);
	return 0;
}