#include <iostream>
#include <stdio.h>
#include <string.h>
#include <windows.h>
#include <stdlib.h>
#include <unistd.h>
#include <stack>

#define M 100
#define INF 1000000  //用于表示没有路径的情况（无限大）
#define file "graph_data.txt"
using namespace std;


//定义账户和密码
char tour_id[] = "123456";
char tour_password[] = "000000";
char admin_id[] = "123456";
char admin_password[] = "111111";

typedef struct Node
{
	char name[100];        //景点名称
	int price;             //票价
	char open_time[20];    //开放时间
	int passenger_flow;    //客流量
	char type[50];         //类型
	char introduction[200];//简介
}Node;

typedef struct 
{
	int count;       //景点总数量
	int edge;        //道路数量
	int m[M][M];     //路径矩阵
	int time[M][M];  //时间矩阵
	int cost[M][M];  //花费矩阵
	Node Pname[M];   //各个景点的信息
}Map;

//读出数据
void read_graph(Map* p);
//写入数据
void write_graph(Map* p);
//查找景点名称对应的索引
int search_num(Map* p, char* name);
//添加景点
void append_graph(Map* p);
//输出景点信息
void print_graph(Map* p);
//管理查询景点
void admin_search_graph(Map* p);
//游客查询景点
void tour_search_graph(Map* p);
//删除景点
void delete_graph(Map* p);
//修改景点
void change_graph(Map* p);
//推荐节点
void show_around(Map* p);
//迪杰斯特拉算法
void dijkstra(int m[M][M], int s, int n, int* dist, int* path);
//单一最短路径
void shortone_graph(Map* p);
//单一最短时间路径
void short_time_path(Map* p);
//单一最低花费路径
void short_cost_path(Map* p);
//弗洛伊德算法，计算图中所有节点之间的最短路径
void floyd(int m[M][M], int path[M][M], int n);
//计算并打印图中所有节点之间的最短路径及路径信息
void shortall_graph(Map* p);
//查找并打印从起点到终点的所有简单路径以及最短简单路径
void all_shortpath(Map* p);
//最优路线规划函数
void best_path(Map* p);
//管理登录界面
void admin_mainface(Map* p);
//游客登录界面
void tour_mainface(Map* p);
//设置密码
void set_password();
//登录身份选择
void log_in();


//管理系统
void Account()
{
	system("cls");
	int flag;
	flag = 1;
	while (flag)
	{
		system("cls");
		printf("             景区管理系统\n");
		printf("*************************************\n");
		printf("            1. 登陆系统\n");
		printf("            2. 退出系统\n");
		char tempt[10];
		printf("请输入选项编号：");
		scanf("%s", tempt);
		int option = atoi(tempt);  // 将输入的字符串转换为整数
		switch (option)
		{
		case 1:
			log_in();  // 调用登录函数
			flag = 0;
			break;
		case 2:
			printf("退出成功!\n");
			flag = 0;
			break;
		default:
			printf("输入错误，请正确输入!\n");
			break;
		}
	}
}


//欢迎界面
void Welcome()
{
	cout<<               "\n\n\t\t******欢迎使用景区旅游信息管理系统******\n\n";
	cout<<" \t \t \t 按任意键进入系统... ...";
	getchar();
	Account();
}

int main()
{
	Welcome();
	return 0;
}

//读取数据
void read_graph(Map* p) 
{
	FILE* fp = fopen(file, "r");
	if (fp == NULL) 
	{
		printf("无法打开文件！\n");
		return;
	}
	
	//读取景点数量和道路数量
	fscanf(fp, "%d %d", &p->count, &p->edge);
	
	int distance, time, cost;
	//读取路径矩阵、时间矩阵和花费矩阵
	for (int i = 1; i <= p->count; i++) 
	{
		for (int j = 1; j <= p->count; j++) 
		{
			fscanf(fp, "%d %d %d", &distance, &time, &cost);
			p->m[i][j] = distance;
			p->time[i][j] = time;
			p->cost[i][j] = cost;
		}
	}
	
	//读取景点信息
	for (int i = 1; i <= p->count; i++) 
	{
		fscanf(fp, "%s %d %s %d %s %[^\n]", 
			p->Pname[i].name, 
			&p->Pname[i].price, 
			p->Pname[i].open_time, 
			&p->Pname[i].passenger_flow, 
			p->Pname[i].type, 
			p->Pname[i].introduction);
	}
	
	fclose(fp);
}

//写入数据
void write_graph(Map* p) 
{
	FILE* fp = fopen(file, "w");
	if (fp == NULL) 
	{
		printf("无法打开文件！\n");
		return;
	}
	
	//写入景点数量和道路数量
	fprintf(fp, "%d %d\n", p->count, p->edge);
	
	//写入路径矩阵、时间矩阵和花费矩阵
	for (int i = 1; i <= p->count; i++) 
	{
		for (int j = 1; j <= p->count; j++) 
		{
			fprintf(fp, "%d %d %d ", p->m[i][j], p->time[i][j], p->cost[i][j]);
		}
		fprintf(fp, "\n");
	}
	
	//写入景点的详细信息
	for (int i = 1; i <= p->count; i++) 
	{
		fprintf(fp, "%s %d %s %d %s %s\n", 
			p->Pname[i].name, 
			p->Pname[i].price, 
			p->Pname[i].open_time, 
			p->Pname[i].passenger_flow, 
			p->Pname[i].type, 
			p->Pname[i].introduction);
	}
	
	fclose(fp);  // 关闭文件
}

//输出景点信息
void print_graph(Map* p) 
{
	printf("景点个数：%d\n", p->count);
	printf("道路条数：%d\n\n", p->edge);
	
	printf("景点路径信息：(邻接矩阵)\n");
	printf("          ");
	for (int i = 1; i <= p->count; i++) 
	{
		printf("%-15s", p->Pname[i].name); //输出景点名称，并且左对齐
	}
	printf("\n");
	
	for (int i = 1; i <= p->count; i++) 
	{
		//打印行名（景点名称）
		printf("%-15s", p->Pname[i].name);
		
		for (int j = 1; j <= p->count; j++) 
		{
			if (p->m[i][j] == INF) 
			{
				printf("%-15s", "INF"); //输出INF并且左对齐
			} 
			else 
			{
				//输出路径值，宽度设为15个字符，右对齐
				printf("%-15d", p->m[i][j]);
			}
		}
		printf("\n");
	}
	printf("\n*** 景点信息 ***\n");
	for (int i = 1; i <= p->count; i++) 
	{
		printf("景点编号：%d\n", i);
		printf("\t名称：%s\n", p->Pname[i].name);
		printf("\t票价：%d\n", p->Pname[i].price);
		printf("\t开放时间：%s\n", p->Pname[i].open_time);
		printf("\t客流量：%d\n", p->Pname[i].passenger_flow);
		printf("\t类型：%s\n", p->Pname[i].type);
		printf("\t简介：%s\n", p->Pname[i].introduction);
		printf("\t---------------------------------------\n");
	}
	printf("输出成功\n");
	printf("----------------------------------------------------------\n");
}

//查找景点名称对应的索引
int search_num(Map* p, char* name) 
{
	for (int i = 1; i <= p->count; i++) 
	{
		if (strcmp(p->Pname[i].name, name) == 0) 
		{
			return i;
		}
	}
	return -1;
}

//添加景点
void append_graph(Map* p) 
{
	if (p->count >= M - 1) 
	{
		printf("景点数量已达最大值，无法添加新景点！\n");
		return;
	}
	
	char name[100], intro[200], open_time[20], type[50];
	int price, passenger_flow, n, len, time, cost;
	
	//增加景点数量
	p->count++;
	
	//获取景点名称
	printf("请输入景点名称：");
	scanf("%s", name);
	strcpy(p->Pname[p->count].name, name);
	
	//获取景点简介
	printf("请输入景点简介：");
	getchar(); //清除缓冲区中的换行符
	fgets(intro, sizeof(intro), stdin); //读取带空格的字符串
	intro[strcspn(intro, "\n")] = 0; //去掉末尾的换行符
	strcpy(p->Pname[p->count].introduction, intro);
	
	//获取景点票价
	printf("请输入景点票价：");
	scanf("%d", &price);
	p->Pname[p->count].price = price;
	
	//获取开放时间
	printf("请输入景点开放时间：");
	scanf("%s", open_time);
	strcpy(p->Pname[p->count].open_time, open_time);
	
	//获取客流量
	printf("请输入景点客流量：");
	scanf("%d", &passenger_flow);
	p->Pname[p->count].passenger_flow = passenger_flow;
	
	//获取景点类型
	printf("请输入景点类型：");
	scanf("%s", type);
	strcpy(p->Pname[p->count].type, type);
	
	//初始化新景点与其他景点的路径、时间、花费为 INF
	for (int j = 1; j <= p->count; j++) 
	{
		p->m[p->count][j] = INF;
		p->m[j][p->count] = INF;
		
		p->time[p->count][j] = INF;
		p->time[j][p->count] = INF;
		
		p->cost[p->count][j] = INF;
		p->cost[j][p->count] = INF;
	}
	p->m[p->count][p->count] = 0;
	p->time[p->count][p->count] = 0;
	p->cost[p->count][p->count] = 0;
	
	//获取相邻景点的数量
	printf("请输入相邻景点个数：");
	scanf("%d", &n);
	
	//输入与相邻景点的连接关系
	for (int i = 0; i < n; i++) 
	{
		char temp[100];
		printf("请输入相邻景点 %d 的名称：", i + 1);
		scanf("%s", temp);
		
		int t = search_num(p, temp); //查找相邻景点
		if (t != -1) 
		{
			p->edge++;
			printf("请输入景点 %s 与 %s 之间的路径长度：", p->Pname[p->count].name, temp);
			scanf("%d", &len);
			
			printf("请输入景点 %s 与 %s 之间的时间长度（分钟）：", p->Pname[p->count].name, temp);
			scanf("%d", &time);
			
			printf("请输入景点 %s 与 %s 之间的花费（单位）：", p->Pname[p->count].name, temp);
			scanf("%d", &cost);
			
			//更新邻接矩阵、时间矩阵和花费矩阵
			p->m[p->count][t] = len;
			p->m[t][p->count] = len;
			
			p->time[p->count][t] = time;
			p->time[t][p->count] = time;
			
			p->cost[p->count][t] = cost;
			p->cost[t][p->count] = cost;
		} 
		else 
		{
			printf("未找到该景点\n");
		}
	}
	
	printf("添加成功！\n");
	printf("--------------------------------------------\n");

	write_graph(p);
}


//管理查找景点
void admin_search_graph(Map* p) 
{
	char name[100];
	printf("请输入景点名称：");
	cin >> name;
	int n = search_num(p, name);
	if(n > 0 && n <= p->count)
	{
		//输出景点信息
		cout << "景点编号：" << n << endl;
		cout << "\t名称：" << p->Pname[n].name << endl;
		cout << "\t票价：" << p->Pname[n].price << endl;
		cout << "\t开放时间：" << p->Pname[n].open_time << endl;
		cout << "\t客流量：" << p->Pname[n].passenger_flow <<endl;
		cout << "\t类型：" << p->Pname[n].type <<endl;
		cout << "\t简介：" << p->Pname[n].introduction << endl;
		cout << "\t---------------------------------------\n";
	}
	else
	{
		cout << "未找到该景点" << endl;
		cout << "是否需要添加该景点（1:是   0:否）" << endl;
		int t;
		cin >> t;
		if(t)
		{
			append_graph(p);
		}
	}
	printf("                  查找结束\n");
	printf("--------------------------------------------\n");
}

//游客查找景点
void tour_search_graph(Map* p)
{
	char name[100];
	printf("请输入景点名称：");
	cin >> name;
	int n = search_num(p, name);
	if(n > 0 && n <= p->count)
	{
		//输出景点信息
		cout << "景点编号：" << n << endl;
		cout << "\t名称：" << p->Pname[n].name << endl;
		cout << "\t票价：" << p->Pname[n].price << endl;
		cout << "\t开放时间：" << p->Pname[n].open_time << endl;
		cout << "\t客流量：" << p->Pname[n].passenger_flow <<endl;
		cout << "\t类型：" << p->Pname[n].type <<endl;
		cout << "\t简介：" << p->Pname[n].introduction << endl;
		cout << "\t---------------------------------------\n";
	}
	else
	{
		cout << "未找到该景点" << endl;
	}
	printf("                  查找结束\n");
	printf("--------------------------------------------\n");
}

//删除景点
void delete_graph(Map* p) 
{
	char name[100];
	cout << "请输入删除景点名称：";
	cin >> name;
	int t = search_num(p, name);
	if (t != -1) 
	{
		int c = 0, i, j;
		//删除景点信息
		for (i = t; i < p->count; i++) 
		{
			p->Pname[i] = p->Pname[i + 1];
		}
		
		//统计删除的边数（无向图）
		for (i = 1; i <= p->count; i++) 
		{
			if (p->m[t][i] != 0 && p->m[t][i] != INF) 
			{
				c += 2; // 每条边计两次
			}
		}
		
		//更新邻接矩阵、时间矩阵和花费矩阵
		//删除列
		for (i = 1; i <= p->count; i++) 
		{
			for (j = t; j < p->count; j++) 
			{
				p->m[i][j] = p->m[i][j + 1];
				p->time[i][j] = p->time[i][j + 1];
				p->cost[i][j] = p->cost[i][j + 1];
			}
		}
		
		//删除行
		for (i = t; i < p->count; i++) 
		{
			for (j = 1; j <= p->count; j++) 
			{
				p->m[i][j] = p->m[i + 1][j];
				p->time[i][j] = p->time[i + 1][j];
				p->cost[i][j] = p->cost[i + 1][j];
			}
		}
		p->count--;
		p->edge -= c;
		cout << "                  删除成功\n";
	} 
	else 
	{
		cout << "          未找到该景点";
	}
	cout << "--------------------------------------------\n";
	write_graph(p);
}

//修改景点
void change_graph(Map* p) 
{
	char name[100];
	cout << "请输入要修改景点的名称：";
	cin >> name;
	int t = search_num(p, name);
	
	if (t != -1) 
	{
		//开始修改
		int choice;
		do {
			cout << "修改选项\n";
			cout << "1: 名称\n";
			cout << "2: 票价\n";
			cout << "3: 开放时间\n";
			cout << "4: 客流量\n";
			cout << "5: 类型\n";
			cout << "6: 简介\n";
			cout << "7: 路径（距离、时间、花费）\n";
			cout << "0: 退出\n";
			cout << "请输入您的选择：";
			cin >> choice;
			
			switch (choice) 
			{
				case 1: 
				//修改名称
				cout << "请输入新的名称：";
				cin >> p->Pname[t].name;
				break;
				
				case 2: 
				//修改票价
				cout << "请输入新的票价：";
				cin >> p->Pname[t].price;
				break;
				
				case 3: 
				//修改开放时间
				cout << "请输入新的开放时间：";
				cin >> p->Pname[t].open_time;
				break;
				
				case 4: 
				//修改客流量
				cout << "请输入新的客流量：";
				cin >> p->Pname[t].passenger_flow;
				break;
				
				case 5: 
				//修改类型
				cout << "请输入新的类型：";
				cin >> p->Pname[t].type;
				break;
				
				case 6: 
				//修改简介
				cout << "请输入新的简介：";
				getchar();
				fgets(p->Pname[t].introduction, sizeof(p->Pname[t].introduction), stdin);
				p->Pname[t].introduction[strcspn(p->Pname[t].introduction, "\n")] = 0; // 去掉换行符
				break;
				
				case 7: 
				//修改路径
				int num;
				cout << "请输入与该景点相邻的景点数量：";
				cin >> num;
				for (int i = 0; i < num; i++) 
				{
					char adjacent_name[100];
					int length, travel_time, cost;
					cout << "请输入相邻景点名称：";
					cin >> adjacent_name;
					int adj_index = search_num(p, adjacent_name);
					
					if (adj_index != -1) 
					{
						cout << "请输入路径长度：";
						cin >> length;
						cout << "请输入路径所需时间：";
						cin >> travel_time;
						cout << "请输入路径花费：";
						cin >> cost;
						
						//更新邻接矩阵
						p->m[t][adj_index] = length;
						p->m[adj_index][t] = length;
						
						//更新时间矩阵和花费矩阵
						p->time[t][adj_index] = travel_time;
						p->time[adj_index][t] = travel_time;
						
						p->cost[t][adj_index] = cost;
						p->cost[adj_index][t] = cost;
					} 
					else 
					{
						cout << "未找到该相邻景点！\n";
					}
				}
				break;
				
				case 0: 
				cout << "退出修改。\n";
				break;
				
			default:
				cout << "无效选择，请重新输入。\n";
			}
		} while (choice != 0);
		
		cout << "修改完成！\n";
	} 
	else 
	{
		cout << "未找到该景点！\n";
	}
	cout << "--------------------------------------------\n";
	write_graph(p);  //更新文件
}

//根据当前节点推荐类型节点
void show_around(Map* p) 
{
	char name[100];
	cout << "请输入当前景点的名称：";
	cin >> name;
	int n = search_num(p, name);
	
	if (n != -1) 
	{
		cout << "\t*** 当前景点信息 ***\n";
		cout << "名称：" << p->Pname[n].name << endl;
		cout << "票价：" << p->Pname[n].price << endl;
		cout << "开放时间：" << p->Pname[n].open_time << endl;
		cout << "客流量：" << p->Pname[n].passenger_flow << endl;
		cout << "类型：" << p->Pname[n].type << endl;
		cout << "简介：" << p->Pname[n].introduction << endl;
		
		cout << "\t*** 景点类型选择 ***\n";
		cout << "1. 自然类\n";
		cout << "2. 人文类\n";
		cout << "3. 科普类\n";
		cout << "请选择感兴趣的景点类型（输入对应编号）：";
		int choice;
		cin >> choice;
		
		string selected_type;
		switch (choice) 
		{
		case 1:
			selected_type = "自然类";
			break;
		case 2:
			selected_type = "人文类";
			break;
		case 3:
			selected_type = "科普类";
			break;
		default:
			cout << "无效选择，显示所有类型的景点。\n";
			selected_type = ""; //不进行类型筛选
			break;
		}
		
		cout << "\t*** 附近景点信息 ***\n";
		bool found = false;
		for (int i = 1; i <= p->count; i++) 
		{
			if (p->m[n][i] != 0 && p->m[n][i] != INF) 
			{
				if (selected_type.empty() || p->Pname[i].type == selected_type) 
				{
					found = true;
					cout << "名称：" << p->Pname[i].name << endl;
					cout << "票价：" << p->Pname[i].price << endl;
					cout << "开放时间：" << p->Pname[i].open_time << endl;
					cout << "客流量：" << p->Pname[i].passenger_flow << endl;
					cout << "类型：" << p->Pname[i].type << endl;
					cout << "简介：" << p->Pname[i].introduction << endl;
					cout << "距离：" << p->m[n][i] << endl;
					cout << "所需时间：" << p->time[n][i] << " 分钟" << endl;
					cout << "花费：" << p->cost[n][i] << " 元" << endl;
					cout << "---------------------------------------\n";
				}
			}
		}
		if (!found) 
		{
			cout << "未找到符合条件的附近景点！\n";
		}
	} 
	else 
	{
		cout << "未找到该景点！\n";
	}
	cout << "--------------------------------------------\n";
}


//迪杰斯特拉算法
void dijkstra(int m[M][M], int s, int n, int* dist, int* path)
{
	int final[M];  //标记是否找到最短路径
	int v, w, i, min;
	//初始化：final置0； path置-1(默认开始都无前驱)；dist置INF(无穷)
	//第一步：遍历出发点对应的行，更新dist(直达路径)，同时把(直达路径) path(前驱节点)设置为出发点编号
	for(v=1;v<=n;v++)
	{
		final[v]=0;            //final[v] = 0 表示节点 v 尚未确定最短路径
		path[v]=-1;            //path[v] = -1 表示节点 v 没有前驱节点
		dist[v]=m[s][v];       //初始化 dist[v] 为从起点 s 到节点 v 的直达距离
		if(dist[v]!=INF&&dist[v]!=0)
		{
			path[v]=s;         //如果 dist[v] 是有效的（不等于 INF 和 0），那么设置 path[v] 为起点 s，表示 v 是从 s 直接可达的
		}
	}
	final[s]=1;                //起始节点 s 的最短路径已经找到，标记为已处理
	dist[s]=0;                 //起点到自身的距离是 0
	
	//循环遍历，循环n-1次每次找出dist最短同时未找到最短路径的节点，
	//该节点已找到最短路径，同时更新与该节点直接相连节点的最短路径
	for (i = 1; i < n; ++i)
	{
		min=INF;
		for (w = 1; w <=n ; ++w)  //找到尚未处理的节点中，距离起点最近的节点 v
		{
			if (!final[w] && dist[w] < min)
			{
				v = w;
				min = dist[w];
			}
		}
		final[v] = 1;  //标记节点 v 为已处理，表示最短路径已经找到
		
		for (w = 1; w <= n; ++w)  //更新与节点 v 直接相连的节点的最短路径
		{
			if (!final[w] && min < INF && m[v][w] < INF && (min + m[v][w] < dist[w]))
			{
				dist[w] = min + m[v][w];    //更新 dist[w]，表示通过节点 v 到节点 w 的路径更短
				path[w] = v;                //更新 path[w]，表示 w 的前驱节点是 v
			}
		}
	}
	
}

//单一最短距离路径
void shortone_graph(Map* p)
{
	int dist[M], path[M]; //辅助数组
	cout << "请输入起点：";
	char name[100];
	cin >> name;
	int t = search_num(p, name);
	if (t != -1)
	{
		//调用 Dijkstra
		dijkstra(p->m, t, p->count, dist, path);
		cout << "\t            ***最短路径信息***\n";
		printf("\t%-16s %-16s %-16s\n", "景点名称", "最短路径", "前驱景点");
		
		for (int i = 1; i <= p->count; i++)
		{
			if (i == t)
				printf("\t%-16s %-16d %-16s\n", p->Pname[i].name, dist[i], "出发景点");
			else
				printf("\t%-16s %-16d %-16s\n", p->Pname[i].name, dist[i], p->Pname[path[i]].name);
		}
		
		cout << "\n";
		cout << "请输入终点：";
		cin >> name;
		int n = search_num(p, name);
		
		if (n != -1) //找到终点
		{
			stack<int> s;
			s.push(n); //终点编号入栈
			
			while (n != t)
			{
				int current = path[n];
				//检查路径长度相等时，选择编号较小的路径
				for (int i = 1; i <= p->count; i++)
				{
					if (dist[i] == dist[n] && i != n && i < current) //确保选择编号较小的路径
					{
						current = i;
					}
				}
				
				s.push(current);
				n = current;
			}
			
			cout << "最短路径：\n";
			n = s.top(); //起点编号
			s.pop();
			cout << p->Pname[n].name; //起点特殊处理
			
			while (!s.empty())
			{
				int next = s.top();
				s.pop();
				cout << "--" << p->m[n][next] << "->"; //输出路径长度
				cout << p->Pname[next].name;
				n = next;
			}
			cout << endl;
		}
		else
		{
			cout << "未找到该景点。\n";
		}
	}
	else
	{
		cout << "未找到该景点。\n";
	}
	cout << "--------------------------------------------\n";
}


//单一最短时间路径
void short_time_path(Map* p)
{
	int dist[M], path[M]; //两个辅助数组
	cout << "请输入起点：";
	char name[100];
	cin >> name;
	int t = search_num(p, name);
	if (t != -1)
	{
		//使用 Dijkstra 算法基于时间矩阵计算最短时间路径
		dijkstra(p->time, t, p->count, dist, path);
		
		printf("\t            ***最短时间路径信息***\n");
		printf("\t%-16s %-16s %-16s\n", "景点名称", "最短时间", "前驱景点");
		
		for (int i = 1; i <= p->count; i++)
		{
			if (i == t)
				printf("\t%-16s %-16d %-16s\n", p->Pname[i].name, dist[i], "出发景点");
			else
				printf("\t%-16s %-16d %-16s\n", p->Pname[i].name, dist[i], p->Pname[path[i]].name);
		}
		
		cout << "\n";
		//输出具体路径，通过栈实现
		cout << "请输入终点：";
		cin >> name;
		int n = search_num(p, name);
		if (n != -1) //找到终点
		{
			stack<int> s;
			s.push(n); //终点编号入栈
			while (n != t)
			{
//				int current = path[n];
//				
//				//检查是否存在多条路径时间相同的情况，优先选择编号较小的路径
//				for (int i = 1; i <= p->count; i++)
//				{
//					if (dist[i] == dist[n] && i != n && i < current) //找到时间相等的候选节点，选择编号较小的
//					{
//						current = i;
//					}
//				}
//				
//				s.push(current);
//				n = current;
				s.push(path[n]);
				n = path[n];
			}
			
			cout << "最短时间路径：\n";
			n = s.top(); //返回栈顶景点编号，即起点编号
			s.pop();     //弹出栈顶元素
			cout << p->Pname[n].name; //起点特殊处理
			while (!s.empty())
			{
				int next = s.top();
				s.pop();
				cout << " --" << p->time[n][next] << "-> ";
				cout << p->Pname[next].name;
				n = next; //更新当前景点编号
			}
			cout << endl;
		}
		else
			cout << "未找到该景点。\n";
	}
	else
		cout << "未找到该景点。\n";
	cout << "--------------------------------------------\n";
}

//单一最少花费
void short_cost_path(Map* p)
{
	int dist[M], path[M]; //两个辅助数组
	cout << "请输入起点：";
	char name[100];
	cin >> name;
	int t = search_num(p, name);
	if (t != -1)
	{
		//使用 Dijkstra 算法基于花费矩阵计算最低花费路径
		dijkstra(p->cost, t, p->count, dist, path);
		
		printf("\t            ***最低花费路径信息***\n");
		printf("\t%-16s %-16s %-16s\n", "景点名称", "最低花费", "前驱景点");
		
		for (int i = 1; i <= p->count; i++)
		{
			if (i == t)
				printf("\t%-16s %-16d %-16s\n", p->Pname[i].name, dist[i], "出发景点");
			else
				printf("\t%-16s %-16d %-16s\n", p->Pname[i].name, dist[i], p->Pname[path[i]].name);
		}
		cout << "\n";
		
		//输出具体路径，通过栈实现
		cout << "请输入终点：";
		cin >> name;
		int n = search_num(p, name);
		if (n != -1)
		{
			stack<int> s;
			s.push(n); // 终点编号入栈
			while (n != t)
			{
				int current = path[n];
				
				//检查是否存在多条路径花费相同的情况，优先选择编号较小的路径
				for (int i = 1; i <= p->count; i++)
				{
					if (dist[i] == dist[n] && i != n && i < current) //找到花费相等的候选节点，选择编号较小的
					{
						current = i;
					}
				}
				
				s.push(current);
				n = current;
			}
			
			cout << "最低花费路径：\n";
			n = s.top(); //返回栈顶景点编号，即起点编号
			s.pop();     //弹出栈顶元素
			cout << p->Pname[n].name; //起点特殊处理
			while (!s.empty())
			{
				int next = s.top();
				s.pop();
				cout << " --" << p->cost[n][next] << "-> ";
				cout << p->Pname[next].name;
				n = next; //更新当前景点编号
			}
			cout << endl;
		}
		else
		{
			cout << "未找到该景点。\n";
		}
	}
	else
	{
		cout << "未找到该景点。\n";
	}
	cout << "--------------------------------------------\n";
}


//弗洛伊德算法，计算图中所有节点之间的最短路径
void floyd(int m[M][M], int path[M][M], int n)
{
	//初始化路径矩阵，所有元素都设置为 -1，表示没有前驱节点
	for (int i = 1; i <= n; i++)
		for (int j = 0; j < n; j++)
			path[i][j] = -1;
	
	//三重循环进行动态规划
	for (int k = 1; k <= n; k++) //中介节点
	{
		for (int i = 1; i <= n; i++) //起始节点
		{
			for (int j = 1; j <= n; j++) //终止节点
			{
				//如果通过中介节点 k 能缩短从 i 到 j 的路径
				if (m[i][j] > m[i][k] + m[k][j])
				{
					//更新从 i 到 j 的最短路径长度
					m[i][j] = m[i][k] + m[k][j];
					//记录路径中 i 到 j 的前驱节点为 k
					path[i][j] = k;
				}
			}
		}
	}
}


//计算并打印图中所有节点之间的最短路径及路径信息
void shortall_graph(Map* p)
{
	int n[M][M], path[M][M]; //n 用于存储邻接矩阵的副本，path 用于存储路径矩阵
	
	//将图的邻接矩阵从 p 中复制到 n 中，以便在弗洛伊德算法中使用
	for (int i = 1; i <= p->count; i++)
	{
		for (int j = 1; j <= p->count; j++)
		{
			n[i][j] = p->m[i][j];
		}
	}
	
	//调用弗洛伊德算法，计算所有节点之间的最短路径
	floyd(n, path, p->count);
	
	//打印最短路径矩阵 n，即所有节点之间的最短路径长度
	cout << "最短路径长度矩阵：\n";
	for (int i = 1; i <= p->count; i++)
	{
		for (int j = 1; j <= p->count; j++)
			// 打印每个路径长度，宽度格式化为15字符
			printf("%15d", n[i][j]);
		cout << "\n";
	}
	cout << "---------------------------------------------------------------------------\n";
	
	//打印路径矩阵 path，即从任意节点 i 到节点 j 的路径中的中间节点
	cout << "路径矩阵（中间节点信息）：\n";
	for (int i = 1; i <= p->count; i++)
	{
		for (int j = 1; j <= p->count; j++)
			// 打印每个中间节点信息，宽度格式化为15字符
			printf("%15d", path[i][j]);
		cout << "\n";
	}
}


//递归查找并打印从起点到终点的所有简单路径
void all_paths(Map* p, int start, int end, int* visit, int* path, int path_idx, int path_len)
{
	//将当前节点标记为已访问，将当前节点添加到路径中，并更新当前路径长度
	visit[start] = 1;
	path[path_idx] = start;
	path_idx++;
	path_len += p->m[path[path_idx - 2]][start]; //累加路径长度
	
	//检查是否到达目标节点
	if (start == end)
	{
		cout << "Path：";
		int i;
		//打印路径中的每个节点及其间的边的长度
		for (i = 0; i < path_idx - 1; i++)
		{
			cout << p->Pname[path[i]].name << "--" << p->m[path[i]][path[i + 1]] << "->";
		}
		cout << p->Pname[path[i]].name << "\n";
		cout << "Distance: " << path_len << "\n";
		cout << "\n";
	}
	else
	{
		//遍历当前节点的所有邻居，进行递归调用以寻找路径
		for (int i = 1; i <= p->count; i++)
		{
			if (p->m[start][i] != INF && visit[i] == 0) //检查是否有边且邻居未被访问
			{
				all_paths(p, i, end, visit, path, path_idx, path_len);
			}
		}
	}
	
	//回溯：将当前节点的访问状态重新标记为未访问，并从路径中移除当前节点，同时更新路径长度
	visit[start] = 0;
	path_idx--;
	path_len -= p->m[path[path_idx - 1]][start];
}


//查找并打印从起点到终点的所有简单路径以及最短简单路径
void all_shortpath(Map* p)
{
	char start[100], end[100];
	int t, n;
	
	//输入起点并查找对应的编号
	cout << "请输入起点：";
	cin >> start;
	t = search_num(p, start);
	
	if (t != -1) //如果起点存在
	{
		// 输入终点并查找对应的编号
		cout << "请输入终点：";
		cin >> end;
		n = search_num(p, end);
		
		if (n != -1) //如果终点存在
		{
			int visit[M] = {0};  //访问标记数组，初始化为0
			int path1[M] = {0};  //存储路径的数组，用于保存所有简单路径
			
			//打印所有从起点到终点的简单路径
			cout << "所有简单路径：\n";
			all_paths(p, t, n, visit, path1, 0, 0);
			
			//求出从起点到终点的最短路径
			int dist[M];  //存储起点到各个节点的最短路径长度
			int path2[M]; //存储各个节点的前驱节点，用于回溯路径
			
			cout << "最短简单路径：\n";
			dijkstra(p->m, t, p->count, dist, path2);
			
			//使用栈来回溯和打印最短路径
			stack<int> s;
			s.push(n); //将终点压入栈中
			
			//回溯路径，直到起点
			while (n != t)
			{
				s.push(path2[n]); //将前驱节点压入栈中
				n = path2[n];
			}
			
			//打印从起点到终点的最短路径
			cout << "最短路径：\n";
			n = s.top(); //获取栈顶元素，即起点编号，不弹出
			s.pop(); //弹出栈顶元素
			cout << p->Pname[n].name; //打印起点
			
			//打印路径上的节点和路径长度
			while (!s.empty())
			{
				n = s.top();
				s.pop();
				cout << "--" << p->m[t][n] << "->"; //输出路径长度
				cout << p->Pname[n].name; //输出当前节点名称
				t = n; //更新 t 为当前节点，用于路径长度输出
			}
			cout << "\n";
		}
		else //未找到终点
			cout << "未找到该景点。\n";
	}
	else //未找到起点
		cout << "未找到该景点。\n";
	
	cout << "-----------------------------------------------\n";
}

//最优路线规划函数
void best_path(Map* p)
{
	char name[100];
	cout << "请输入出发景点：";
	cin >> name;
	
	//查找起点的编号
	int start = search_num(p, name);
	if (start != -1) // 如果起点存在
	{
		//定义权重（以整数表示权重分配）
		int w_dist = 4; // 距离权重
		int w_time = 3; // 时间权重
		int w_cost = 3; // 花费权重
		int weight_total = w_dist + w_time + w_cost; //权重总和，用于归一化
		
		//初始化辅助数组
		int dist[M], path[M], visit[M]; 
		int total_score = 0; //累计综合得分
		int total_dist = 0;  //总路径长度
		int total_time = 0;  //总时间
		int total_cost = 0;  //总花费
		
		//初始化权重矩阵（综合得分矩阵）
		int weight[M][M];
		for (int i = 1; i <= p->count; i++)
		{
			for (int j = 1; j <= p->count; j++)
			{
				//综合得分为三个属性的加权和
				if (p->m[i][j] == 0 || p->m[i][j] == INF)
					weight[i][j] = INF; 
				else
					weight[i][j] = (w_dist * p->m[i][j] + w_time * p->time[i][j] + w_cost * p->cost[i][j]) / weight_total;
			}
		}
		
		//使用 Dijkstra 算法基于权重矩阵计算最优路径
		dijkstra(weight, start, p->count, dist, path);
		
		//初始化访问状态数组
		int v, min;
		for (int i = 1; i <= p->count; i++)
			visit[i] = 0;
		
		cout << "综合最优路径：\n";
		cout << p->Pname[start].name;
		visit[start] = 1; //将起点标记为已访问
		
		while (1)
		{
			min = INF; //初始化最小值为无穷大
			for (int i = 1; i <= p->count; i++) //查找当前未访问的最优路径
			{
				if (!visit[i] && min > dist[i])
				{
					v = i;
					min = dist[i];
				}
			}
			
			int t = start, n = v;
			stack<int> s; //使用栈来回溯路径
			s.push(n); //将终点压入栈中
			
			//回溯路径，将前驱节点依次压入栈中
			while (n != t)
			{
				s.push(path[n]);
				n = path[n];
			}
			
			//打印路径和路径长度
			t = s.top(); //获取栈顶元素，不弹出
			s.pop(); //弹出栈顶元素
			while (!s.empty())
			{
				n = s.top();
				s.pop();
				cout << " --(";
				cout << "距离: " << p->m[t][n] << ", 时间: " << p->time[t][n] << ", 花费: " << p->cost[t][n] << ")--> ";
				cout << p->Pname[n].name; //打印节点名称
				//累加各项总值
				total_score += weight[t][n];
				total_dist += p->m[t][n];
				total_time += p->time[t][n];
				total_cost += p->cost[t][n];
				t = n; //更新 t 为当前节点
			}
			
			start = v; //更新起点为当前访问的最优路径终点
			visit[start] = 1; //将新起点标记为已访问
			
			//检查是否所有节点都已被访问
			int flag = 1;
			for (int i = 1; i <= p->count; i++)
				if (visit[i] == 0) flag = 0;
			if (flag == 1) break; //如果都访问过了，则退出循环
			
			//重新运行 Dijkstra 算法以更新从新起点到其他未访问节点的最短路径
			dijkstra(weight, start, p->count, dist, path);
		}
		
		cout << "\n";
		cout << "综合路径得分：" << total_score << "\n";
		cout << "总路径长度：" << total_dist << " 公里\n";
		cout << "总所需时间：" << total_time << " 分钟\n";
		cout << "总花费：" << total_cost << " 元\n";
	}
	else
		cout << "未找到该景点。\n";
	cout << "-----------------------------------------------\n";
}




//管理登录界面
void admin_mainface(Map* p)
{
	while (1)
	{
		system("cls");
		printf("\t****************************************************************\n");
		printf("\t------------------欢迎进入郑州市景点管理系统--------------------\n");
		printf("\t    *                   主菜单                              *    \n");
		printf("\t    *            【1】展示所有景点                          *    \n");
		printf("\t    *            【2】查找景点                              *    \n");
		printf("\t    *            【3】添加景点                              *    \n");
		printf("\t    *            【4】删除景点                              *    \n");
		printf("\t    *            【5】修改景点                              *    \n");
		printf("\t    *            【6】根据当前节点个性化推荐                *    \n");
		printf("\t    *            【7】两景点最短路径规划                    *    \n");
		printf("\t    *            【8】两景点最短时间规划                    *    \n");
		printf("\t    *            【9】两景点最少花费规划                    *    \n");
		printf("\t    *            【10】两景点所有简单路径和最短简单路径     *    \n");
		printf("\t    *            【11】景区最佳游览路径规划                 *    \n");
		printf("\t    *            【12】写入数据                             *    \n");
		printf("\t    *            【13】清屏                                 *    \n");
		printf("\t    *            【0】退出系统                              *    \n");
		printf("\t提示：本系统只会在开始时读出文件，退出系统以后写入文件，中间可以用过指令完成写入\n");
		printf("\t******************************************************\n");
		
		int t;
		printf("\n\t请选择您的操作 (0-13): ");
		scanf("%d", &t);
		printf("操作 %d\n", t);
		
		// 检测输入数据是否有效
		while (!(t >= 0 && t <= 13))
		{
			printf("*输入有误，请重新输入：");
			scanf("%d", &t);
			printf("操作 %d\n", t);
		}
		
		switch (t)
		{
		case 1:
			print_graph(p);
			cout << "\t \t \t 请按任意键继续...";
			getchar();
			getchar();
			break;
		case 2:
			admin_search_graph(p);
			cout << "\t \t \t 请按任意键继续...";
			getchar();
			getchar();
			break;
		case 3:
			append_graph(p);
			cout << "\t \t \t 请按任意键继续...";
			getchar();
			getchar();
			break;
		case 4:
			delete_graph(p);
			cout << "\t \t \t 请按任意键继续...";
			getchar();
			getchar();
			break;
		case 5:
			change_graph(p);
			cout << "\t \t \t 请按任意键继续...";
			getchar();
			getchar();
			break;
		case 6:
			show_around(p);
			cout << "\t \t \t 请按任意键继续...";
			getchar();
			getchar();
			break;
		case 7:
			shortone_graph(p);
			cout << "\t \t \t 请按任意键继续...";
			getchar();
			getchar();
			break;
		case 8:
			short_time_path(p);
			cout << "\t \t \t 请按任意键继续...";
			getchar();
			getchar();
			break;
		case 9:
			short_cost_path(p);
			cout << "\t \t \t 请按任意键继续...";
			getchar();
			getchar();
			break;
		case 10:
			all_shortpath(p);
			cout << "\t \t \t 请按任意键继续...";
			getchar();
			getchar();
			break;
		case 11:
			best_path(p);
			cout << "\t \t \t 请按任意键继续...";
			getchar();
			getchar();
			break;
		case 12:
			write_graph(p);
			cout << "\t \t \t 请按任意键继续...";
			getchar();
			getchar();
			break;
		case 13:
			system("cls");  // 清屏
			break;
		case 0:
			printf("\n\t\t\t*按任意键关闭本系统*\n");
			exit(0);  // 退出程序
			break;
		}
	}
}

//游客登录界面
void tour_mainface(Map* p)
{
	while (1)
	{
		system("cls"); 
		printf("\t*****************************************************************\n");
		printf("\t------------------欢迎进入郑州市景点管理系统---------------------\n");
		printf("\t    *                    主菜单                              *    \n");
		printf("\t    *            【1】展示所有景点                           *    \n");
		printf("\t    *            【2】查找景点                               *    \n");
		printf("\t    *            【3】根据当前节点个性化推荐                 *    \n");
		printf("\t    *            【4】两景点最短路径规划                     *    \n");
		printf("\t    *            【5】两景点最短时间规划                     *    \n");
		printf("\t    *            【6】两景点最少花销规划                     *    \n");
		printf("\t    *            【7】两景点所有简单路径和最短简单路径       *    \n");
		printf("\t    *            【8】景区最佳游览路径规划                   *    \n");
		printf("\t    *            【9】写入数据                               *    \n");
		printf("\t    *            【10】清屏                                  *    \n");
		printf("\t    *            【0】退出系统                               *    \n");
		printf("\t提示：本系统只会在开始时读出文件，退出系统以后写入文件，中间可以用过指令完成写入\n");
		printf("\t******************************************************\n");
		
		int t;
		printf("\n\t请选择您的操作 (0-10): ");
		scanf("%d", &t);
		printf("操作 %d\n", t);
		
		// 检测输入数据是否有效
		while (!(t >= 0 && t <= 10))
		{
			printf("*输入有误，请重新输入：");
			scanf("%d", &t);
			printf("操作 %d\n", t);
		}
		
		switch (t)
		{
		case 1:
			print_graph(p);
			cout << "\t \t \t 请按任意键继续...";
			getchar();
			getchar();
			break;
		case 2:
			tour_search_graph(p);
			cout << "\t \t \t 请按任意键继续...";
			getchar();
			getchar();
			break;
		case 3:
			show_around(p);
			cout << "\t \t \t 请按任意键继续...";
			getchar();
			getchar();
			break;
		case 4:
			shortone_graph(p);
			cout << "\t \t \t 请按任意键继续...";
			getchar();
			getchar();
			break;
		case 5:
			short_time_path(p);
			cout << "\t \t \t 请按任意键继续...";
			getchar();
			getchar();
			break;
		case 6:
			short_cost_path(p);
			cout << "\t \t \t 请按任意键继续...";
			getchar();
			getchar();
			break;
		case 7:
			all_shortpath(p);
			cout << "\t \t \t 请按任意键继续...";
			getchar();
			getchar();
			break;
		case 8:
			best_path(p);
			cout << "\t \t \t 请按任意键继续...";
			getchar();
			getchar();
			break;
		case 9:
			write_graph(p);
			cout << "\t \t \t 请按任意键继续...";
			getchar();
			getchar();
			break;
		case 10:
			system("cls"); // 清屏
			break;
		case 0:
			printf("\n\t\t\t*按任意键关闭本系统*\n");
			exit(0); // 退出程序
			break;
		}
	}
}

//设置密码
void set_password()
{
	printf("***提示：本系统可修改游客和管理的账户密码***\n");
	
	char id[50], old_password[50];
	printf("帐号：");
	scanf("%s", id);             //输入账号
	
	printf("旧密码：");
	scanf("%s", old_password);   //输入旧密码
	
	//检查是否是游客账号
	if (strcmp(old_password, tour_password) == 0 && strcmp(id, tour_id) == 0) 
	{
		//修改游客密码
		printf("请输入新密码：");
		scanf("%s", tour_password);     //输入新密码
		printf("游客密码重置成功\n");
	}
	//检查是否是管理员账号
	else if (strcmp(old_password, admin_password) == 0 && strcmp(id, admin_id) == 0) 
	{
		// 修改管理员密码
		printf("请输入新密码：");
		scanf("%s", admin_password);      //输入新密码
		printf("管理密码重置成功\n");
	}
	else 
	{
		printf("重置密码失败！\n");        //密码错误或者账号不匹配
	}
}

//登录身份选择
void log_in()
{
	Map p;
	read_graph(&p);
	
	char id[50], password[50];
	int flag1 = 1;      //辅助变量，便于退出循环
	
	while(flag1)        //多层嵌套循环实现不同界面之间的转化
	{
		system("cls");
		printf("             景区登陆界面\n");
		printf("*************************************\n");
		printf("            1. 游客登陆\n");
		printf("            2. 管理登录\n");
		printf("            3. 退出\n");
		printf("请输入选项编号：");
		
		int option;
		scanf("%d", &option);
		
		switch (option)
		{
		case 1:
			{
				printf("***游客登陆***\n");
				printf("   账号：");
				scanf("%s", id);
				if(strcmp(id, tour_id) == 0)
				{
					printf("   密码：");
					scanf("%s", password);
					if(strcmp(password, tour_password) == 0)
					{
						printf("           游客登陆成功，欢迎您！\n");
						printf("提示：系统将在3秒后跳转界面\n");
						printf("主界面进入中\n");
						sleep(3);  //延时3秒
						tour_mainface(&p);
						return;
					}
					else
					{
						printf("***密码错误***\n");
						sleep(3);  //延时3秒
					}
				}
				flag1 = 0;  //修改辅助变量，退出循环
				break;
			}
		case 2:
			{
				printf("***管理登陆***\n");
				for(int i = 3; i >= 0; i--)
				{
					printf("   账号：");
					scanf("%s", id);
					printf("   密码：");
					scanf("%s", password);
					if(strcmp(id, admin_id) == 0 && strcmp(password, admin_password) == 0)
					{
						printf("           管理登陆成功！\n");
						printf("请选择以下操作\n");
						printf("*************\n");
						printf("******1.进入管理系统******\n");
						printf("******2.修改密码    ******\n");
						printf("请输入选项编号：");
						
						int option;
						scanf("%d", &option);
						
						int flag2 = 1;  //辅助变量，便于退出循环
						while(flag2)
						{
							switch(option)
							{
							case 1:
								printf("           管理登陆成功，欢迎您！\n");
								printf("提示：系统将在3秒后跳转界面\n");
								printf("主界面进入中\n");
								sleep(3);  //延时3秒
								admin_mainface(&p);
								flag2 = 0;  //修改辅助变量2，退出循环
								break;
							case 2:
								set_password();
								flag2 = 0;  //修改辅助变量2，退出循环
								break;
							default:
								printf("输入错误，请正确输入!\n");
								break;
							}
						}
						break;  //登录成功后立即跳出循环
					}
					if(i != 0)
						printf("密码错误，请重新输入（你还有 %d 次输入机会)\n", i);
					else
						printf("4次输入错误，此账号已冻结！\n");
				}
				break;
			}
		case 3:
			printf("退出成功!\n");
			sleep(4);  //延时4秒
			return;
		default:
			printf("输入错误，请正确输入!\n");
			break;
		}
	}
}


