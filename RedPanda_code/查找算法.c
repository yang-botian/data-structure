#include<stdio.h>
#define KeyType int

//数据元素类型
typedef struct{
	KeyType key;  //关键字域
	//InfoType otherinfo;
}ElemType;

//顺序表的定义
typedef struct{
	ElemType *R;  //存储空间基地址
	int length;   //长度
}SSTable;


//顺序查找
/*
int Search_Seq(SSTable ST, KeyType key)
{
//在顺序表ST中顺序查找其关键字等于key的数据元素，若找到，则函数值为该元素在表中的位置，否则为0
for(int i = ST.length; i > 0 ; --i)
{
if(ST.R[i].key == key)
{
return 1;
}
return 0;
}
}
*/

//顺序查找（带哨兵位，且返回该数据所在数组下标）
//时间复杂度O(N)
int Search_Seq(SSTable ST,KeyType key)
{
	int i = 0;
	ST.R[0].key = key;  //哨兵
	for(i = ST.length; ST.R[i].key !=key; --i);    //从后往前找
	return i;  //若表中不存在关键字为 key 的元素，将查找到 i 为 0 时退出 for 循环
}
//在上述的算法中，将ST.R[0].key = key称作哨兵。
//引入它的目的是使得 Search_Seq内循环不必判断数组是否越界，因为满足 i==0 时，循环一定会跳出。
//这是因为初始化 ST.R[0].key = key ，当 i 减至 0 时，判断语句一定会成立。


//二分查找
//时间复杂度O(log2N)
int Search_Bin(SSTable ST, KeyType key)
{
	//在有序表ST中折半查找其关键字等于key的数据元素。若找到，则函数值为该元素在表中的位置，否则为0
	//设置查找范围
	int low = 1;
	int high = ST.length;
	while(low <= high)
	{
		int mid = (low + high) / 2;
		if(key == ST.R[mid].key)
		{
			return mid;   //找到返回其下标
		}
		else if(key < ST.R[mid].key)
		{
			high = mid - 1;  //在前一段表中查找
		}
		else
		{
			low = mid + 1;   //在后一段表中查找
		}
		return 0;
	}
}


