#include <stdio.h>
#include <stdlib.h>
typedef struct BiTreeNode{//平衡二叉树上的每一个节点
	struct BiTreeNode *lchild;
	int data;
	int height;
	struct BiTreeNode *rchild;
}BiTreeNode;
void menu(){
	printf("请选择操作：\n");
	printf("1. 插入节点（输入一系列值构建平衡二叉排序树，以回车结束输入）\n");
	printf("2. 展示平衡二叉排序树结构\n");
	printf("3. 进行中序遍历并输出结果\n");
	printf("4. 删除/搜索节点（先尝试删除，若不存在则插入）\n");
	printf("5. 退出程序\n");
}
BiTreeNode* creatBiTreeNode(int data){
	BiTreeNode* newNode=(BiTreeNode*)malloc(sizeof(BiTreeNode));//创建二叉树节点
	newNode->data=data;
	newNode->lchild=NULL;
	newNode->rchild=NULL;
	newNode->height=1;
	return newNode;
}
int max(int a, int b) {
	return (a > b) ? a : b;
}
int getHeight(BiTreeNode* currentNode){
	if(currentNode==NULL)
		return 0;
	else
		return currentNode->height;
}
int getBalanceFactor(BiTreeNode* currentNode){
	if(currentNode==NULL)
		return 0;
	else
		return getHeight(currentNode->lchild) - getHeight(currentNode->rchild);
}
BiTreeNode* RRotate(BiTreeNode*  originalRoot){//右旋
	BiTreeNode* currentNewRoot=originalRoot->lchild;
	BiTreeNode* child=currentNewRoot->rchild;
	currentNewRoot->rchild=originalRoot;
	originalRoot->lchild=child;
	//更新高度
	originalRoot->height = 1 + max(getHeight(originalRoot->lchild), getHeight(originalRoot->rchild));
	currentNewRoot->height = 1 + max(getHeight(currentNewRoot->lchild), getHeight(currentNewRoot->rchild));
	return currentNewRoot;
}
BiTreeNode* LRotate(BiTreeNode*  originalRoot){//左旋
	BiTreeNode* currentNewRoot=originalRoot->rchild;
	BiTreeNode* child=currentNewRoot->lchild;
	currentNewRoot->lchild=originalRoot;
	originalRoot->rchild=child;
	originalRoot->height = 1 + max(getHeight(originalRoot->lchild), getHeight(originalRoot->rchild));
	currentNewRoot->height = 1 + max(getHeight(currentNewRoot->lchild), getHeight(currentNewRoot->rchild));
	return currentNewRoot;
}
BiTreeNode* insert(BiTreeNode*  Root,int data)
{
	if(Root==NULL){
		return creatBiTreeNode(data);
	}else{
		if(data<Root->data)
		{
			Root->lchild=insert(Root->lchild,data);
		}else if(data>Root->data){
			Root->rchild=insert(Root->rchild,data);
		}else{
			return Root;
		}
	}
	Root->height=1+(getHeight(Root->lchild)>getHeight(Root->rchild)?getHeight(Root->lchild):getHeight(Root->rchild));
	int balanceFactor=getBalanceFactor(Root);
	if(balanceFactor>1&&data<Root->lchild->data)//左左
	{
		return RRotate(Root);
	}else if(balanceFactor>1&&data>Root->lchild->data)//先左后右
	{
		Root->lchild=LRotate(Root->lchild);
		return RRotate(Root);
	}else if(balanceFactor<-1&&data<Root->rchild->data){//右左
		Root->rchild=RRotate(Root->rchild);
		return LRotate(Root);
	}else if(balanceFactor<-1&&data>Root->rchild->data){//右右
		return LRotate(Root);
	}
	return Root;
}

void MiddleOorderTraversal(BiTreeNode*  Root){
	if(Root!=NULL){
		MiddleOorderTraversal(Root->lchild);
		printf("%d ",Root->data);
		MiddleOorderTraversal(Root->rchild);
	}
}
void displayTree(BiTreeNode* root, int level) {
	if (root == NULL) return;
	
	// 先打印右子树
	displayTree(root->rchild,level + 1);
	
	// 打印当前节点前的缩进
	for (int i = 0; i < level; i++) {
		printf("    ");
	}
	// 打印当前节点的值
	printf("%d", root->data);
	
	// 根据是否有左子树或右子树打印相应的符号
	if (root->lchild) printf(" <-- ");
	else printf("\n");
	
	// 打印左子树
	displayTree(root->lchild, level + 1);
}
BiTreeNode* Min(BiTreeNode* root)
{
	BiTreeNode* currentNode=root;
	while(currentNode&&currentNode->lchild!=NULL)
	{
		currentNode=currentNode->lchild;
	}
	return currentNode;
}
BiTreeNode* deleteBiTreeNode(BiTreeNode* root, int data) {
	//基本情况：空树直接返回
	if (root == NULL) {
		return NULL;
	}
	
	//遍历树寻找需要删除的节点
	if (data < root->data) 
	{
		//如果目标值小于当前节点值，递归进入左子树
		root->lchild = deleteBiTreeNode(root->lchild, data);
	} 
	else if (data > root->data) 
	{
		//如果目标值大于当前节点值，递归进入右子树
		root->rchild = deleteBiTreeNode(root->rchild, data);
	} 
	else 
	{
		//找到目标节点
		if (root->lchild == NULL || root->rchild == NULL) 
		{
			//情况1：该节点有一个子树或没有子树
			BiTreeNode* temp = root->lchild ? root->lchild : root->rchild; //选择非空子树
			free(root); //释放当前节点
			return temp; //返回非空子树或NULL（如果没有子树）
		} 
		else 
		{
			//情况2：该节点有两个子树
			//使用右子树的最小节点替代当前节点
			BiTreeNode* temp = Min(root->rchild);
			root->data = temp->data; //替换当前节点值
			//删除右子树中该最小节点
			root->rchild = deleteBiTreeNode(root->rchild, temp->data);
		}
	}
	
	//更新当前节点的高度
	root->height = 1 + max(getHeight(root->lchild), getHeight(root->rchild));
	
	//计算平衡因子
	int balanceFactor = getBalanceFactor(root);
	
	//检查并调整平衡性
	//情况1：左左 - 平衡因子大于1且左子树的平衡因子非负
	if (balanceFactor > 1 && getBalanceFactor(root->lchild) >= 0) 
	{
		return RRotate(root);
	}
	
	//情况2：左右 - 平衡因子大于1且左子树的平衡因子为负
	if (balanceFactor > 1 && getBalanceFactor(root->lchild) < 0) 
	{
		root->lchild = LRotate(root->lchild);
		return RRotate(root);
	}
	
	//情况3：右右 - 平衡因子小于-1且右子树的平衡因子非正
	if (balanceFactor < -1 && getBalanceFactor(root->rchild) <= 0) 
	{
		return LRotate(root);
	}
	
	//情况4：右左 - 平衡因子小于-1且右子树的平衡因子为正
	if (balanceFactor < -1 && getBalanceFactor(root->rchild) > 0) 
	{
		root->rchild = RRotate(root->rchild);
		return LRotate(root);
	}
	
	//返回调整后的根节点
	return root;
}

int main(){
	BiTreeNode* root = NULL;
	int choice;
	int data;
	do{
		menu();
		printf("请输入你的选择：");
		scanf("%d",&choice);
		if(choice==1)
		{
			printf("请输入要插入的值（输入-1结束）：\n");
			while (scanf("%d", &data) == 1) {
				if(data==-1)
				{
					break;
				}
				root = insert(root, data);
			}
			displayTree(root, 0);
			continue;
		}else if(choice==2)
		{
			if (root == NULL)
			{
				printf("树为空，请先进行插入节点操作构建树。\n");
			} else
			{
				// 直观展示生成的平衡二叉排序树结构
				printf("构建的平衡二叉排序树结构如下：\n");
				displayTree(root, 0);
			}
			continue;
		}else if(choice==3)
		{
			if (root == NULL)
			{
				printf("树为空，请先进行插入节点操作构建树。\n");
			} else {
				printf("构建的AVL树的中序遍历：\n");
				MiddleOorderTraversal(root);
				printf("\n");
			}
			continue;
		}else if(choice>5||choice<1)
		{
			printf("输入错误，请重新输入！");
			continue;
		}else if(choice==4)
		{
			if (root == NULL) {
				printf("树为空，请先进行插入节点操作构建树。\n");
			} else {
				printf("请输入要删除/搜索的值：");
				scanf("%d", &data);
				getchar(); // 吸收换行符
				
				root = deleteBiTreeNode(root,data);
				if (root) {
					printf("删除后的中序遍历：\n");
					MiddleOorderTraversal(root);
					printf("\n");
				} else {
					printf("无 %d\n", data);
					root = insert(root, data);
					printf("插入后的中序遍历：\n");
					MiddleOorderTraversal(root);
					printf("\n");
				}
			}
		}
	}while(choice!=5);
	printf("再见");
	
}




