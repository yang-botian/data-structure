#include<stdio.h>
#include<stdlib.h>

//基数排序
#define MAXD 2
#define MAXR 10
#define N 10
typedef struct node
{
	char data[MAXD];
	struct node* next;
}NodeType;

//p是指向待排序序列链表的头指针
//r为基数，d为关键字位数
//这里采用二位十进制数为测试
//则d = 2, r = 10, MAXR = 10
void Radix_Sort(NodeType* &p, int r, int d)
{
	NodeType *Head[MAXR], *Tail[MAXR], *t;
	int i, j, k; //循环变量
	//外循环次数是关键字位数d
	for(i = 0; i < d; i++)
	{
		//依次从低到高对数字入队
		for(j = 0; j < r; j++)
		{
			//首先对Head和Tail数组初始化
			Head[j] = NULL;
			Tail[j] = NULL;
		}
		while(p != NULL) //依次开始入队
		{
			k = p->data[i] - '0';
			if(Head[k] == NULL)  //假设当前数组中为NULL
			{
				Head[k] = p;
				Tail[k] = p;
			}
			else
			{
				Tail[k]->next = p;
				Tail[k] = p;
			}
			p = p->next;
		}
		
		//随后开始出队
		//重新进入排序
		p = NULL;
		for(j = 0; j < r; j++)
		{
			if(Head[j] != NULL)
			{
				if(p == NULL)
				{
					p = Head[j];
					t = Tail[j];
				}
				else
				{
					t->next = Tail[j];
					t = Tail[j];
				}
			}
		}
		//排完序后
		t->next = NULL;
	}
}

int main()
{
	
}
