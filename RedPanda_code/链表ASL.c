#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define MAX_WORD_LEN 50
#define MAX_WORDS 5000
typedef struct LinkNode 
{
	char word[MAX_WORD_LEN];
	int frequency;
	struct ListNode* next;
}LinkNode;

typedef struct
{
	LinkNode* head;
	int size;
}LinkList;

//创建链表
LinkNode* CreateNode(char* word);
//插入链表中单词
void Link_Insert(LinkList* link, char* word);
//将链表内容写入文件
void Write_Link_To_File(LinkList* link, char* filename);
//顺序查找ASL
void Link_Search(LinkList* list, char* searchWord, char* filename);
//释放
void Link_Free(LinkList* link);

int main() 
{
	// 初始化链表
	LinkList list;
	list.head = NULL;
	list.size = 0;
	
	// 插入一些单词
	Link_Insert(&list, "apple");
	Link_Insert(&list, "banana");
	Link_Insert(&list, "cherry");
	Link_Insert(&list, "date");
	Link_Insert(&list, "elderberry");
	
	// 写入链表到文件
	Write_Link_To_File(&list, "linklist_output.txt");
	
	// 测试查找成功
	printf("测试查找成功：\n");
	Link_Search(&list, "banana", "search_result_success.txt");
	
	// 测试查找失败
	printf("测试查找失败：\n");
	Link_Search(&list, "fig", "search_result_fail.txt");
	
	// 释放链表
	Link_Free(&list);
	
	return 0;
}


//创建链表
LinkNode* CreateNode(char* word)
{
	LinkNode* newNode = (LinkNode*)malloc(sizeof(LinkNode));
	if (newNode == NULL)
	{
		perror("malloc fail");
		exit(1);
	}
	strcpy(newNode->word, word);
	newNode->frequency = 1;
	newNode->next = NULL;
	return newNode;
}

//插入链表中单词(按字典频率)
void Link_Insert(LinkList* link, char* word)
{
	//判断头结点是否为NULL
	if (link->head == NULL)
	{
		link->head = CreateNode(word);
		link->size = 1;
		return;
	}
	//否则就依次循环判断
	LinkNode* current = link->head;
	LinkNode* prev = NULL;
	//遍历链表，直到找到单词或者需要插入位置
	while (current != NULL && strcmp(current->word, word) < 0)
	{
		prev = current;
		current = current->next;
	}
	//如果找到相同单词
	if (current != NULL && strcmp(current->word, word) == 0)
	{
		current->frequency++;
	}
	else
	{
		LinkNode* newNode = CreateNode(word);
		//判断插入哪里
		if (prev == NULL)
		{
			newNode->next = link->head;
			link->head = newNode;
		}
		else
		{
			//在中间或者最后插入
			newNode->next = prev->next;
			prev->next = newNode;
		}
		link->size++;
	}
}


//将链表内容写入文件
void Write_Link_To_File(LinkList* list, char* filename)
{
	FILE* file = fopen(filename, "w");
	if (file == NULL)
	{
		perror("fopen fail");
		return;
	}
	LinkNode* current = list->head;
	while (current != NULL)
	{
		fprintf(file, "%s: %d\n", current->word, current->frequency);
		current = current->next;
	}
	fclose(file);
}


//顺序查找ASL
void Link_Search(LinkList* link, char* searchWord, char* filename) {
	clock_t start_time = clock();
	int comparisons = 0;
	LinkNode* current = link->head;
	int position = 1;  // 位置从 1 开始，用于计算 ASL
	int found = 0;
	
	while (current != NULL) 
	{
		comparisons++;
		if (strcmp(current->word, searchWord) == 0) 
		{
			// 查找成功
			clock_t end_time = clock();
			double search_time = (double)(end_time - start_time) / CLOCKS_PER_SEC;
			double ASL = (double)comparisons / link->size;
			
			printf("查找成功：单词 '%s' 的频率为 %d\n", current->word, current->frequency);
			printf("查找成功的平均查找长度 ASL 为 %.2f\n", ASL);
			printf("查找所花的时间为 %.6f 秒\n", search_time);
			
			// 写入文件
			FILE* file = fopen(filename, "w");
			if (file == NULL) 
			{
				perror("fopen fail");
				return;
			}
			fprintf(file, "单词：%s\n频率：%d\n平均查找长度（ASL）：%.2f\n查找时间：%.6f 秒\n",
				current->word, current->frequency, ASL, search_time);
			fclose(file);
			found = 1;
			break;
		}
		current = current->next;
		position++;
	}
	
	if (!found) 
	{
		printf("查找失败：单词 '%s' 不在链表中\n", searchWord);
		
		// 写入文件
		FILE* file = fopen(filename, "w");
		if (file == NULL) 
		{
			perror("fopen fail");
			return;
		}
		fprintf(file, "查找失败：单词 '%s' 不在链表中\n", searchWord);
		fclose(file);
	}
}


//释放
void Link_Free(LinkList* link)
{
	LinkNode* current = link->head;
	while (current != NULL)
	{
		LinkNode* tmp = current;
		current = current->next;
		free(tmp);
	}
	link->head = NULL;
	link->size = 0;
}
