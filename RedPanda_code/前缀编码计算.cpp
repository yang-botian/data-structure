#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define MAX_LEN 30

// 定义栈的结构
typedef struct {
	double data[MAX_LEN];
	int top;
} Stack;

// 初始化栈
void init_stack(Stack* stack) {
	stack->top = -1;
}

// 判断栈是否为空
int is_empty(Stack* stack) {
	return stack->top == -1;
}

// 向栈中压入元素
void push(Stack* stack, double value) {
	if (stack->top < MAX_LEN - 1) {
		stack->data[++stack->top] = value;
	}
}

// 从栈中弹出元素
double pop(Stack* stack) {
	if (is_empty(stack)) {
		printf("ERROR\n");
		exit(1);  // 弹栈时栈为空，输出错误并退出
	}
	return stack->data[stack->top--];
}

// 进行基本的四则运算
double operate(double a, double b, char op) {
	switch (op) {
		case '+': return a + b;
		case '-': return a - b;
		case '*': return a * b;
		case '/': 
		if (b == 0) {
			printf("ERROR\n");
			exit(1);  // 防止除以零，输出错误并退出
		}
		return a / b;
	default:
		printf("ERROR\n");
		exit(1);  // 如果遇到非法运算符，输出错误并退出
	}
}

// 计算前缀表达式
double evaluate_prefix(char* expression) {
	Stack stack;
	init_stack(&stack);
	
	// 从右到左遍历表达式
	char* token = strtok(expression, " ");
	char* tokens[MAX_LEN];
	int count = 0;
	
	while (token != NULL) {
		tokens[count++] = token;
		token = strtok(NULL, " ");
	}
	
	// 逆序遍历 tokens 数组
	for (int i = count - 1; i >= 0; i--) {
		token = tokens[i];
		
		// 如果是数字，则压入栈
		if (isdigit(token[0]) || (token[0] == '-' && isdigit(token[1]))) {
			push(&stack, atof(token));
		} 
		// 如果是运算符，则从栈中弹出两个操作数进行运算
		else if (token[0] == '+' || token[0] == '-' || token[0] == '*' || token[0] == '/') {
			if (stack.top < 1) {
				printf("ERROR\n");
				exit(1);  // 如果栈中不足两个操作数，报错
			}
			double operand1 = pop(&stack);
			double operand2 = pop(&stack);
			double result = operate(operand1, operand2, token[0]);
			push(&stack, result);
		} 
		else {
			printf("ERROR\n");
			exit(1);  // 如果遇到非法字符，输出错误并退出
		}
	}
	
	// 最终栈中应该只剩下一个值
	if (stack.top != 0) {
		printf("ERROR\n");
		exit(1);  // 如果栈中没有最终结果或多于一个结果，报错
	}
	
	return pop(&stack);
}

int main() {
	char expression[MAX_LEN];
	
	// 读取输入
	fgets(expression, MAX_LEN, stdin);
	
	// 去除换行符
	expression[strcspn(expression, "\n")] = '\0';
	
	// 计算前缀表达式结果
	double result = evaluate_prefix(expression);
	
	// 输出结果，保留小数点后一位
	printf("%.1f\n", result);
	
	return 0;
}

