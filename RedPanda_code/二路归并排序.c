#include <stdio.h>
#include <stdlib.h>

// 合并两个有序子数组
void Merge(int arr[], int left, int mid, int right, long* compare_count, long* move_count)
{
	int i = left, j = mid + 1, k = 0;
	int* tmp_arr = (int*)malloc((right - left + 1) * sizeof(int)); // 临时数组
	
	// 合并左右两部分
	while (i <= mid && j <= right)
	{
		(*compare_count)++;
		if (arr[i] <= arr[j])
		{
			tmp_arr[k++] = arr[i++];
		}
		else
		{
			tmp_arr[k++] = arr[j++];
		}
		(*move_count)++;
	}
	
	// 处理左半部分剩余元素
	while (i <= mid)
	{
		tmp_arr[k++] = arr[i++];
		(*move_count)++;
	}
	
	// 处理右半部分剩余元素
	while (j <= right)
	{
		tmp_arr[k++] = arr[j++];
		(*move_count)++;
	}
	
	// 拷贝回原数组
	for (k = 0, i = left; i <= right; k++, i++)
	{
		arr[i] = tmp_arr[k];
		(*move_count)++;
	}
	
	free(tmp_arr);
}

// 对数组 arr 进行一趟归并
void MergePass(int arr[], int length, int n, long* compare_count, long* move_count)
{
	int i = 0;
	while (i + 2 * length - 1 < n) // 每次归并两个完整的子序列
	{
		Merge(arr, i, i + length - 1, i + 2 * length - 1, compare_count, move_count);
		i += 2 * length; // 跳到下一个待归并的位置
	}
	// 如果剩余部分无法完整分为两部分，进行最后的归并
	if (i + length - 1 < n - 1)
	{
		Merge(arr, i, i + length - 1, n - 1, compare_count, move_count);
	}
}

// 二路归并排序
void Merge_sort(int arr[], int n, long* compare_count, long* move_count)
{
	for (int length = 1; length < n; length *= 2)
	{
		MergePass(arr, length, n, compare_count, move_count);
	}
}

int main()
{
	int arr[] = {38, 27, 43, 3, 9, 82, 10};
	int n = sizeof(arr) / sizeof(arr[0]);
	long compare_count = 0, move_count = 0;
	
	Merge_sort(arr, n, &compare_count, &move_count);
	
	printf("Sorted array: ");
	for (int i = 0; i < n; i++)
	{
		printf("%d ", arr[i]);
	}
	printf("\n");
	printf("Comparisons: %ld, Moves: %ld\n", compare_count, move_count);
	
	return 0;
}

