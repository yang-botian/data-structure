#include<stdio.h>
#include<stdlib.h>

typedef struct Node 
{
	int data;
	int height;
	struct Node* left;
	struct Node* right;
} Node;

//获取节点高度
int getHeight(Node* node) 
{
	return node == NULL ? 0 : node->height;
}

//更新节点高度
int getMax(int a, int b) 
{
	return a > b ? a : b;
}

//创建新节点
Node* createNode(int data) 
{
	Node* node = (Node*)malloc(sizeof(Node));
	node->data = data;
	node->height = 1; //初始高度为 1
	node->left = NULL;
	node->right = NULL;
	return node;
}

//右旋
Node* rotateRight(Node* y) 
{
	Node* x = y->left;
	Node* T2 = x->right;
	
	//旋转
	x->right = y;
	y->left = T2;
	
	//更新高度
	y->height = getMax(getHeight(y->left), getHeight(y->right)) + 1;
	x->height = getMax(getHeight(x->left), getHeight(x->right)) + 1;
	
	return x;
}

//左旋
Node* rotateLeft(Node* x) 
{
	Node* y = x->right;
	Node* T2 = y->left;
	
	//旋转
	y->left = x;
	x->right = T2;
	
	//更新高度
	x->height = getMax(getHeight(x->left), getHeight(x->right)) + 1;
	y->height = getMax(getHeight(y->left), getHeight(y->right)) + 1;
	
	return y;
}

//获取平衡因子
int getBalance(Node* node) 
{
	return node == NULL ? 0 : getHeight(node->left) - getHeight(node->right);
}

//插入节点
Node* insert(Node* node, int data) 
{
	if (node == NULL)
		return createNode(data);
	
	if (data < node->data)
		node->left = insert(node->left, data);
	else if (data > node->data)
		node->right = insert(node->right, data);
	else //不允许重复节点
		return node;
	
	//更新高度
	node->height = getMax(getHeight(node->left), getHeight(node->right)) + 1;
	
	//检查平衡性
	int balance = getBalance(node);

	//左左
	if (balance > 1 && data < node->left->data)
		return rotateRight(node);
	
	//右右
	if (balance < -1 && data > node->right->data)
		return rotateLeft(node);
	
	//左右
	if (balance > 1 && data > node->left->data) 
	{
		node->left = rotateLeft(node->left);
		return rotateRight(node);
	}
	
	//右左
	if (balance < -1 && data < node->right->data) 
	{
		node->right = rotateRight(node->right);
		return rotateLeft(node);
	}
	
	return node;
}

//查找最小值节点
Node* findMin(Node* node) 
{
	while (node->left != NULL)
		node = node->left;
	return node;
}

//删除节点
Node* deleteNode(Node* root, int data) 
{
	if (root == NULL)
		return root;
	
	if (data < root->data)
	{
		root->left = deleteNode(root->left, data);
	}
		
	else if (data > root->data)
	{
		root->right = deleteNode(root->right, data);
	}
		
	else
	{
		if ((root->left == NULL) || (root->right == NULL)) 
		{
			Node* temp = root->left ? root->left : root->right;
			if (temp == NULL) 
			{
				temp = root;
				root = NULL;
			} else
				*root = *temp;
			free(temp);
		} else 
		{
			Node* temp = findMin(root->right);
			root->data = temp->data;
			root->right = deleteNode(root->right, temp->data);
		}
	}
	
	if (root == NULL)
		return root;
	
	//更新高度
	root->height = getMax(getHeight(root->left), getHeight(root->right)) + 1;
	
	//检查平衡性
	int balance = getBalance(root);
	
	//左左
	if (balance > 1 && getBalance(root->left) >= 0)
		return rotateRight(root);
	
	//左右
	if (balance > 1 && getBalance(root->left) < 0) 
	{
		root->left = rotateLeft(root->left);
		return rotateRight(root);
	}
	
	//右右
	if (balance < -1 && getBalance(root->right) <= 0)
		return rotateLeft(root);
	
	//右左
	if (balance < -1 && getBalance(root->right) > 0)
	{
		root->right = rotateRight(root->right);
		return rotateLeft(root);
	}
	
	return root;
}

//中序遍历
void inorderTraversal(Node* root) 
{
	if (root != NULL) 
	{
		inorderTraversal(root->left);
		printf("%d ", root->data);
		inorderTraversal(root->right);
	}
}

//横向打印AVL
void printHorizontal(Node* root, int space) 
{
	if (root == NULL)
		return;
	
	space += 5; // 增加缩进
	printHorizontal(root->right, space); // 先打印右子树
	
	printf("\n");
	for (int i = 0; i < space; i++)
		printf(" "); // 打印空格以形成缩进
	printf("%d\n", root->data); // 打印根节点
	
	printHorizontal(root->left, space); // 再打印左子树
}


//主函数
int main() 
{
	Node* root = NULL;
	int data;
	
	printf("输入数据（以回车结束）：\n");
	while (scanf("%d", &data)) 
	{
		root = insert(root, data);
		char c = getchar();
		if (c == '\n') break;
	}
	
	printf("\n生成的二叉排序树 (AVL 树)：\n");
	printHorizontal(root, 0);
	
	printf("\n中序遍历结果：\n");
	inorderTraversal(root);
	printf("\n");
	
	printf("\n输入要查找的元素：");
	scanf("%d", &data);
	
	if (deleteNode(root, data) == NULL) 
	{
		printf("无 %d，已插入。\n", data);
		root = insert(root, data);
	} 
	else 
	{
		printf("删除 %d 后的中序遍历：\n", data);
	}
	
	inorderTraversal(root);
	printf("\n最终的二叉排序树：\n");
	printHorizontal(root, 0);
	
	return 0;
}

