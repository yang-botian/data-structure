#include <stdio.h>
#include <math.h>

#define NULLKEY -1 // 定义空关键字值
typedef int KeyType;

typedef struct 
{
	KeyType key;
	int count; // 探测次数域
} HashTable;

// 判断是否是素数
int isPrime(int num) 
{
	if (num < 2) 
	{
		return 0;
	}
	//依次判断，该数能否被除1和自身以外的数整除
	for (int i = 2; i <= sqrt(num); i++) 
	{
		if (num % i == 0) 
		{
			return 0;
		}
	}
	return 1;
}

// 找到大于等于 m 的最小素数
int findNextPrime(int m) 
{
	while (isPrime(m) == 0) 
	{
		m++;
	}
	return m;
}

//插入关键字到散列表，平方探测解决冲突
int InsertHT(HashTable ha[], int &n, int m, int p, KeyType k) 
{
	int adr = k % p;      //计算初始哈希值
	int cnt = 0;          //记录探测次数
	while (ha[adr].key != NULLKEY) 
	{
		cnt++;
		adr = (k % p + cnt * cnt) % m;     //平方探测
		if (cnt >= m)    //散列表已满，无法插入
		{                    
			return -1;
		}
	}
	//如果可以插入就将该数据写入
	ha[adr].key = k;     
	ha[adr].count = cnt; 
	n++;                    //散列表元素个数增加
	return adr;
}

//创建散列表
void CreateHT(HashTable ha[], int &n, int m, int p, KeyType keys[], int total) 
{
	//初始化散列表
	for (int i = 0; i < m; i++) 
	{
		ha[i].key = NULLKEY;
		ha[i].count = 0;
	}
	n = 0;      //初始化元素个数
	for (int i = 0; i < total; i++) 
	{
		int result = InsertHT(ha, n, m, p, keys[i]);
		//针对打印情况处理，在插入第一个元素后，需要打印空格
		if (i > 0) 
			printf(" ");      //在首个元素后添加空格分隔
		if (result == -1)     //判断需要打印什么
		{
			printf("-");
		} 
		else 
		{
			printf("%d", result);
		}
	}
		printf("\n");
}

int main() 
{
	int MSize, N;           //输入的表长和关键字个数
	scanf("%d %d", &MSize, &N);
	
	MSize = findNextPrime(MSize);       //将表长调整为最小素数
	HashTable hashTable[MSize];         //定义散列表
	KeyType keys[N];                    //定义关键字数组
	
	for (int i = 0; i < N; i++)         //读取关键字
	{
		scanf("%d", &keys[i]);
	}
	
	int total;                          //哈希表的总元素个数
	CreateHT(hashTable, total, MSize, MSize, keys, N);
	
	return 0;
}

