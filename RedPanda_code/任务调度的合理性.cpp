#include<iostream>
using namespace std;

#define MAXV 201

//边点结构体
typedef struct ArcNode
{
	int adjvex;
	struct ArcNode* nextarc;
}ANode;
//顶点结构体
typedef struct VNode
{
	ANode* firstarc;
	int indegree;  //入度数
}VNode;
//图结构体
typedef struct
{
	VNode adjlist[MAXV];
}AdjGraph;

#define OK 1
#define ERROR 0
#define OVERFLOW -2
typedef int Status;
#define MAXSIZE 100  //顺序表的存储范围
#define STACK_INCREMENT 2  //存储空间分配增量

//这里假定SElemType是int类型
typedef int SElemType;

typedef struct
{
	SElemType* base;   //栈底指针
	SElemType* top;    //栈顶指针
	int stacksize;     //栈可用的最大容量
}SqStack;

//初始化栈
Status InitStack(SqStack& S)
{
	S.base = new SElemType[MAXSIZE];
	if (!S.base)
		exit(OVERFLOW);
	S.top = S.base;  //top初始化为base，空栈
	S.stacksize = MAXSIZE;  //stacksize的最大容量为MAXSIZE
	return OK;
}

//判空
Status StackEmpty(SqStack S)
{
	if (S.top == S.base) //如果栈顶和栈底指向同一个位置则证明栈为NULL
		return OK;
	else
		return ERROR;
}

//入栈
Status Push(SqStack& S, SElemType e)
{
	//插入元素为新的栈顶元素
	if (S.top - S.base == S.stacksize)//满栈
	{
		S.base = (SElemType*)realloc(S.base, (S.stacksize + STACK_INCREMENT) * sizeof(SElemType));
		//判断是否扩容成功
		if (!S.base)
			exit(OVERFLOW);//如果扩容失败，退出程序
		S.top = S.base + S.stacksize;
		S.stacksize += STACK_INCREMENT;  //更新栈的最大容量
	}
	*(S.top)++ = e;  //这里是先对S.top解引用后存入数据e，随后将top指针向后移动一位
	return OK;
}

//出栈
Status Pop(SqStack& S, SElemType &e)
{
	//删除栈顶元素，并返回其值
	if (!StackEmpty(S)) //栈不为空进行操作
	{
		e = *(--S.top);
		return OK;
	}
	else
		return ERROR;
}


//初始化图
void Init_Graph(AdjGraph* G)
{
	for(int i = 1; i <= MAXV - 1; i++)
	{
		G->adjlist[i].firstarc = NULL;
		G->adjlist[i].indegree = 0;
	}
}

//添加边
void Add_edge(AdjGraph* G, int from, int to)
{
	//创建新的边结点
	ANode* new_node = (ANode*)malloc(sizeof(ANode));
	//新的边节点用来作from的边
	new_node->adjvex = to;
	//头插法增加from的链表
	new_node->nextarc = G->adjlist[from].firstarc;
	G->adjlist[from].firstarc = new_node;
	//from点的入度值加1
	G->adjlist[to].indegree++;
}

//拓扑排序
bool TopSort(AdjGraph* G, int num_tasks)
{
	SqStack st;
	InitStack(st);
	
	// 入度为0的任务入栈
	for (int i = 1; i <= num_tasks; i++)
	{
		if (G->adjlist[i].indegree == 0)
		{
			Push(st, i);
		}
	}
	
	int count = 0;
	int m;
	
	// 拓扑排序
	while (!StackEmpty(st))
	{
		Pop(st, m);
		count++;
		ANode* p = G->adjlist[m].firstarc;
		
		// 遍历与m相邻的任务
		while (p != NULL)
		{
			G->adjlist[p->adjvex].indegree--;  // 入度减1
			if (G->adjlist[p->adjvex].indegree == 0)
			{
				Push(st, p->adjvex);  // 如果入度为0，加入栈
			}
			p = p->nextarc;
		}
	}
	
	// 如果所有任务都被处理了，说明没有环
	return count == num_tasks;
}

int main()
{
	//总任务数
	int num_tasks;
	scanf("%d",&num_tasks);
	
	AdjGraph G;
	Init_Graph(&G);
	
	for(int i = 1; i <= num_tasks; i++)
	{
		int k, from;
		scanf("%d",&k);
		for(int j = 0; j < k; j++)
		{
			scanf("%d",&from);
			Add_edge(&G, from, i); //from 依赖于 to
		}
	}
	
	//判断拓扑排序
	if(TopSort(&G, num_tasks))
	{
		cout << 1 << endl;
	}else{
		cout << 0 << endl;
	}
	
	return 0;
}
