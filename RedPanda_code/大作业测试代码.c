#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// 生成随机数并打乱数组
void random_array(int arr[], int n, int seed)
{
	srand(seed);  // 用给定的种子初始化随机数生成器
	
	// 生成随机数
	for (int i = 0; i < n; i++)
	{
		arr[i] = rand() % 100;  // 生成随机数 [0, 99]
	}
}

//交换函数
void Swap(int* a, int* b)
{
	int temp = *a;
	*a = *b;
	*b = temp;
}

//快速排序
//分区操作
int partition(int arr[], int left, int right)
{
	// 随机选择一个基准元素
	int pivotIndex = left + rand() % (right - left + 1);
	Swap(&arr[left], &arr[pivotIndex]);  // 将基准元素移到最左边
	int pivot = arr[left];//基准
	int i = left;
	int j = right;
	
	//结束条件是i == j
	while (i < j)
	{
		while (j > i && arr[j] >= pivot)
		{
			//(*compare_count)++;
			j--;  //从右向左遍历，直到找到一个小于pivot的数
		}
		if (i < j)
		{
			arr[i] = arr[j];
			//(*move_count) += 3;
			i++;
		}
		//寻找一次小数后寻找大数
		while (i < j && arr[i] <= pivot)
		{
			//(*compare_count)++;
			i++;
		}
		if (i < j)
		{
			arr[j] = arr[i];
			//(*move_count) += 3;
			j--;
		}
	}
	//循环结束后i == j的位置就是pivot的位置
	arr[i] = pivot;
	//(*move_count) += 3;
	return i;
}
//递归快排
void Quick_sort(int arr[], int left, int right)
{
	int i = 0;
	if (left < right)
	{
		//基准元素的索引
		i = partition(arr, left, right);
		//左递归
		Quick_sort(arr, left, i - 1);
		//右递归
		Quick_sort(arr, i + 1, right);
	}
}

int main()
{
	int arr[10];
	int n = sizeof(arr) / sizeof(arr[0]);
	for(int i=0;i<5;i++){
		random_array(arr,n,time(NULL));
		for(int i=0;i<=10;i++){
			printf(" %d ",arr[i]);
		}
		printf("\n");
	}
	

}
