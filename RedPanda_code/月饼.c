#include <stdio.h>
#include <stdlib.h>

//定义月饼的结构体
typedef struct 
{
	double stock;   // 库存量
	double price;   // 总售价
	double unitPrice; // 单位价格（price / stock）
}Mooncake;
//快排
//快排辅助函数
int partition(Mooncake mooncakes[], int left, int right)
{
	Mooncake pivot = mooncakes[left];
	int i = left;
	int j = right;
	while(i < j)
	{
		while(j > i && mooncakes[j].unitPrice <= pivot.unitPrice)
		{
			j--;
		}
		if(i < j)
		{
			mooncakes[i] = mooncakes[j];
			i++;
		}
		while(i < j && mooncakes[i].unitPrice >= pivot.unitPrice)
		{
			i++;
		}
		if(i < j)
		{
			mooncakes[j] = mooncakes[i];
			j--;
		}
	}
	//循环结束后，i == j的位置
	mooncakes[i] = pivot;
	return i;
}

void QuickSort(Mooncake mooncakes[], int left, int right)
{
	int i;
	if(left < right)
	{
		i = partition(mooncakes, left, right);
		QuickSort(mooncakes, left, i-1);
		QuickSort(mooncakes, i+1, right);
	}
}

int main() 
{
	int N;         //月饼种类数
	double D;      //市场最大需求量
	scanf("%d %lf", &N, &D);
	
	Mooncake mooncakes[N];
	for (int i = 0; i < N; i++) 
	{
		scanf("%lf", &mooncakes[i].stock);
	}
	for (int i = 0; i < N; i++) 
	{
		scanf("%lf", &mooncakes[i].price);
		mooncakes[i].unitPrice = mooncakes[i].price / mooncakes[i].stock; // 计算单位价格
	}
	
	QuickSort(mooncakes, 0, N - 1);
	
	double maxProfit = 0;       //最大收益
	for (int i = 0; i < N; i++) 
	{
		if (D >= mooncakes[i].stock) 
		{
			maxProfit += mooncakes[i].price;    //卖出全部库存
			D -= mooncakes[i].stock;
		} 
		else 
		{
			maxProfit += mooncakes[i].unitPrice * D; //部分卖出
			break;                           
		}
	}
	
	printf("%.2lf\n", maxProfit); // 输出最大收益，保留两位小数
	return 0;
}

