#include <stdio.h>
#include <stdlib.h>

void Two_way_insert_sort(int arr[], int n, long* compare_count, long* move_count) {
	int* temp = (int*)malloc(n * sizeof(int)); // 创建辅助数组
	int first = 0, final = 0;                 // 初始化头尾指针
	temp[0] = arr[0];                         // 初始化辅助数组第一个元素
	(*move_count)++;                          // 第一次移动计数
	
	for (int i = 1; i < n; i++) {
		if (arr[i] < temp[first]) { // 插入到辅助数组头部
			first = (first - 1 + n) % n;
			temp[first] = arr[i];
			(*move_count)++;
		} else if (arr[i] > temp[final]) { // 插入到辅助数组尾部
			final = (final + 1) % n;
			temp[final] = arr[i];
			(*move_count)++;
		} else { // 插入到中间，需移动部分元素
			int j = final; // 从尾部开始查找插入位置
			while (arr[i] < temp[j]) {
				(*compare_count)++;
				temp[(j + 1) % n] = temp[j];
				j = (j - 1 + n) % n;
				(*move_count)++;
			}
			temp[(j + 1) % n] = arr[i];
			(*move_count)++;
			final = (final + 1) % n; // 更新尾部指针
		}
		(*compare_count)++; // 每次插入都会有一次比较计数
	}
	
	// 将辅助数组按顺序拷回原数组
	for (int i = 0; i < n; i++) {
		arr[i] = temp[(first + i) % n];
		(*move_count)++;
	}
	
	free(temp); // 释放辅助数组
}

void print_array(int arr[], int n) {
	for (int i = 0; i < n; i++) {
		printf("%d ", arr[i]);
	}
	printf("\n");
}

int main() {
	int arr[] = {5, 3, 8, 6, 2, 7, 4, 1};
	int n = sizeof(arr) / sizeof(arr[0]);
	long compare_count = 0, move_count = 0;
	
	printf("Original array:\n");
	print_array(arr, n);
	
	Two_way_insert_sort(arr, n, &compare_count, &move_count);
	
	printf("Sorted array:\n");
	print_array(arr, n);
	
	printf("Comparisons: %ld, Moves: %ld\n", compare_count, move_count);
	
	return 0;
}

