#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#define MAX_SITES 20

// 景点结构体
typedef struct {
	char name[50];
	char description[200];
	float ticket_price;
	int max_capacity;
	char type[50];
	float x, y; // 坐标
} Site;

// 全局景点数组和数量
Site sites[MAX_SITES];
int site_count = 0;

// 添加景点
void add_site(char *name, char *description, float ticket_price, int max_capacity, char *type, float x, float y) {
	if (site_count >= MAX_SITES) {
		printf("景点数量已满！\n");
		return;
	}
	strcpy(sites[site_count].name, name);
	strcpy(sites[site_count].description, description);
	sites[site_count].ticket_price = ticket_price;
	sites[site_count].max_capacity = max_capacity;
	strcpy(sites[site_count].type, type);
	sites[site_count].x = x;
	sites[site_count].y = y;
	site_count++;
}

// 查询景点信息
void query_site(char *name) {
	for (int i = 0; i < site_count; i++) {
		if (strcmp(sites[i].name, name) == 0) {
			printf("景点名称: %s\n简介: %s\n票价: %.2f\n最大客流量: %d\n类型: %s\n",
				sites[i].name, sites[i].description, sites[i].ticket_price, sites[i].max_capacity, sites[i].type);
			return;
		}
	}
	printf("未找到景点: %s\n", name);
}

// 计算两个景点之间的距离
float calculate_distance(float x1, float y1, float x2, float y2) {
	return sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
}

// 推荐附近景点
void recommend_nearby_sites(float current_x, float current_y, float max_distance) {
	printf("附近的景点:\n");
	for (int i = 0; i < site_count; i++) {
		float distance = calculate_distance(current_x, current_y, sites[i].x, sites[i].y);
		if (distance <= max_distance) {
			printf("%s (距离: %.2f)\n", sites[i].name, distance);
		}
	}
}

// 路径规划（模拟实现）
void plan_route(char *start, char *end) {
	printf("路径规划:\n");
	printf("从%s到%s的路径方案:\n", start, end);
	printf("1. 公交车: 预计30分钟，费用2元\n");
	printf("2. 步行: 预计50分钟，免费\n");
	printf("3. 出租车: 预计15分钟，费用20元\n");
}

// 游览规划（简单推荐）
void plan_tour(char *start) {
	printf("从%s开始的游览推荐:\n", start);
	for (int i = 0; i < site_count; i++) {
		printf("%d. %s\n", i + 1, sites[i].name);
	}
}

// 主函数
int main() {
	// 初始化一些景点
	add_site("嵩山少林寺", "少林功夫的发源地", 80.0, 5000, "历史文化", 0.0, 0.0);
	add_site("郑州博物馆", "展示郑州历史文化的博物馆", 50.0, 3000, "博物馆", 1.0, 2.0);
	add_site("黄河风景名胜区", "黄河沿岸的自然风光", 100.0, 8000, "自然风光", 4.0, 5.0);
	
	int choice;
	char name[50];
	float x, y, max_distance;
	
	while (1) {
		printf("\n景点导游系统\n");
		printf("1. 查询景点\n");
		printf("2. 推荐附近景点\n");
		printf("3. 路径规划\n");
		printf("4. 游览规划\n");
		printf("5. 退出\n");
		printf("请选择功能: ");
		scanf("%d", &choice);
		
		switch (choice) {
		case 1:
			printf("请输入景点名称: ");
			scanf("%s", name);
			query_site(name);
			break;
		case 2:
			printf("请输入当前位置坐标 (x y): ");
			scanf("%f %f", &x, &y);
			printf("请输入最大推荐距离: ");
			scanf("%f", &max_distance);
			recommend_nearby_sites(x, y, max_distance);
			break;
		case 3:
			printf("请输入起点景点名称: ");
			scanf("%s", name);
			char end[50];
			printf("请输入终点景点名称: ");
			scanf("%s", end);
			plan_route(name, end);
			break;
		case 4:
			printf("请输入起点景点名称: ");
			scanf("%s", name);
			plan_tour(name);
			break;
		case 5:
			printf("感谢使用景点导游系统！\n");
			return 0;
		default:
			printf("无效的选择，请重新输入。\n");
		}
	}
	return 0;
}

