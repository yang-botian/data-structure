#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define Maxval 32767 // 定义最大值

// 哈夫曼树节点结构
typedef struct {
	char ch; // 字符
	int weight; // 节点权重
	int parent, lchild, rchild; // 父节点、左孩子、右孩子
} HTNode, *HuffmanTree; // 动态数组用于存储哈夫曼树

// 测试节点结构
typedef struct {
	char ch; // 字符
	char code[64]; // 编码
} TestNode;

// 选择最小和次小的权重节点
void Select(HuffmanTree HF, int i, int &s1, int &s2) 
{
	int small_1 = Maxval, small_2 = Maxval;
	
	for (int j = 1; j <= i - 1; j++) 
	{
		if (HF[j].parent == 0) 
		{
			if (HF[j].weight < small_1) 
			{
				small_2 = small_1;
				s2 = s1;
				small_1 = HF[j].weight;
				s1 = j;
			} 
			else if (HF[j].weight < small_2) 
			{
				small_2 = HF[j].weight;
				s2 = j;
			}
		}
	}
}

// 构建哈夫曼树
void Huffman(HuffmanTree HF, int num, int roads) 
{
	if(num <= 1)
		return;
	
	for (int i = 1; i <= roads; i++) 
	{
		HF[i].parent = 0; // 初始化父节点为0
		HF[i].lchild = HF[i].rchild = 0; // 初始化左右子节点为0
	}
	
	for (int i = num + 1; i <= roads; i++) 
	{
		int s1, s2;
		
		// 找到最小和第二小的权重
		Select(HF, i, s1, s2);
		
		// 创建新节点
		HF[i].weight = HF[s1].weight + HF[s2].weight;
		HF[i].lchild = s1;
		HF[i].rchild = s2;
		HF[s1].parent = HF[s2].parent = i; // 设置父节点
	}
}

// 计算哈夫曼树的总权重
int HuffmanVal(HuffmanTree HF, int num, int roads) 
{
	int val = 0;
	for (int i = num + 1; i <= roads; i++) 
	{
		val += HF[i].weight; // 累加权重
	}
	return val;
}

// 判断前缀码
int JudgePrefixCode(const char *code1, const char *code2) 
{
	int len1 = strlen(code1), len2 = strlen(code2);
	for (int i = 0; i < len2 && i < len1; i++) 
	{
		if (code1[i] != code2[i]) 
		{
			return 0; // 不是前缀
		}
	}
	return 1; // 是前缀
}

// 打印结果
void Print(int flags[], int n) 
{
	for (int i = 0; i < n; i++) 
	{
		printf(flags[i] == 0 ? "Yes\n" : "No\n"); // 根据标志打印
	}
}

int main() 
{
	int num;
	scanf("%d", &num);
	getchar();
	int roads = 2 * num - 1;
	HuffmanTree HF = (HuffmanTree)malloc((roads + 1) * sizeof(HTNode)); // 动态分配哈夫曼树
	
	// 初始化哈夫曼树
	for (int i = 0; i <= roads; i++) 
	{
		HF[i].weight = (i <= num) ? -1 : 0; // 初始化权重
		HF[i].ch = (i <= num) ? '-' : 0; // 初始化字符
	}
	
	// 输入字符和权重
	for (int i = 1; i <= num; i++) 
	{
		scanf(" %c %d", &HF[i].ch, &HF[i].weight); // 输入字符和权重，前面加空格以跳过换行
	}
	
	Huffman(HF, num, roads); // 构建哈夫曼树
	int val = HuffmanVal(HF, num, roads); // 获取总权重
	int n;
	scanf("%d", &n);
	getchar();
	TestNode exam[num + 1];  //创建测试节点的数组，每个节点结构体存储该测试的字符ch和给出的编码
	int flags[n];  //用来存储每组编码判断结果是否正确，0为正确即编码符合最优编码
	
	// 输入测试案例
	for (int k = 0; k < n; k++) 
	{
		flags[k] = 0;  //假定编码正确
		int score = 0;  //计算输入编码的实际长度
		
		for (int m = 1; m <= num; m++) 
		{
			scanf(" %c %s", &exam[m].ch, exam[m].code); // 输入字符和编码
			score += HF[m].weight * strlen(exam[m].code); // 计算
		}
		
		if (score != val) 
		{
			flags[k] = 1; // 不匹配
			continue;
		}
		
		// 检查前缀码
		for (int i = 1; i <= num; i++) 
		{
			for (int j = i + 1; j <= num; j++) 
			{
				if (JudgePrefixCode(exam[i].code, exam[j].code)) 
				{
					flags[k] = 1; // 发现前缀重合，判错
					break;
				}
			}
			if (flags[k] == 1) break; // 提前退出
		}
	}
	Print(flags, n); // 打印结果
	
	free(HF); // 释放动态分配的内存
	return 0;
}

