#include<stdio.h>
#include<stdlib.h>

//定义空关键字值
#define NULLKEY -1
//关键字类型
typedef int KeyType;
typedef struct{
	KeyType key;
	int count;  //探测次数域
}HashTable;

//插入
void InsertHT(HashTable ha[], int &n, int m, int p, KeyType k)
{
	int adr = k % p;  //计算哈希值
	if(ha[adr].key == NULLKEY)
	{
		ha[adr].key = k;
		ha[adr].count = 1;
	}
	else
	{
		int cnt = 1;   //cnt记录冲突的次数
		do
		{
			adr = (adr + 1) % m;
			cnt++;
		}while(ha[adr].key != NULLKEY);
		ha[adr].key = k;
		ha[adr].count = cnt;
	}
	n++;   //哈希表中总元素个数加一
}

//创建
void CreatHT(HashTable ha[], int &n, int m, int p, KeyType keys[], int total)
{
	//由关键字序列keys[0...total - 1]创建哈希表
	for(int i = 0; i < m; i++)  //首先将哈希表全部置空
	{
		ha[i].key = NULLKEY;
		ha[i].count = 0;
	}
	n = 0;
	//哈希表的总元素个数从0开始递增
	for(int i = 0; i < total; i++)
	{
		InsertHT(ha, n, m, p, keys[i]);
	}
}
