#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

// 定义最大顶点数
#define MAX_N 10
#define MAX_QUEUE_SIZE 100  // 定义队列的最大大小

// 图的邻接矩阵表示，graph[i][j]为1表示顶点i与顶点j之间有边
int graph[MAX_N][MAX_N];


// 队列结构体，用于BFS中的队列操作
typedef struct {
	int data[MAX_QUEUE_SIZE];  // 存放队列元素的数组
	int front, rear;           // 队列的前端和后端索引
} Queue;

// 初始化队列，设置front和rear为0
void initQueue(Queue *q) {
	q->front = q->rear = 0;
}

// 入队操作
void enqueue(Queue *q, int value) {
	if ((q->rear + 1) % MAX_QUEUE_SIZE != q->front) {  // 检查队列是否已满
		q->data[q->rear] = value;  // 将元素放入队列的后端
		q->rear = (q->rear + 1) % MAX_QUEUE_SIZE;  // 更新rear指针
	}
}

// 出队操作
int dequeue(Queue *q) {
	if (q->front == q->rear) {
		return -1;  // 如果队列为空，返回-1
	}
	int value = q->data[q->front];  // 获取队列前端元素
	q->front = (q->front + 1) % MAX_QUEUE_SIZE;  // 更新front指针
	return value;
}

// 判断队列是否为空
bool isQueueEmpty(Queue *q) {
	return q->front == q->rear;  // 如果front和rear相等，则队列为空
}

// 标记数组，用于标记顶点是否已经访问过
bool visited[MAX_N];

// 递归遍历图的所有顶点
void DFS(int v, int n) 
{
	visited[v] = true;  // 标记当前顶点为已访问
	printf("%d ", v);  
	
	// 遍历与当前顶点相邻的所有顶点
	for (int i = 0; i < n; i++) {
		// 如果存在边且邻接点未访问过，则递归调用DFS
		if (graph[v][i] && !visited[i]) {
			DFS(i, n);  // 递归深度优先遍历
		}
	}
}

// 用于广度优先遍历（BFS），使用队列按层遍历图的所有顶点
void BFS(int v, int n) 
{
	Queue q;
	initQueue(&q);  // 初始化队列
	enqueue(&q, v);  // 将起始顶点入队
	visited[v] = true;  // 标记起始顶点为已访问
	
	// 当队列不为空时，继续遍历
	while (!isQueueEmpty(&q)) 
	{
		int u = dequeue(&q);  // 出队操作，获取队列前端的元素
		printf("%d ", u);  // 输出当前顶点
		
		// 遍历与当前顶点相邻的所有顶点
		for (int i = 0; i < n; i++) {
			// 如果存在边且邻接点未访问过，则入队
			if (graph[u][i] && !visited[i]) {
				visited[i] = true;  // 标记邻接点为已访问
				enqueue(&q, i);  // 将邻接点入队
			}
		}
	}
}

int main() 
{
	int n, m;
	scanf("%d %d", &n, &m);  // 输入图的顶点数和边数
	
	// 初始化图的邻接矩阵，全部设置为0表示没有边
	for (int i = 0; i < n; i++) 
	{
		for (int j = 0; j < n; j++) 
		{
			graph[i][j] = 0;
		}
	}
	
	// 读取图的边
	for (int i = 0; i < m; i++) 
	{
		int u, v;
		scanf("%d %d", &u, &v);  // 读取边的两个顶点
		graph[u][v] = 1;  // 在邻接矩阵中标记这条边
		graph[v][u] = 1;  // 因为是无向图，边是双向的
	}
	
	// DFS：打印所有连通集
	//printf("DFS:\n");
	for (int i = 0; i < n; i++) 
	{
		// 如果顶点i没有被访问过，则开始一次新的DFS
		if (!visited[i]) 
		{
			printf("{ ");
			DFS(i, n);  // 从顶点i开始深度优先遍历
			printf("}\n");
		}
	}
	
	// 重置visited数组，为BFS做准备
	for (int i = 0; i < n; i++) {
		visited[i] = false;
	}
	
	// BFS：打印所有连通集
	//printf("BFS:\n");
	for (int i = 0; i < n; i++) {
		// 如果顶点i没有被访问过，则开始一次新的BFS
		if (!visited[i]) {
			printf("{ ");
			BFS(i, n);  // 从顶点i开始广度优先遍历
			printf("}\n");
		}
	}
	
	return 0;
}

