#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

#define MAX_SEATS 10  // 假设图书馆有10个座位
#define SEAT_FILE "seats.dat"  // 文件名

// 定义结构体表示文件锁
struct flock lock; 

// 加锁文件
void lock_file(int fd) 
{
	memset(&lock, 0, sizeof(lock)); // 清空lock结构体
	lock.l_type = F_WRLCK;           // 设置为写锁
	lock.l_whence = SEEK_SET;        // 从文件开始处锁定
	lock.l_start = 0;                // 锁从文件的开始位置开始
	lock.l_len = 0;                  // 锁定整个文件
	lock.l_pid = getpid();           // 设置锁定进程的PID
	
	// 强制加锁
	if (fcntl(fd, F_SETLK, &lock) == -1) 
	{
		perror("无法加锁文件");
		exit(1);
	}
}

//解锁文件
void unlock_file(int fd) 
{
	lock.l_type = F_UNLCK;  // 设置为解锁
	if (fcntl(fd, F_SETLK, &lock) == -1) 
	{
		perror("无法解锁文件");
		exit(1);
	}
}

//显示座位状态
void show_seats() 
{
	int fd = open(SEAT_FILE, O_RDONLY);//以只读模式打开
	if (fd == -1) 
	{
		perror("无法打开文件");
		return;
	}
	
	char seats[MAX_SEATS];  // 用来存储座位状态的数组
	if (read(fd, seats, MAX_SEATS) == -1) 
	{
		perror("无法读取文件");
		close(fd);
		return;
	}
	
	//若读取成功则根据数组中的数字进行打印座位信息
	printf("座位状态：\n");
	for (int i = 0; i < MAX_SEATS; i++) 
	{
		if (seats[i] == '1') 
		{
			printf("座位 %d: 已预定\n", i + 1);
		} 
		else 
		{
			printf("座位 %d: 可用\n", i + 1);
		}
	}
	
	close(fd);  // 关闭文件
}

//预定座位
void reserve_seat(int seat_number, char *student_name) 
{
	int fd = open(SEAT_FILE, O_RDWR);//以读写模式打开文件
	if (fd == -1) 
	{
		perror("无法打开文件");
		return;
	}
	
	// 加锁，确保文件在同一时刻只能被一个进程访问
	lock_file(fd);  // 强制行锁
	
	char seats[MAX_SEATS];  // 用来存储座位状态的数组
	if (read(fd, seats, MAX_SEATS) == -1) 
	{
		perror("无法读取文件");
		//这里解锁文件是因为，当程序遇到错误时，需要解开强制锁并释放空间
		//如果读取失败后没有解锁文件，锁会一直保持，可能导致其他进程无法访问该文件
		unlock_file(fd);  // 解锁
		close(fd);
		return;
	}
	
	// 如果座位已经被预定，输出提示
	//seat_number数据相对数组下标大1
	if (seats[seat_number - 1] == '1') 
	{
		printf("座位 %d 已经被预定。\n", seat_number);
	} 
	else 
	{
		seats[seat_number - 1] = '1';  // 将座位标记为已预定
		lseek(fd, 0, SEEK_SET);        // 文件指针移动到文件开头
		//这里将指针移到文件开头，是为了能够重写seats数组，覆盖之前的内容（更新座位信息）
		if (write(fd, seats, MAX_SEATS) == -1) 
		{
			perror("无法写入文件");
		} 
		else 
		{
			printf("座位 %d 成功预定给 %s。\n", seat_number, student_name);
		}
	}
	
	// 解锁，释放文件锁
	unlock_file(fd);  // 解锁文件
	close(fd);  // 关闭文件
}

//取消座位预定
void cancel_reservation(int seat_number) 
{
	int fd = open(SEAT_FILE, O_RDWR);
	if (fd == -1) 
	{
		perror("无法打开文件");
		return;
	}
	
	// 加锁，确保文件在同一时刻只能被一个进程访问
	lock_file(fd);  // 强制行锁
	
	char seats[MAX_SEATS];  // 用来存储座位状态的数组
	if (read(fd, seats, MAX_SEATS) == -1) 
	{
		perror("无法读取文件");
		unlock_file(fd);  // 解锁
		close(fd);
		return;
	}
	
	// 如果座位没有被预定，输出提示
	if (seats[seat_number - 1] == '0') 
	{
		printf("座位 %d 尚未预定。\n", seat_number);
	} else {
		seats[seat_number - 1] = '0';  // 将座位标记为未预定
		lseek(fd, 0, SEEK_SET);        // 文件指针移动到文件开头
		if (write(fd, seats, MAX_SEATS) == -1) 
		{
			perror("无法写入文件");
		} 
		else 
		{
			printf("座位 %d 的预定已取消。\n", seat_number);
		}
	}
	
	// 解锁，释放文件锁
	unlock_file(fd);  // 解锁文件
	close(fd);  // 关闭文件
}

int main() 
{
	// 初始创建座位文件并设置默认状态
	int fd = open(SEAT_FILE, O_RDWR | O_CREAT, 0644);
	if (fd == -1) 
	{
		perror("无法创建文件");
		return 1;
	}
	
	// 设置座位的初始状态，全为未预定（'0'）
	char seats[MAX_SEATS] = {'0'};
	if (write(fd, seats, MAX_SEATS) == -1) 
	{
		perror("无法写入初始状态");
		close(fd);
		return 1;
	}
	close(fd);
	
	int choice, seat_number;
	char student_name[50];
	
	while (1) 
	{
		printf("\n图书馆座位预定系统\n");
		printf("1. 显示座位状态\n");
		printf("2. 预定座位\n");
		printf("3. 取消预定\n");
		printf("4. 退出\n");
		printf("请输入操作选项：");
		scanf("%d", &choice);
		
		switch (choice) 
		{
		case 1:
			show_seats();
			break;
		case 2:
			printf("请输入要预定的座位号：");
			scanf("%d", &seat_number);
			printf("请输入预定者姓名：");
			scanf("%s", student_name);
			reserve_seat(seat_number, student_name);
			break;
		case 3:
			printf("请输入要取消预定的座位号：");
			scanf("%d", &seat_number);
			cancel_reservation(seat_number);
			break;
		case 4:
			printf("退出系统\n");
			return 0;
		default:
			printf("无效的选项，请重新选择。\n");
		}
	}
	
	return 0;
}

