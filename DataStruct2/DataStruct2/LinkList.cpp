#include<iostream>
using namespace std;

//初始化定义
#define OK 1
#define ERROR 0
#define OVERFLOW -1
//Status是函数返回值类型，其值是函数结果状态代码
typedef int Status;

//图书的数据结构
typedef struct
{
	char no[20];
	char name[50];
	float peice;
}Book;

//单链表的结构
typedef struct LNode
{
	Book data;
	struct LNode* next;
}LNode,*LinkList;

//声明链表
LinkList L;

//链表初始化
Status InitLink(LinkList &L)
{
	L = new LNode;
	L->next = NULL;
	return OK;
}

//链表销毁
Status DestroyLink(LinkList& L)
{
	LinkList p;
	while (L)
	{
		p = L;
		L = L->next;
		delete p;
	}
	//循环结束后，L将置为NULL
	return OK;
}

//清空链表
Status CleanLink(LinkList& L)
{
	LinkList p; //作为中间变量
	p = L->next;  //p指向该链表的首元节点
	while (p)
	{
		L->next = p->next;
		delete p;
		p = L->next;
	}
	L->next = NULL; //最后需要将头指针置为NULL，否则会成为野指针
	return OK;
}

//求表长
int GetLengthLink(LinkList L)
{
	LinkList p;
	p = L->next;
	int count = 0;//用于计数
	while (p)
	{
		++count;
		p = p->next;
	}
	return count;
}

//判空
Status EmptyLink(LinkList L)
{
	if (L->next)
		return ERROR;  //不为空
	else
		return OK;     //为空
}

//按照序号进行查找
Status GetElemLink(LinkList L, int i, Book& e)
{
	LinkList p;
	p = L->next;
	int j = 1;
	while (p && j < i)
	{
		p = p->next;
		++j;
	}
	if (!p || j > i)
	{
		return ERROR;  //如果没找到返回0
	}
	else
	{
		e = p->data;  //用引用返回得到的参数数据
		return OK;
	}
}

//按照元素进行查找
LNode* LocateElem(LinkList L, Book e)
{
	LinkList p;
	p = L->next;
	while (p && p->data.no != e.no)
	{
		p = p->next; 
	}
	return p;   //查找到后返回e元素的结点地址p，查找失败后返回为NULL
}

//链表插入
//将e节点元素插入到i节点的前面，则需要移动到i-1的位置
Status InsertLink(LinkList& L, int i, Book e)
{
	LinkList p;
	p = L;
	int j = 0;
	while (p && j < i - 1)
	{
		++j;
		p = p->next;
	}
	if (!p && j > i - 1)
	{
		return ERROR;
	}//该情况出现，即插入位置非法
	//循环结束后，已经移动到了i节点的前一个位置，进行尾插即可
	LinkList s = new LNode; //为即将要插入位置开辟一个结点
	s->data = e;
	s->next = p->next;//首先进行尾部连接
	p->next = s;//随后进行头部连接
	return OK;
}

//链表删除某个结点
Status DeleteLink(LinkList& L, int i, Book e)
{
	LinkList p;
	p = L;
	int j = 0;
	while (p && j < i - 1)
	{
		p = p->next;
		j++;
	}//循环结束后，p会指向要删除节点的前驱节点
	if (!(p->next) || j > i - 1)
	{
		return ERROR;
	}//要判断位置是否非法
	//进行删除
	LinkList q;
	q = p->next;    //临时保存被删除节点的地址以备后续释放
	p->next = q->next;  //改变被删除节点的前驱节点的指针域
	e = q->data;  //删除时拿出来被删除节点的数据
	delete q;
	return OK;
}

//创建单链表
//头插法
void CreatLink_F(LinkList &L, int n)
{
	L = new LNode;
	L->next = NULL;
	for (int i = 0; i < n; i++)
	{
		LinkList p = new LNode;
		cout << "依次输入书的书号，书名和价格" << endl;
		cin >> p->data.no;
		cin >> p->data.name;
		cin >> p->data.peice;
		p->next = L->next;
		L->next = p;
	}
}
//尾插法
void CreatLink_L(LinkList& L, int n)
{
	L = new LNode;
	L->next = NULL;
	LinkList r = L;
	for (int i = 0; i < n; i++)
	{
		LinkList p = new LNode;
		cout << "依次输入书的书号，书名和价格" << endl;
		cin >> p->data.no;
		cin >> p->data.name;
		cin >> p->data.peice;
		p->next = NULL;
		r = p;
	}
}

//遍历打印链表
void TraverseList(LinkList L)
{
	if (L == nullptr || L->next == nullptr) {
		cout << "链表为空" << endl;
		return;
	}
	LinkList p;
	p = L->next;
	while (p)
	{
		cout << p->data.no << "  " << p->data.name << "  " << p->data.peice << endl;
		p = p->next;
	}
}

int main()
{
	cout << "初始化创建链表时需要输入3个数据" << endl;
	CreatLink_F(L, 3);
	TraverseList(L);
}