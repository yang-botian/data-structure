
#include"Tree.h"

int main()
{
	BTNode* root = CreatNode();
	printf("前序遍历法: ");
	PreOrder(root);
	printf("\n该树中的结点数为: %d \n", TreeSize(root));
	printf("树的高度为: %d\n", TreeHeight(root));
	printf("层序遍历法: ");
	TreeLevelOrder(root);
	printf("该树是否为完全二叉树: %d",BinTreeComplete(root));
	TreeDestory(root);
	return 0;
}