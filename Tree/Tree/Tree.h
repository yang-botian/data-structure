#pragma once

#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<string.h>

typedef int BTDataType;

typedef struct BinTreeNode
{
	struct BinTreeNode* left;
	struct BinTreeNode* right;
	BTDataType val;
}BTNode;

BTNode* BuyNode(BTDataType x);
BTNode* CreatNode();

//前序
void PreOrder(BTNode* root);
//中序
void InOrder(BTNode* root);
//后序
void PostOrder(BTNode* root);
//层序遍历
void TreeLevelOrder(BTNode* root);
//这棵树的结点数
int TreeSize(BTNode* root);
//树高
int TreeHeight(BTNode* root);
//寻找树中的最大值
int maxDepth(BTNode* root);
//求第k层有几个数据
int TreeKLevel(BTNode* root, int k);
//查找数据所在的位置
BTNode* TreeFind(BTNode* root, BTDataType x);
//销毁树
void TreeDestory(BTNode* root);
//判断是否为完全二叉树
int BinTreeComplete(BTNode* root);