#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>

#define MAX_SIZE 1000  // 定义最大大小为1000，用于数组和堆栈

// 函数用于检查给定序列是否合法
int isValidSequence(int* sequence, int n, int m) {
    int stack[MAX_SIZE];  // 用数组模拟堆栈
    int top = -1;  // 堆栈顶部索引，初始为-1表示空堆栈
    int next_push = 1;  // 下一个要入栈的数字，从1开始
    int i;

    // 遍历给定序列中的每个数字
    for (i = 0; i < n; i++) 
    {
        // 如果当前数字大于next_push，就不断将数字入栈
        // 直到达到当前数字或堆栈满
        while (next_push <= sequence[i] && top < m - 1) 
        {
            stack[++top] = next_push++;
        }

        // 如果堆栈为空或顶部元素不是当前数字，序列不合法
        if (top == -1 || stack[top] != sequence[i]) {
            return 0;  // 返回0表示序列不合法
        }

        top--;  // 将顶部元素出栈
    }

    return 1;  // 所有数字都成功处理，序列合法
}

int main() {
    int m, n, k;  // m: 堆栈最大容量, n: 序列长度, k: 待检查序列数量
    int sequence[MAX_SIZE];  // 存储待检查的序列
    int i, j;

    // 读取输入参数
    scanf("%d %d %d", &m, &n, &k);

    // 处理k个序列
    for (i = 0; i < k; i++) 
    {
        // 读取当前序列的n个数字
        for (j = 0; j < n; j++) 
        {
            scanf("%d", &sequence[j]);
        }

        // 检查序列是否合法并输出结果
        if (isValidSequence(sequence, n, m)) {
            printf("YES\n");
        }
        else {
            printf("NO\n");
        }
    }

    return 0;
}