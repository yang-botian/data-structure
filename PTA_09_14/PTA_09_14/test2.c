#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>

typedef int ElementType;
typedef struct Node* PtrToNode;
struct Node {
    ElementType Data;
    PtrToNode   Next;
};
typedef PtrToNode List;

List Read(); /* 细节在此不表 */
void Print(List L); /* 细节在此不表 */

List Insert(List L, ElementType X);

int main()
{
    List L;
    ElementType X;
    L = Read();
    scanf("%d", &X);
    L = Insert(L, X);
    Print(L);
    return 0;
}

/* 你的代码将被嵌在这里 */
List Insert(List L, ElementType X)
{
    List current = L;
    List new_node = (List)malloc(sizeof(struct Node));
    new_node->Data = X;
    new_node->Next = NULL;
    while (current->Next != NULL && current->Next->Data < X)
    {
        current = current->Next;
    }
    new_node->Next = current->Next;
    current->Next = new_node;
    return L;
}