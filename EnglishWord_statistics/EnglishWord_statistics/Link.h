#pragma once
#define _CRT_SECURE_NO_WARNINGS
#include"Other.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct LinkNode 
{
    char word[MAX_WORD_LEN];
    int frequency;
    struct ListNode* next;
}LinkNode;

typedef struct
{
    LinkNode* head;
    int size;
}LinkList;

//创建链表
LinkNode* CreateNode(char* word);
//插入链表中单词
void Link_Insert(LinkList* link, char* word);
//将链表内容写入文件
void Write_Link_To_File(LinkList* link, char* filename);
//顺序查找ASL
void Link_Search(LinkList* link, char* searchWord, char* filename);
//释放
void Link_Free(LinkList* link);

