#pragma once
#include <stdbool.h>
#include "Other.h"

//哈希表项
typedef struct OpenHashTable
{
    char word[MAX_WORD_LEN];        //单词
    int frequency;                  //词频
    int count;                      //探测次数
}Open_HashTable;

//哈希函数
extern int HashFunction(char* key, int size);

//初始化散列表
void Init_Open_Address_HashTable(Open_HashTable* hashTable[], int size);
//插入单词到散列表
void Open_Address_HashTable_Insert(Open_HashTable* hashTable[], int size, char* word);
//将散列表内容写入文件
void Write_Open_Address_HashTable_To_File(Open_HashTable* hashTable[], int size, char* filename);
//查找ASL
void Open_Address_HashTable_Search(Open_HashTable* hashTable[], char* searchWord, int size, char* filename);
//释放内存
void Free_Open_Address_HashTable(Open_HashTable* hashTable[], int size);
