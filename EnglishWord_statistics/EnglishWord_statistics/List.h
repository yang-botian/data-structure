#pragma once
#include "Other.h"

typedef struct 
{
    char word[MAX_WORD_LEN];
    int frequency;
}WordNode;

typedef struct 
{
    WordNode data[MAX_WORDS];
    int size;
}List;

//将单词插入线性表
void List_Insert(List* list, char* word);
//将线性表写入文件中
void Write_List_To_File(List* list, char* filename);
//顺序查找ASL
void List_Search(List* list, char* searchWord, char* filename);
//二分查找ASL
void List_Binary_Search(List* list, char* searchWord, char* filename);
