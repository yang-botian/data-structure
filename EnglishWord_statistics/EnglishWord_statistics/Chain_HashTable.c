#include"Chain_HashTable.h"
#include"Other.h"
#include<time.h>

//初始化
void Init_Chain_HashTable(Chained_HashTable* hashTable, int size)
{
	hashTable->table = (Node**)malloc(size * sizeof(Node*));
	if (hashTable->table == NULL)
	{
		perror("malloc fail");
		exit(1);
	}
	hashTable->size = size;
	for (int i = 0; i < size; i++)
	{
		hashTable->table[i] = NULL;
	}
}

//插入
void Chain_HashTable_Insert(Chained_HashTable* hashTable, char* word)
{
	int index = HashFunction(word, hashTable->size);
	Node* current = hashTable->table[index];

	//遍历链表，查看是否存在相同单词
	while (current != NULL)
	{
		if (strcmp(current->word, word) == 0)
		{
			current->frequency++;
			return;
		}
		current = current->next;
	}

	//创建新节点
	Node* newNode = (Node*)malloc(sizeof(Node));
	if (newNode == NULL)
	{
		perror("malloc fail");
		exit(1);
	}
	strcpy(newNode->word, word);
	newNode->frequency = 1;
	newNode->next = hashTable->table[index];
	hashTable->table[index] = newNode;   //插入到链表头部
}

//将内容写入文件
void Write_Chain_HashTable_To_File(Chained_HashTable* hashTable, char* filename)
{
	FILE* file = fopen(filename, "w");
	if (file == NULL)
	{
		perror("fopen fail");
		exit(1);
	}

	//统计非空链表节点
	int count = 0;
	Node** nodeArray = (Node**)malloc(hashTable->size * sizeof(Node*));
	if (nodeArray == NULL)
	{
		perror("malloc fail");
		exit(1);
	}
	for (int i = 0; i < hashTable->size; i++)
	{
		Node* current = hashTable->table[i];
		while (current)
		{
			nodeArray[count++] = current;
			current = current->next;
		}
	}

	//排序
	for (int i = 0; i < count - 1; i++)
	{
		for (int j = 0; j < count - i - 1; j++)
		{
			if (strcmp(nodeArray[j]->word, nodeArray[j + 1]->word) > 0)
			{
				Node* temp = nodeArray[j];
				nodeArray[j] = nodeArray[j + 1];
				nodeArray[j + 1] = temp;
			}
		}
	}

	//将排序后的内容写入文件
	for (int i = 0; i < count; i++)
	{
		fprintf(file, "%s: %d\n", nodeArray[i]->word, nodeArray[i]->frequency);
	}
	free(nodeArray);
	fclose(file);
}

//链地址法ASL
void Chain_HashTable_Search(Chained_HashTable* hashTable, char* searchWord, char* filename)
{
	//输入合法性检查
	if (hashTable == NULL || searchWord == NULL || hashTable->size == 0)
	{
		printf("无效输入.\n");
		return;
	}

	clock_t start_time = clock();
	int totalComparisons = 0;  //总比较次数
	int unCount = 0;           //非空链的数量
	int found = 0;

	//遍历整个哈希表，统计总比较次数和非空链数量
	for (int i = 0; i < hashTable->size; i++)
	{
		Node* current = hashTable->table[i];
		if (current != NULL)
		{
			unCount++;
			while (current != NULL)
			{
				totalComparisons++;
				current = current->next;
			}
		}
	}

	//计算目标单词的哈希值
	int index = HashFunction(searchWord, hashTable->size);
	Node* current = hashTable->table[index];
	int comparisons = 0;  // 当前查找比较次数

	//遍历链表，查找目标单词
	while (current != NULL)
	{
		comparisons++;
		if (strcmp(current->word, searchWord) == 0)
		{
			//查找成功
			clock_t end_time = clock();
			double search_time = (double)(end_time - start_time) / CLOCKS_PER_SEC;
			double ASL = (double)totalComparisons / unCount;  //平均查找长度

			printf("#### 链地址法哈希表查找成功：####\n");
			printf("单词 '%s' 的频率为 %d\n", searchWord, current->frequency);
			printf("查找成功的平均查找长度 ASL 为 %.2f\n", ASL);
			printf("查找所花的时间为 %.6f 秒\n", search_time);

			//写入文件
			FILE* file = fopen(filename, "w");
			if (file == NULL)
			{
				perror("fopen fail");
				return;
			}
			fprintf(file, "#### 链地址法哈希表查找成功：####\n");
			fprintf(file, "单词 '%s' 的频率为 %d\n", searchWord, current->frequency);
			fprintf(file, "查找成功的平均查找长度 ASL 为 %.2f\n", ASL);
			fprintf(file, "查找所花的时间为 %.6f 秒\n", search_time);
			fclose(file);

			found = 1;
			break;
		}
		current = current->next;
	}

	if (!found)
	{
		//查找失败
		printf("#### 链地址法哈希表查找失败：####\n");
		printf("单词 '%s' 不在哈希表中\n", searchWord);

		FILE* file = fopen(filename, "w");
		if (file == NULL)
		{
			perror("fopen fail");
			return;
		}
		fprintf(file, "#### 链地址法哈希表查找失败：####\n");
		fprintf(file, "单词 '%s' 不在哈希表中\n", searchWord);
		fclose(file);
	}
}


//释放
void Free_Chain_HashTable(Chained_HashTable* hashTable)
{
	for (int i = 0; i < hashTable->size; i++)
	{
		Node* current = hashTable->table[i];
		while (current != NULL)
		{
			Node* temp = current;
			current = current->next;
			free(temp);
		}
	}
	free(hashTable->table);
}