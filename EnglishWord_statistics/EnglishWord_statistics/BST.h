#pragma once

#include "Other.h"

typedef struct BSTNode 
{
    char word[MAX_WORD_LEN];
    int frequency;
    int depth;
    struct BSTNode* left, * right;
} BSTNode;

typedef struct 
{
    BSTNode* root;
    int size;
} BSTTree;

//二叉排序树的插入
BSTNode* BST_Insert(BSTTree* tree, char* word, int depth);
//将二叉排序树写入文件
void Write_BST_To_File(BSTNode* root, char* filename);
//查找ASL
void BST_Search(BSTTree* tree, char* SearchWord, char* filename);
//释放空间
void Free_BSTTree(BSTTree* tree);
