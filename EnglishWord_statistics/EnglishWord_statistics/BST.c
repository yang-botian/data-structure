#include "BST.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

//插入单词到二叉排序树
BSTNode* BST_Insert(BSTNode* root, char* word, int depth, BSTTree* tree)
{
    //当前节点为空时，创建新节点
    if (root == NULL)
    {
        BSTNode* newNode = (BSTNode*)malloc(sizeof(BSTNode));
        if (newNode == NULL)
        {
            perror("malloc fail");
            exit(1);
        }
        strcpy(newNode->word, word);
        newNode->frequency = 1;
        newNode->depth = depth;
        newNode->left = newNode->right = NULL;
        tree->size++;  // 更新树的大小
        return newNode;
    }

    //否则树根不为空，比较当前单词与节点的单词
    int cmp = strcmp(word, root->word);

    //比树根小，插入到左子树
    if (cmp < 0)
    {
        root->left = BST_Insert(root->left, word, depth + 1, tree);
    }
    //比树根大，插入到右子树
    else if (cmp > 0)
    {
        root->right = BST_Insert(root->right, word, depth + 1, tree);
    }
    //如果单词相同，增加频率
    else
    {
        root->frequency++;
    }

    return root;
}



//中序遍历写入
void Inorder(BSTNode* node, FILE* file)
{
    if (node == NULL)
    {
        return;
    }
    Inorder(node->left, file);
    fprintf(file, "%s: %d\n", node->word, node->frequency);
    Inorder(node->right, file);
}
//将二叉排序树内容写入文件（中序遍历）
void Write_BST_To_File(BSTNode* root, char* filename) 
{
    FILE* file = fopen(filename, "w");
    if (file == NULL)
    {
        perror("fopen Error");
        return;
    }
    Inorder(root, file);
    fclose(file);
}

//查找ASL
void BST_Search(BSTTree* tree, char* searchWord, char* filename)
{
    //输入合法性检查
    if (tree == NULL || searchWord == NULL || tree->size <= 0)
    {
        printf("无效输入.\n");
        return;
    }

    clock_t start_time = clock();
    int comparisons = 0;  //比较次数
    int depth = 0;        //查找路径深度
    int found = 0;        //是否找到标记
    BSTNode* current = tree->root;

    while (current != NULL)
    {
        comparisons++;
        depth++;
        if (strcmp(current->word, searchWord) == 0)
        {
            //查找成功
            clock_t end_time = clock();
            double search_time = (double)(end_time - start_time) / CLOCKS_PER_SEC;
            double ASL = (tree->size > 0) ? (double)depth / tree->size : 0.0;

            printf("#### BST查找成功：####\n");
            printf("单词 '%s' 的频率为 %d\n", current->word, current->frequency);
            printf("查找成功的平均查找长度 ASL 为 %.2f\n", ASL);
            printf("查找所花的时间为 %.6f 秒\n", search_time);

            //写入文件
            FILE* file = fopen(filename, "w");
            if (file == NULL)
            {
                perror("fopen fail");
                return;
            }
            fprintf(file, "#### BST查找成功：####\n");
            fprintf(file, "单词 '%s' 的频率为 %d\n", current->word, current->frequency);
            fprintf(file, "查找成功的平均查找长度 ASL 为 %.2f\n", ASL);
            fprintf(file, "查找所花的时间为 %.6f 秒\n", search_time);
            fclose(file);

            found = 1;
            break;
        }
        current = (strcmp(searchWord, current->word) < 0) ? current->left : current->right;
    }

    if (!found)
    {
        //查找失败处理
        printf("#### BST查找失败：####\n");
        printf("单词 '%s' 不在树中\n", searchWord);

        FILE* file = fopen(filename, "w");
        if (file == NULL)
        {
            perror("fopen fail");
            return;
        }
        fprintf(file, "#### BST查找失败：####\n");
        fprintf(file, "单词 '%s' 不在树中\n", searchWord);
        fclose(file);
    }
}


//释放二叉排序树空间
void Free_BST(BSTNode* root)
{
    if (root != NULL)
    {
        Free_BST(root->left);  //释放左子树
        Free_BST(root->right); //释放右子树
        free(root);            //释放当前节点
    }
}
// 释放BST树结构
void Free_BSTTree(BSTTree* tree)
{
    if (tree != NULL && tree->root != NULL)
    {
        Free_BST(tree->root);  //释放整个二叉排序树
        tree->root = NULL;     //清空根节点
        tree->size = 0;        //重置大小
    }
}