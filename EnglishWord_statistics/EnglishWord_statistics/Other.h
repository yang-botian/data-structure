#pragma once
#define _CRT_SECURE_NO_WARNINGS
#define MAX_WORD_LEN 50
#define MAX_WORDS 5000

//读取单词数量，并将每个单词转化为小写存入words数组中
int read_words(const char* filename, char words[][MAX_WORD_LEN]);
//哈希函数
int HashFunction(char* key, int size);
//判断是否是素数
int isPrime(int num);
//找到大于等于 m 的最小素数
int findNextPrime(int m);