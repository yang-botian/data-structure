#include"Open_Address_HashTable.h"
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<time.h>

//初始化散列表
void Init_Open_Address_HashTable(Open_HashTable* hashTable[], int size)
{
	for (int i = 0; i < size; i++)
	{
		hashTable[i] = (Open_HashTable*)malloc(sizeof(Open_HashTable));
		if (hashTable[i] == NULL)
		{
			perror("malloc fail");
			exit(1);
		}
		hashTable[i]->word[0] = '\0';
		hashTable[i]->frequency = 0;
		hashTable[i]->count = 0;
	}
}

//插入哈希表
void Open_Address_HashTable_Insert(Open_HashTable* hashTable[], int size, char* word)
{
	int index = HashFunction(word, size);
	int probeCount = 1;     //初始化探测次数
	
	while (hashTable[index]->word[0] != '\0')
	{
		if (strcmp(hashTable[index]->word, word) == 0)
		{
			hashTable[index]->frequency++;
			return;
		}
		//如果不同继续探测
		index = (index + 1) % size;   //线性探测
		probeCount++;
	}
	//找到空位置
	strcpy(hashTable[index]->word, word);
	hashTable[index]->frequency = 1;
	hashTable[index]->count = probeCount;  //保存探测次数
}

//将散列表内容写入文件
void Write_Open_Address_HashTable_To_File(Open_HashTable* hashTable[], int size, const char* filename)
{
	FILE* file = fopen(filename, "w");
	if (file == NULL)
	{
		printf("fopen fail");
		return;
	}

	//提取非空内容
	Open_HashTable** nonEmptyTable = (Open_HashTable**)malloc(size * sizeof(Open_HashTable*));
	if (nonEmptyTable == NULL)
	{
		perror("malloc fail");
		return;
	}

	int count = 0;
	for (int i = 0; i < size; i++)
	{
		if (hashTable[i] != NULL && hashTable[i]->word[0] != '\0')
		{
			nonEmptyTable[count++] = hashTable[i];
		}
	}
	//对提取出来的数据进行排序
	for (int i = 0; i < count - 1; i++)
	{
		for (int j = 0; j < count - i - 1; j++)
		{
			if (strcmp(nonEmptyTable[j]->word, nonEmptyTable[j + 1]->word) > 0)
			{
				Open_HashTable* temp = nonEmptyTable[j];
				nonEmptyTable[j] = nonEmptyTable[j + 1];
				nonEmptyTable[j + 1] = temp;
			}
		}
	}

	//写入文件
	for (int i = 0; i < count; i++)
	{
		fprintf(file, "%s: %d\n", nonEmptyTable[i]->word, nonEmptyTable[i]->frequency);
	}

	free(nonEmptyTable);
	fclose(file);
}

//查找ASL
void Open_Address_HashTable_Search(Open_HashTable* hashTable[], char* searchWord, int size, char* filename)
{
	//输入验证
	if (hashTable == NULL || searchWord == NULL || size <= 0)
	{
		printf("无效输入.\n");
		return;
	}

	clock_t start_time = clock();
	int index = HashFunction(searchWord, size);
	int totalProbes = 0;    //总探测次数
	int unCount = 0;        //哈希表中非空项
	int probeCount = 0;     //当前查找探测次数
	int found = 0;

	//遍历哈希表，计算非空项和总探测次数
	for (int i = 0; i < size; i++)
	{
		if (hashTable[i] != NULL && hashTable[i]->word[0] != '\0')
		{
			unCount++;
			totalProbes += hashTable[i]->count;  //`count`表示插入时的探测次数
		}
	}

	//避免除零错误
	double ASL = (unCount > 0) ? (double)totalProbes / unCount : 0;

	//查找单词
	while (hashTable[index] != NULL && probeCount < size)
	{
		probeCount++;
		if (strcmp(hashTable[index]->word, searchWord) == 0)
		{
			//查找成功
			clock_t end_time = clock();
			double search_time = (double)(end_time - start_time) / CLOCKS_PER_SEC;

			printf("#### 哈希表查找成功：####\n");
			printf("单词 '%s' 的频率为 %d\n", searchWord, hashTable[index]->frequency);
			printf("查找成功的平均查找长度 ASL 为 %.2f\n", ASL);
			printf("查找所花的时间为 %.6f 秒\n", search_time);

			//写入文件
			FILE* file = fopen(filename, "w");
			if (file == NULL)
			{
				perror("fopen fail");
				return;
			}
			fprintf(file, "#### 哈希表查找成功：####\n");
			fprintf(file, "单词 '%s' 的频率为 %d\n", searchWord, hashTable[index]->frequency);
			fprintf(file, "查找成功的平均查找长度 ASL 为 %.2f\n", ASL);
			fprintf(file, "查找所花的时间为 %.6f 秒\n", search_time);
			fclose(file);

			found = 1;
			break;
		}
		index = (index + 1) % size;  //线性探测
	}

	//查找失败
	if (!found)
	{
		printf("#### 哈希表查找失败：####\n");
		printf("单词 '%s' 不在哈希表中\n", searchWord);

		FILE* file = fopen(filename, "w");
		if (file == NULL)
		{
			perror("fopen fail");
			return;
		}
		fprintf(file, "#### 哈希表查找失败：####\n");
		fprintf(file, "单词 '%s' 不在哈希表中\n", searchWord);
		fclose(file);
	}
}


//释放内存
void Free_Open_Address_HashTable(Open_HashTable* hashTable[], int size)
{
	for (int i = 0; i < size; i++)
	{
		if (hashTable[i] != NULL)
		{
			free(hashTable[i]);  // 释放每个哈希表项的内存
		}
	}
}