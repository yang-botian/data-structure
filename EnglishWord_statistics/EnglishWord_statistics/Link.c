#include"Link.h"
#include<time.h>

//创建链表
LinkNode* CreateNode(char* word)
{
	LinkNode* newNode = (LinkNode*)malloc(sizeof(LinkNode));
	if (newNode == NULL)
	{
		perror("malloc fail");
		exit(1);
	}
	strcpy(newNode->word, word);
	newNode->frequency = 1;
	newNode->next = NULL;
	return newNode;
}

//插入链表中单词(按字典频率)
void Link_Insert(LinkList* link, char* word)
{
	//判断头结点是否为NULL
	if (link->head == NULL)
	{
		link->head = CreateNode(word);
		link->size = 1;
		return;
	}
	//否则就依次循环判断
	LinkNode* current = link->head;
	LinkNode* prev = NULL;
	//遍历链表，直到找到单词或者需要插入位置
	while (current != NULL && strcmp(current->word, word) < 0)
	{
		prev = current;
		current = current->next;
	}
	//如果找到相同单词
	if (current != NULL && strcmp(current->word, word) == 0)
	{
		current->frequency++;
	}
	else
	{
		LinkNode* newNode = CreateNode(word);
		//判断插入哪里
		if (prev == NULL)
		{
			newNode->next = link->head;
			link->head = newNode;
		}
		else
		{
			//在中间或者最后插入
			newNode->next = prev->next;
			prev->next = newNode;
		}
		link->size++;
	}
}


//将链表内容写入文件
void Write_Link_To_File(LinkList* link, char* filename)
{
	FILE* file = fopen(filename, "w");
	if (file == NULL)
	{
		perror("fopen fail");
		return;
	}
	LinkNode* current = link->head;
	while (current != NULL)
	{
		fprintf(file, "%s: %d\n", current->word, current->frequency);
		current = current->next;
	}
	fclose(file);
}


//顺序查找ASL
void Link_Search(LinkList* link, char* searchWord, char* filename)
{
	//检查输入合法性
	if (link == NULL || searchWord == NULL || link->head == NULL)
	{
		printf("无效输入.\n");
		return;
	}

	clock_t start_time = clock();
	int comparisons = 0;  //比较次数
	LinkNode* current = link->head;
	int position = 1;     //当前节点的位置
	int found = 0;        //是否找到标记

	while (current != NULL)
	{
		comparisons++;
		if (strcmp(current->word, searchWord) == 0)
		{
			//查找成功
			clock_t end_time = clock();
			double search_time = (double)(end_time - start_time) / CLOCKS_PER_SEC;
			double ASL = (double)comparisons;           //顺序查找时，ASL即为比较次数

			printf("#### 链表顺序查找成功：####\n");
			printf("单词 '%s' 的频率为 %d\n", current->word, current->frequency);
			printf("查找成功的平均查找长度 ASL 为 %.2f\n", ASL);
			printf("查找所花的时间为 %.6f 秒\n", search_time);

			//写入文件
			FILE* file = fopen(filename, "w");
			if (file == NULL)
			{
				perror("fopen fail");
				return;
			}
			fprintf(file, "#### 链表顺序查找成功：####\n");
			fprintf(file, "单词 '%s' 的频率为 %d\n", current->word, current->frequency);
			fprintf(file, "查找成功的平均查找长度 ASL 为 %.2f\n", ASL);
			fprintf(file, "查找所花的时间为 %.6f 秒\n", search_time);
			fclose(file);

			found = 1;
			break;
		}
		current = current->next;
		position++;
	}

	if (!found)
	{
		//查找失败处理
		printf("#### 链表查找失败：####\n");
		printf("单词 '%s' 不在链表中\n", searchWord);

		FILE* file = fopen(filename, "w");
		if (file == NULL)
		{
			perror("fopen fail");
			return;
		}
		fprintf(file, "#### 链表查找失败：####\n");
		fprintf(file, "单词 '%s' 不在链表中\n", searchWord);
		fclose(file);
	}
}



//释放
void Link_Free(LinkList* link)
{
	LinkNode* current = link->head;
	while (current != NULL)
	{
		LinkNode* tmp = current;
		current = current->next;
		free(tmp);
	}
	link->head = NULL;
	link->size = 0;
}