#include"List.h"
#include"Other.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

//将单词插入线性表
void List_Insert(List* list, char* word)
{
	//首先判断在原有序列中有无该单词
	for (int i = 0; i < list->size; i++)
	{
		if (strcmp(list->data[i].word, word) == 0)
		{
			list->data[i].frequency++;
			return;
		}
	}
	//如果单词不存在，需要增加
	//首先判断是否超出容量
	if (list->size >= MAX_WORDS)
	{
		printf("Error: List capacity exceeded.\n");
		return;
	}
	//如果没有超出容量，则进行插入
	strcpy(list->data[list->size].word, word);
	list->data[list->size].frequency = 1;
	list->size++;
	return;
}

//冒泡排序，按字典顺序输出频率
void Bubble_Sort_List(List* list)
{
	for (int i = 0; i < list->size - 1; i++)
	{
		for (int j = 0; j < list->size - 1 - i; j++)
		{
			if (strcmp(list->data[j].word, list->data[j + 1].word) > 0)
			{
				WordNode tmp = list->data[j];
				list->data[j] = list->data[j + 1];
				list->data[j + 1] = tmp;
			}
		}
	}
}
//将线性表写入文件中
void Write_List_To_File(List* list, char* filename)
{
	//首先进行排序
	Bubble_Sort_List(list);

	//写入文件
	FILE* file = fopen(filename, "w");
	if (file == NULL)
	{
		perror("fopen Error");
		return;
	}
	for (int i = 0; i < list->size; i++)
	{
		fprintf(file, "%s: %d\n", list->data[i].word, list->data[i].frequency);
	}
	fclose(file);
}

//顺序表的顺序查找
void List_Search(List* list, char* searchWord, char* filename)
{
	//查找成功时为 （n+1）/2;  失败时为 n
	//输入验证
	if (list == NULL || list->size == 0 || searchWord == NULL)
	{
		printf("无效输入.\n");
		return;
	}

	clock_t start_time = clock();
	int comparisons = 0;              //比较次数
	int index = -1;                   //查找到的下标

	//顺序查找
	for (int i = 0; i < list->size; i++)
	{
		comparisons++;
		if (strcmp(list->data[i].word, searchWord) == 0)
		{
			index = i;
			break;
		}
	}

	clock_t end_time = clock();
	double search_time = (double)(end_time - start_time) / CLOCKS_PER_SEC;

	//打开文件
	FILE* file = fopen(filename, "w");
	if (file == NULL)
	{
		perror("fopen fail");
		return;
	}

	if (index != -1) 
	{
		//查找成功
		double ASL = (list->size + 1) / 2.0;
		printf("#### 顺序表顺序查找成功：####\n");
		printf("单词 '%s' 的频率为 %d\n", list->data[index].word, list->data[index].frequency);
		printf("查找成功的平均查找长度 ASL 为 %.2f\n", ASL);
		printf("查找所花的时间为 %.6f 秒\n", search_time);

		fprintf(file, "#### 顺序表顺序查找成功：####\n");
		fprintf(file, "单词 '%s' 的频率为 %d\n", list->data[index].word, list->data[index].frequency);
		fprintf(file, "查找成功的平均查找长度 ASL 为 %.2f\n", ASL);
		fprintf(file, "查找所花的时间为 %.6f 秒\n", search_time);
	}
	else 
	{
		//查找失败
		printf("#### 顺序表顺序查找失败：####\n");
		printf("单词 '%s' 不在顺序表中\n", searchWord);

		fprintf(file, "#### 顺序表顺序查找失败：####\n");
		fprintf(file, "单词 '%s' 不在顺序表中\n", searchWord);
	}

	fclose(file);
}

//二分查找ASL
void List_Binary_Search(List* list, char* searchWord, char* filename)
{
	if (list == NULL || list->size == 0 || searchWord == NULL)
	{
		printf("无效输入.\n");
		return;
	}

	clock_t start_time = clock();
	int comparisons = 0;
	int left = 0;
	int right = list->size - 1;
	int mid = 0;
	int index = -1;

	// 二分查找过程
	while (left <= right)
	{
		mid = (left + right) / 2;
		comparisons++;
		int cmp_result = strcmp(list->data[mid].word, searchWord);

		if (cmp_result == 0)
		{
			// 查找成功
			index = mid;
			break;
		}
		else if (cmp_result > 0)
		{
			right = mid - 1;
		}
		else
		{
			left = mid + 1;
		}
	}

	clock_t end_time = clock();
	double search_time = (double)(end_time - start_time) / CLOCKS_PER_SEC;

	// 动态计算二分查找的ASL
	double ASL = 0.0;
	int step = 0, layer_count = 1;
	for (int i = 1; i <= list->size; i *= 2)
	{
		int elements_at_layer = i < list->size - step ? i : list->size - step;
		ASL += layer_count * (elements_at_layer / (double)list->size);
		step += elements_at_layer;
		layer_count++;
	}

	if (index != -1)
	{
		// 输出查找成功信息
		printf("#### 顺序表二分查找成功 ####\n");
		printf("单词 '%s' 的频率为 %d\n", list->data[index].word, list->data[index].frequency);
		printf("查找成功的平均查找长度 ASL 为 %.2f\n", ASL);
		printf("查找所花的时间为 %.6f 秒\n", search_time);

		// 写入文件
		FILE* file = fopen(filename, "w");
		if (file == NULL)
		{
			perror("fopen fail");
			return;
		}
		fprintf(file, "#### 顺序表二分查找成功 ####\n");
		fprintf(file, "单词 '%s' 的频率为 %d\n", list->data[index].word, list->data[index].frequency);
		fprintf(file, "查找成功的平均查找长度 ASL 为 %.2f\n", ASL);
		fprintf(file, "查找所花的时间为 %.6f 秒\n", search_time);
		fclose(file);
	}
	else
	{
		// 查找失败
		printf("#### 顺序表二分查找失败 ####\n");
		printf("单词 '%s' 不在顺序表中\n", searchWord);

		FILE* file = fopen(filename, "w");
		if (file == NULL)
		{
			perror("fopen fail");
			return;
		}
		fprintf(file, "#### 顺序表二分查找失败 ####\n");
		fprintf(file, "单词 '%s' 不在顺序表中\n", searchWord);
		fclose(file);
	}
}
