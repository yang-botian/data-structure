#include "Other.h"
#include <stdio.h>
#include <ctype.h>
#include <string.h>

//从文件中读取单词，过滤标点并转小写
int read_words(const char* filename, char words[][MAX_WORD_LEN]) 
{
    FILE* file = fopen(filename, "r");
    //如果打开失败
    if (!file)
    {
        return -1;
    }
    //打开成功
    char ch;    //依次读取每个字符
    char word[MAX_WORD_LEN];   //存储单词
    int word_index = 0, char_index = 0;

    while ((ch = fgetc(file)) != EOF) 
    {
        //判断读取字符是不是英文字符或者数字
        if (isalnum(ch)) 
        {
            //如果是，将该字符的小写形式写入
            word[char_index++] = tolower(ch);
        }
        else if (char_index > 0)   //当读取的字符不是英语字符或者数字，那就是符号，证明该单词结束了
        {
            word[char_index] = '\0';
            strcpy(words[word_index++], word);   //将该单词存入words数组中
            //单词数量++
            char_index = 0;
        }
    }
    fclose(file);
    return word_index;
}

//哈希函数
int HashFunction(char* key, int size)
{
    unsigned long hash = 0;
    while (*key)
    {
        hash = hash * 31 + *key;  //31作为权重
        key++;
    }
    return hash % size;
}

//判断是否是素数
int isPrime(int num)
{
    if (num < 2)
    {
        return 0;
    }
    //依次判断，该数能否被除1和自身以外的数整除
    for (int k = 2; k <= sqrt(num); k++)
    {
        if (num % k == 0)
        {
            return 0;
        }
    }
    return 1;
}

//找到大于等于 m 的最小素数
int findNextPrime(int m)
{
    while (isPrime(m) == 0)
    {
        m++;
    }
    return m;
}
