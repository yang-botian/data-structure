#include <stdio.h>
#include "Other.h"
#include "List.h"
#include "Link.h"
#include "BST.h"
#include "Open_Address_HashTable.h"
#include "Chain_HashTable.h"
#define  SEARCHWORD "some"

void Print_Write(char* filename)
{
    printf("*****************************************\n");
    printf("**                                     **\n");
    printf("**  Write   %-15s success    **\n", filename);
    printf("**                                     **\n");
    printf("*****************************************\n");

}

int main()
{
    //读取文件并解析单词
    char words[MAX_WORDS][MAX_WORD_LEN];
    int word_count = read_words("D:\\Testing\\InFile.txt", words);
    if (word_count < 0)
    {
        printf("Failed to read file.\n");
        return 1;
    }
    printf("read InFile.txt success\n");
    printf("Total words read: %d\n", word_count);

    //1、顺序表的统计和输出
    List list = { .size = 0 };
    for (int i = 0; i < word_count; i++)
    {
        List_Insert(&list, words[i]);
    }
    //写入文件
    Write_List_To_File(&list, "D:\\Testing\\List.txt");
    Print_Write("List.txt");

    //2、链表的统计和输出
    LinkList* link = (LinkList*)malloc(sizeof(LinkList));
    link->head = NULL;
    link->size = 0;
    for (int i = 0; i < word_count; i++)
    {
        Link_Insert(link, words[i]);
    }
    //写入文件
    Write_Link_To_File(link, "D:\\Testing\\Link.txt");
    Print_Write("Link.txt");

    //3、二叉排序树统计和输出
    BSTTree BST_tree;
    BST_tree.root = NULL;
    BST_tree.size = 0;
    //依次建立二叉排序树
    for (int i = 0; i < word_count; i++)
    {
        BST_tree.root = BST_Insert(BST_tree.root, words[i], 1, &BST_tree);
    }
    //将二叉排序树写入文件
    Write_BST_To_File(BST_tree.root, "D:\\Testing\\BST.txt");
    Print_Write("BST.txt");

    //4、开放地址法
    int Msize = findNextPrime(word_count);
    Open_HashTable** open_hashTable = (Open_HashTable**)malloc(Msize * sizeof(Open_HashTable*));
    //初始化
    Init_Open_Address_HashTable(open_hashTable, Msize);
    //插入散列表中
    for (int i = 0; i < word_count; i++)
    {
        Open_Address_HashTable_Insert(open_hashTable, Msize, words[i]);
    }
    //写入文件
    Write_Open_Address_HashTable_To_File(open_hashTable, Msize, "D:\\Testing\\OpenHash.txt");
    Print_Write("OpenHash.txt");


    //5、链地址法
    //可以用同一个Msize
    Chained_HashTable* chain_hashTable = (Chained_HashTable*)malloc(Msize * sizeof(Chained_HashTable));
    //初始化
    Init_Chain_HashTable(chain_hashTable, Msize);
    //插入哈希表
    for (int i = 0; i < word_count; i++)
    {
        Chain_HashTable_Insert(chain_hashTable, words[i]);
    }
    //写入文件
    Write_Chain_HashTable_To_File(chain_hashTable, "D:\\Testing\\ChainHash.txt");
    Print_Write("ChainHash.txt");
    

    printf("\n########################################\n");
    printf("下面将进行六种查找算法：\n");
    printf("1、顺序表的顺序查找：\n");
    printf("Press any key to continue...");
    getchar();
    List_Search(&list, SEARCHWORD, "D:\\Testing\\List_Search.txt");
    printf("2、顺序表的二分查找：\n");
    printf("Press any key to continue...");
    getchar();
    List_Binary_Search(&list, SEARCHWORD, "D:\\Testing\\List_Binary_Search.txt");
    printf("3、链表的顺序查找：\n");
    printf("Press any key to continue...");
    getchar();
    Link_Search(link, SEARCHWORD, "D:\\Testing\\Link_Search.txt");
    printf("4、二叉排序树的查找：\n");
    printf("Press any key to continue...");
    getchar();
    BST_Search(&BST_tree, SEARCHWORD, "D:\\Testing\\BST_Search.txt");
    printf("5、开放地址法的哈希表的查找：\n");
    printf("Press any key to continue...");
    getchar();
    Open_Address_HashTable_Search(open_hashTable, SEARCHWORD, Msize, "Open_HashTable.txt");
    printf("6、拉链法的哈希表的查找：\n");
    printf("Press any key to continue...");
    getchar();
    Chain_HashTable_Search(chain_hashTable, SEARCHWORD, "Chain_HashTable.txt");

    //链表释放空间
    Link_Free(link);
    //二叉排序树释放空间
    Free_BSTTree(&BST_tree);
    //释放空间
    Free_Open_Address_HashTable(open_hashTable, Msize);
    //释放空间
    Free_Chain_HashTable(chain_hashTable);
}
