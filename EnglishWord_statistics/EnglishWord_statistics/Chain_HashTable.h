#pragma once
#include"Other.h"
#include<stdio.h>
#include<stdlib.h>

typedef struct Node 
{
    char word[MAX_WORD_LEN];  //单词
    int frequency;            //词频
    struct Node* next;        //指向下一个节点
} Node;

typedef struct ChainHashTable 
{
    Node** table;             //哈希表数组
    int size;                 //哈希表大小
} Chained_HashTable;

//哈希函数
extern int HashFunction(char* key, int size);

//初始化
void Init_Chain_HashTable(Chained_HashTable* hashTable, int size);
//插入
void Chain_HashTable_Insert(Chained_HashTable* hashTable, char* word);
//将内容写入文件
void Write_Chain_HashTable_To_File(Chained_HashTable* hashTable, char* filename);
//查找ASL
void Chain_HashTable_Search(Chained_HashTable* hashTable, char* search, char* filename);
//释放
void Free_Chain_HashTable(Chained_HashTable* hashTable);