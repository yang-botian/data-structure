#ifndef ROUTE_H
#define ROUTE_H

#define MAX_NAME_LEN 100
#define MAX_ROUTES 200  // 定义最大路线数量

// 路线结构体
typedef struct 
{
	char start_spot[MAX_NAME_LEN]; // 起始景点名称
	char end_spot[MAX_NAME_LEN];   // 结束景点名称
	float distance;                // 距离
	float cost;                    // 费用
} Route;

//从文件中读取路线数据
int load_routes_from_file(Route* routes);
//添加路线到文件中
void add_route_to_file(Route* new_route);
//展示路线信息
void show_route_info(Route route);

#endif // ROUTE_H

