#include <stdio.h>
#include <string.h>
#include "spot.h"

// 从文件加载景点数据
int load_spots_from_file(Spot* spots) 
{
	FILE* file = fopen("spots.txt", "r");
	if (file == NULL) 
	{
		printf("无法打开景点数据文件!\n");
		return 0;
	}
	
	int spot_count = 0;
	while (fscanf(file, "%99[^,],%d,%f,%49[^,],%49[^,],%199[^\n]\n", 
		spots[spot_count].name,
		&spots[spot_count].max_flow,
		&spots[spot_count].ticket_price,
		spots[spot_count].open_time,
		spots[spot_count].spot_type,
		spots[spot_count].description) != EOF) 
	{
		spot_count++;
		if (spot_count >= MAX_SPOTS) 
		{
			break;  //超过最大景点数量
		}
	}
	
	fclose(file);
	return spot_count;
}

// 显示景点信息
void show_spot_info(Spot spot) 
{
	printf("景点名称: %s\n", spot.name);
	printf("最大客流量: %d人\n", spot.max_flow);
	printf("票价: %.2f元\n", spot.ticket_price);
	printf("开放时间: %s\n", spot.open_time);
	printf("景点类型: %s\n", spot.spot_type);
	printf("景点简介: %s\n", spot.description);
}

// 根据名称查找景点
Spot* find_spot_by_name(Spot* spots, int spot_count, const char* name) 
{
	for (int i = 0; i < spot_count; i++) 
	{
		if (strcmp(spots[i].name, name) == 0) 
		{
			return &spots[i];  // 返回找到的景点的地址
		}
	}
	return NULL;  // 如果未找到，返回 NULL
}


// 向文件中添加景点
void add_spot_to_file(Spot* new_spot) 
{
	FILE* file = fopen("spots.txt", "a");  //以追加模式打开文件
	if (file == NULL) 
	{
		printf("无法打开景点数据文件!\n");
		return;
	}
	
	// 将新的景点信息写入文件
	fprintf(file, "%s,%d,%.2f,%s,%s,%s\n",
		new_spot->name,
		new_spot->max_flow,
		new_spot->ticket_price,
		new_spot->open_time,
		new_spot->spot_type,
		new_spot->description);
	
	fclose(file);
	printf("景点添加成功!\n");
}
