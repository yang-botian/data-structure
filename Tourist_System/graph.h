#ifndef GRAPH_H
#define GRAPH_H

#include "spot.h"
#include "route.h"

#define MAX_SPOTS 100

// 图结构
typedef struct 
{
	Spot spots[MAX_SPOTS];                    // 存储所有景点的信息
	Route routes[MAX_SPOTS][MAX_SPOTS];       // 存储景点之间的所有路径
	float distance_matrix[MAX_SPOTS][MAX_SPOTS]; // 存储景点之间的距离
	int spot_count;  // 图中景点的数量
} Graph;

// 函数声明
void init_graph(Graph* graph, int spot_count);
void add_route(Graph* graph, int start, int end, float distance, float cost);
// Dijkstra算法的声明
void dijkstra(Graph* graph, int start, int end, int* path, float* distance);

#endif // GRAPH_H

