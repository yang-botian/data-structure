#ifndef OTHER_H
#define OTHER_H

#include "spot.h"
#include "route.h"
#include "graph.h"

// 推荐附近景点
void recommend_nearby_spots(Spot* spots, int spot_count, int current_spot_index, const char* visitor_type);

// 路径规划
void plan_route(Spot* spots, int spot_count, Route* routes, int route_count, Graph* graph, const char* start_spot, const char* end_spot);

// 游览规划
void plan_tour(Spot* spots, int spot_count, Graph* graph, const char* start_spot, float max_time, float max_cost);

#endif

