#include <stdio.h>
#include <string.h>
#include <float.h>

#include "graph.h"

// 初始化图结构
void init_graph(Graph* graph, int spot_count) {
	graph->spot_count = spot_count;
	// 初始化所有景点之间的距离为无穷大
	for (int i = 0; i < spot_count; i++) {
		for (int j = 0; j < spot_count; j++) {
			graph->distance_matrix[i][j] = FLT_MAX;  // 默认没有路径连接
		}
	}
	// 初始化每个景点的路线结构（假设routes为一个二维数组，存储路径信息）
	for (int i = 0; i < spot_count; i++) {
		for (int j = 0; j < spot_count; j++) {
			graph->routes[i][j].distance = FLT_MAX;  // 路线的距离初始化为无穷大
			graph->routes[i][j].cost = FLT_MAX;     // 路线的费用初始化为无穷大
		}
	}
}

// 添加路线到图结构
void add_route(Graph* graph, int start_index, int end_index, float distance, float cost) 
{
	if (start_index == -1 || end_index == -1) 
	{
		printf("无效的景点索引!\n");
		return;
	}
	
	// 更新距离矩阵和路线信息
	graph->distance_matrix[start_index][end_index] = distance;
	graph->distance_matrix[end_index][start_index] = distance;  // 假设路径是双向的
	
	graph->routes[start_index][end_index].distance = distance;
	graph->routes[start_index][end_index].cost = cost;
	graph->routes[end_index][start_index].distance = distance;
	graph->routes[end_index][start_index].cost = cost;
}

// Dijkstra算法 - 计算最短路径
void dijkstra(Graph* graph, int start, int end, int* path, float* total_distance) 
{
	int spot_count = graph->spot_count;
	float dist[spot_count];  // 存储从起点到每个节点的最短距离
	int prev[spot_count];    // 存储每个节点的前驱节点
	int visited[spot_count]; // 标记节点是否被访问过
	
	// 初始化
	for (int i = 0; i < spot_count; i++) 
	{
		dist[i] = FLT_MAX;
		prev[i] = -1;
		visited[i] = 0;
	}
	dist[start] = 0; // 起点到起点的距离为0
	
	// Dijkstra算法主循环
	for (int i = 0; i < spot_count; i++) 
	{
		// 寻找未访问的最短距离节点
		int u = -1;
		float min_dist = FLT_MAX;
		for (int j = 0; j < spot_count; j++) 
		{
			if (!visited[j] && dist[j] < min_dist) 
			{
				u = j;
				min_dist = dist[j];
			}
		}
		
		if (u == -1) 
			break;  // 所有节点已访问过，退出循环
		
		visited[u] = 1;
		
		// 更新邻接节点的距离
		for (int v = 0; v < spot_count; v++) 
		{
			if (graph->distance_matrix[u][v] != FLT_MAX && !visited[v]) 
			{
				float alt = dist[u] + graph->distance_matrix[u][v];
				if (alt < dist[v]) 
				{
					dist[v] = alt;
					prev[v] = u;
				}
			}
		}
	}
	
	*total_distance = dist[end]; // 记录最短路径的总距离
	
	// 构建路径
	int path_len = 0;
	for (int u = end; u != -1; u = prev[u]) 
	{
		path[path_len++] = u;
	}
	
	// 如果路径不为空，则反转路径数组（因为是从终点向起点回溯的）
	if (path_len > 1) 
	{
		for (int i = 0; i < path_len / 2; i++) 
		{
			int temp = path[i];
			path[i] = path[path_len - 1 - i];
			path[path_len - 1 - i] = temp;
		}
	}
}

