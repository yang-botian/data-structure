#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "spot.h"
#include "route.h"
#include "graph.h"
#include "other.h"

int main() 
{
	Spot spots[MAX_SPOTS] = {0};  // 初始化所有景点为零
	Route routes[MAX_ROUTES] = {0};  // 初始化所有路线为零
	Graph graph;
	
	// 加载景点和路线
	int spot_count = load_spots_from_file(spots);
	int route_count = load_routes_from_file(routes);
	
	if (spot_count < 0 || route_count < 0) 
	{
		printf("加载文件失败！\n");
		return -1;
	}
	
	// 初始化图结构
	init_graph(&graph, spot_count);
	printf("初始化图成功");
	// 添加路线到图结构
	for (int i = 0; i < route_count; i++) 
	{
		int start = -1, end = -1;
		
		// 根据景点名称获取对应的索引
		for (int j = 0; j < spot_count; j++) 
		{
			if (strcmp(spots[j].name, routes[i].start_spot) == 0) 
			{
				start = j;
			}
			if (strcmp(spots[j].name, routes[i].end_spot) == 0) 
			{
				end = j;
			}
		}
		
		// 如果找到了有效的起点和终点，则添加路线
		if (start != -1 && end != -1) 
		{
			add_route(&graph, start, end, routes[i].distance, routes[i].cost);
		} else 
		{
			if (start == -1) 
			{
				printf("未找到起始景点 '%s'!\n", routes[i].start_spot);
			}
			if (end == -1) 
			{
				printf("未找到结束景点 '%s'!\n", routes[i].end_spot);
			}
		}
	}
	
	int option;
	while (1) 
	{
		printf("\n1. 查看景点信息\n2. 添加景点\n3. 查看路线信息\n4. 添加路线\n5. 规划路径\n6. 规划游览\n7. 推荐附近景点\n8. 退出\n");
		printf("请输入选择: ");
		scanf("%d", &option);
		switch (option) 
		{
		case 1:
			{
				char name[MAX_NAME_LEN];
				printf("请输入景点名称: ");
				getchar();  // 清除输入缓冲区
				fgets(name, sizeof(name), stdin);
				name[strcspn(name, "\n")] = 0; // 去掉换行符
				
				Spot* spot = find_spot_by_name(spots, spot_count, name);  // 使用指针来接收返回值
				if (spot != NULL) {  // 检查是否找到了景点
					show_spot_info(*spot);  // 通过解引用指针来访问结构体内容
				} else {
					printf("未找到景点 '%s'\n", name);
				}
				break;
			}
			
			case 2: 
			{
				// 添加景点
				Spot new_spot;
				printf("请输入景点名称: ");
				getchar();  // 清除输入缓冲区
				fgets(new_spot.name, sizeof(new_spot.name), stdin);
				new_spot.name[strcspn(new_spot.name, "\n")] = 0;
				
				printf("请输入最大客流量: ");
				scanf("%d", &new_spot.max_flow);
				
				printf("请输入票价: ");
				scanf("%f", &new_spot.ticket_price);
				
				printf("请输入开放时间: ");
				getchar();  // 清除输入缓冲区
				fgets(new_spot.open_time, sizeof(new_spot.open_time), stdin);
				new_spot.open_time[strcspn(new_spot.open_time, "\n")] = 0;
				
				printf("请输入景点类型: ");
				fgets(new_spot.spot_type, sizeof(new_spot.spot_type), stdin);
				new_spot.spot_type[strcspn(new_spot.spot_type, "\n")] = 0;
				
				printf("请输入景点简介: ");
				fgets(new_spot.description, sizeof(new_spot.description), stdin);
				new_spot.description[strcspn(new_spot.description, "\n")] = 0;
				
				add_spot_to_file(&new_spot);
				break;
			}
			
			case 3: {
				// 查看路线信息
				char start[MAX_NAME_LEN], end[MAX_NAME_LEN];
				printf("请输入起始景点名称: ");
				getchar();  // 清除输入缓冲区
				fgets(start, sizeof(start), stdin);
				start[strcspn(start, "\n")] = 0;
				
				printf("请输入结束景点名称: ");
				fgets(end, sizeof(end), stdin);
				end[strcspn(end, "\n")] = 0;
				
				for (int i = 0; i < route_count; i++) {
					if (strcmp(routes[i].start_spot, start) == 0 && strcmp(routes[i].end_spot, end) == 0) {
						show_route_info(routes[i]);
					}
				}
				break;
			}
			
			case 4: {
				// 添加路线
				Route new_route;
				printf("请输入起始景点名称: ");
				getchar();  // 清除输入缓冲区
				fgets(new_route.start_spot, sizeof(new_route.start_spot), stdin);
				new_route.start_spot[strcspn(new_route.start_spot, "\n")] = 0;
				
				printf("请输入结束景点名称: ");
				fgets(new_route.end_spot, sizeof(new_route.end_spot), stdin);
				new_route.end_spot[strcspn(new_route.end_spot, "\n")] = 0;
				
				printf("请输入距离: ");
				scanf("%f", &new_route.distance);
				
				printf("请输入费用: ");
				scanf("%f", &new_route.cost);
				
				add_route_to_file(&new_route);
				break;
			}
			
			case 5: {
				// 路径规划
				char start_spot[MAX_NAME_LEN], end_spot[MAX_NAME_LEN];
				printf("请输入起始景点名称: ");
				getchar();  // 清除输入缓冲区
				fgets(start_spot, sizeof(start_spot), stdin);
				start_spot[strcspn(start_spot, "\n")] = 0;
				
				printf("请输入结束景点名称: ");
				fgets(end_spot, sizeof(end_spot), stdin);
				end_spot[strcspn(end_spot, "\n")] = 0;
				
				plan_route(spots, spot_count, routes, route_count, &graph, start_spot, end_spot);
				break;
			}
			
			case 6: {
				// 游览规划
				char start_spot[MAX_NAME_LEN];
				float max_time, max_cost;
				printf("请输入起始景点名称: ");
				getchar();  // 清除输入缓冲区
				fgets(start_spot, sizeof(start_spot), stdin);
				start_spot[strcspn(start_spot, "\n")] = 0;
				
				printf("请输入最大游览时间（小时）: ");
				scanf("%f", &max_time);
				
				printf("请输入最大费用（元）: ");
				scanf("%f", &max_cost);
				
				plan_tour(spots, spot_count, &graph, start_spot, max_time, max_cost);
				break;
			}
			
			case 7: {
				// 推荐附近景点
				int current_spot_index;
				char visitor_type[MAX_TYPE_LEN];
				printf("请输入当前景点编号: ");
				scanf("%d", &current_spot_index);
				
				printf("请输入游客类型（如家庭、背包客等）: ");
				getchar();  // 清除输入缓冲区
				fgets(visitor_type, sizeof(visitor_type), stdin);
				visitor_type[strcspn(visitor_type, "\n")] = 0;
				
				recommend_nearby_spots(spots, spot_count, current_spot_index, visitor_type);
				break;
			}
			
		case 8:
			printf("退出程序\n");
			exit(0);
		}
	}
	
	return 0;
}

