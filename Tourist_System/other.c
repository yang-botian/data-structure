#include <stdio.h>
#include <string.h>
#include <float.h>
#include "other.h"
#include "graph.h"

// 推荐附近景点
void recommend_nearby_spots(Spot* spots, int spot_count, int current_spot_index, const char* visitor_type) {
	printf("根据您是 %s 游客，以下是推荐的附近景点：\n", visitor_type);
	
	// 基本推荐：简单按景点的最大客流量来排序（示例）
	for (int i = 0; i < spot_count; i++) {
		if (i != current_spot_index) {
			printf("景点名称: %s\n", spots[i].name);
			printf("票价: %.2f元\n", spots[i].ticket_price);
			printf("最大客流量: %d\n", spots[i].max_flow);
			printf("景点类型: %s\n", spots[i].spot_type);
			printf("景点简介: %s\n\n", spots[i].description);
		}
	}
}

// 规划路线：通过Dijkstra算法计算最短路径
void plan_route(Spot* spots, int spot_count, Route* routes, int route_count, Graph* graph, const char* start_spot, const char* end_spot) {
	int start_index = -1, end_index = -1;
	
	// 查找起始景点和结束景点的索引
	for (int i = 0; i < spot_count; i++) 
	{
		if (strcmp(spots[i].name, start_spot) == 0) 
		{
			start_index = i;
		}
		if (strcmp(spots[i].name, end_spot) == 0) 
		{
			end_index = i;
		}
	}
	
	if (start_index == -1 || end_index == -1) 
	{
		printf("未找到起始或结束景点，请检查输入!\n");
		return;
	}
	
	// 获取起始点到结束点的最短路径（通过中转景点）
	int path[MAX_SPOTS];
	float distance = 0.0;
	dijkstra(graph, start_index, end_index, path, &distance);
	
	if (distance == FLT_MAX) {
		printf("无法从 '%s' 到 '%s'，没有路径连接！\n", start_spot, end_spot);
		return;
	}
	
	// 打印路径
	printf("从 '%s' 到 '%s' 的最短路径是：\n", start_spot, end_spot);
	
	// 检查路径的输出，避免重复的起点或终点
	int path_index = 0;
	while (path[path_index] != -1) {
		printf("%s", spots[path[path_index]].name);
		path_index++;
		if (path[path_index] != -1) {
			printf(" -> ");
		}
	}
	printf("\n总距离: %.2f km\n", distance);
}

// 游览规划：根据时间和费用，规划最优游览方案
void plan_tour(Spot* spots, int spot_count, Graph* graph, const char* start_spot, float max_time, float max_cost) {
	int start_index = -1;
	
	// 查找景点的索引
	for (int i = 0; i < spot_count; i++) {
		if (strcmp(spots[i].name, start_spot) == 0) {
			start_index = i;
			break;
		}
	}
	
	if (start_index == -1) {
		printf("未找到起始景点!\n");
		return;
	}
	
	printf("从 %s 开始的游览规划：\n", start_spot);
	
	// 先简单做一个示例，只根据距离和费用来规划
	printf("最多游览 %.2f 小时和 %.2f 元的景点。\n", max_time, max_cost);
	
	// 假设每个景点的游览时间是固定的，并且根据景点之间的距离计算预计的游览时间
	float total_time = 0.0;
	float total_cost = 0.0;
	
	// 规划一个简单的路径（实际应用中应该使用贪心算法或动态规划来解决）
	for (int i = 0; i < spot_count; i++) {
		if (i != start_index) {
			// 估算游览时间（假设速度为5公里每小时）
			float travel_time = graph->distance_matrix[start_index][i] / 5.0;
			float spot_cost = spots[i].ticket_price;
			
			// 检查是否超出时间和费用限制
			if (total_time + travel_time > max_time || total_cost + spot_cost > max_cost) {
				printf("超过最大时间或费用限制，规划结束。\n");
				break;
			}
			
			// 更新总时间和费用
			total_time += travel_time;
			total_cost += spot_cost;
			
			printf("游览景点: %s, 预计游览时间: %.2f小时, 票价: %.2f元\n", spots[i].name, travel_time, spot_cost);
		}
	}
	printf("总游览时间: %.2f小时, 总费用: %.2f元\n", total_time, total_cost);
}
