#include <stdio.h>
#include <string.h>
#include "route.h"

//从文件加载路线数据
int load_routes_from_file(Route* routes) 
{
	FILE* file = fopen("routes.txt", "r");
	if (file == NULL) 
	{
		printf("无法打开路线数据文件!\n");
		return 0;
	}
	
	int route_count = 0;
	while (fscanf(file, "%[^,],%[^,],%f,%f\n", 
		routes[route_count].start_spot,
		routes[route_count].end_spot,
		&routes[route_count].distance,
		&routes[route_count].cost) != EOF) 
	{
		route_count++;
		if (route_count >= MAX_ROUTES) 
		{
			break;  // 超过最大路线数量
		}
	}
	
	fclose(file);
	return route_count;
}

//向文件中添加路线
void add_route_to_file(Route* new_route) 
{
	FILE* file = fopen("routes.txt", "a");  //以追加模式打开文件
	if (file == NULL) 
	{
		printf("无法打开路线数据文件!\n");
		return;
	}
	
	// 将新的路线信息写入文件
	fprintf(file, "%s,%s,%.2f,%.2f\n",
		new_route->start_spot,
		new_route->end_spot,
		new_route->distance,
		new_route->cost);
	
	fclose(file);
	printf("路线添加成功!\n");
}

//显示路线信息
void show_route_info(Route route) 
{
	printf("起始景点: %s\n", route.start_spot);
	printf("结束景点: %s\n", route.end_spot);
	printf("距离: %.2f km\n", route.distance);
	printf("费用: %.2f元\n", route.cost);
}

