#ifndef SPOT_H
#define SPOT_H

// 定义常量
#define MAX_NAME_LEN 100    // 景点名称最大长度
#define MAX_TYPE_LEN 50     // 景点类型最大长度
#define MAX_DESC_LEN 200    // 景点简介最大长度
#define MAX_SPOTS 100       // 定义最大景点数量

// 景点结构体
typedef struct 
{
	char name[MAX_NAME_LEN];        // 景点名称
	int max_flow;                   // 最大客流量
	float ticket_price;             // 票价
	char open_time[MAX_NAME_LEN];   // 开放时间
	char spot_type[MAX_TYPE_LEN];   // 景点类型
	char description[MAX_DESC_LEN]; // 景点简介
} Spot;

// 从文件加载景点数据
int load_spots_from_file(Spot* spots);
// 向文件中添加景点
void add_spot_to_file(Spot* new_spot);
// 显示景点信息
void show_spot_info(Spot spot);
// 根据名称查找景点
Spot* find_spot_by_name(Spot* spots, int spot_count, const char* name);

#endif // SPOT_H

