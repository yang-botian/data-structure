//链队列的实现
#include<iostream>
using namespace std;

//初始化定义
#define OK 1
#define ERROR 0
#define OVERFLOW -1
//Status是函数返回值类型，其值是函数结果状态代码
typedef int Status;

typedef int QElemType;
typedef struct QNode
{
	QElemType data;
	struct QNode* next;
}QNode, *QueuePtr;

typedef struct
{
	QueuePtr front;   //队头指针
	QueuePtr rear;    //队尾指针
}LinkQueue;

//队列初始化
Status InitQueue(LinkQueue& Q)
{
	//构造一个空队列
	Q.front = Q.rear = new QNode;  //生成新结点作为头结点，队头和队尾指针指向此结点
	Q.front->next = NULL;   //头结点的指针域置空
	return OK;
}

//销毁队列
Status DestroyQueue(LinkQueue& Q)
{
	while (Q.front)
	{
		Q.rear = Q.front->next;
		delete Q.front;
		Q.front = Q.rear;
	}
	return OK;
}

//求队列中元素数量
int QueueLength(LinkQueue Q)
{
	int count = 0;
	QNode* p = Q.front->next;
	while (p)
	{
		count++;
		p = p->next;
	}
	return count;
}

//入队
Status EnQueue(LinkQueue& Q, QElemType e)
{
	//插入元素e为Q的新的队尾元素
	QNode* p = new QNode;
	p->data = e;
	p->next = NULL;
	Q.rear->next = p;   //将新结点插入队尾
	Q.rear = p;         //修改队尾指针
	return OK;
}

//出队
Status DeQueue(LinkQueue& Q, QElemType& e)
{
	//删除队头元素，用e返回其值
	if (Q.front == Q.rear)
		return ERROR;   //若队列为空，返回ERROR
	QNode* p = Q.front->next;
	e = p->data;  //用e保存头结点数据
	Q.front->next = p->next;  //修改头结点指针域
	if (Q.rear == p)
		Q.rear = Q.front;  //最后一个元素被删除，队尾指针指向头结点
	delete p;   //释放原队头元素的空间
	return OK;
}

//取队头元素
QElemType GetHead(LinkQueue Q)
{
	//返回Q的队头元素，不修改头指针
	if (Q.front != Q.rear)
		return Q.front->next->data;  //返回头元素的值，队头元素不变
}

//输出队列元素
void PrintQueue(LinkQueue Q)
{
	//前提队列不为空
	printf("(front) ");
	if (Q.front != Q.rear)
	{
		QNode* p = Q.front->next;
		while (p != NULL)
		{
			printf("%d ", p->data);
			p = p->next;
		}
	}
	printf("(rear)\n");
}

int main()
{
	LinkQueue Q;
	QElemType e;

	cout << "初始化队列..." << endl;
	if (InitQueue(Q) == OK)
		cout << "队列初始化成功！" << endl;
	else
		cout << "队列初始化失败！" << endl;

	cout << "\n测试入队操作：" << endl;
	for (int i = 1; i <= 5; i++)
	{
		if (EnQueue(Q, i) == OK)
			cout << i << " 入队成功" << endl;
		else
			cout << i << " 入队失败" << endl;
	}

	cout << "\n当前队列：" << endl;
	PrintQueue(Q);

	cout << "\n队列长度：" << QueueLength(Q) << endl;

	cout << "\n测试出队操作：" << endl;
	for (int i = 0; i < 3; i++)
	{
		if (DeQueue(Q, e) == OK)
			cout << e << " 出队成功" << endl;
		else
			cout << "出队失败，队列可能为空" << endl;
	}

	cout << "\n当前队列：" << endl;
	PrintQueue(Q);

	cout << "\n队列长度：" << QueueLength(Q) << endl;

	cout << "\n队头元素：" << GetHead(Q) << endl;

	cout << "\n销毁队列..." << endl;
	if (DestroyQueue(Q) == OK)
		cout << "队列销毁成功！" << endl;
	else
		cout << "队列销毁失败！" << endl;

	return 0;
}