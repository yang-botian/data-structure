//#include<iostream>
//using namespace std;
//
////初始化定义
//#define OK 1
//#define ERROR 0
//#define OVERFLOW -1
////Status是函数返回值类型，其值是函数结果状态代码
//typedef int Status;
//
//#define MAXQSIZE 100  //队列可能达到的最大长度
//typedef int QElemType;
//typedef struct
//{
//	QElemType* base;  //存储空间的基地址
//	int front;        //头指针
//	int rear;         //尾指针
//}SqQueue;
//
////队空的条件：Q.front == Q.rear
////队满的条件：(Q.rear + 1) % MAXQSIZE == Q.front
//
////队列初始化
//Status InitQueue(SqQueue& Q)
//{
//	//构造一个空队列Q
//	Q.base = new QElemType[MAXQSIZE];  //为队列分配一个最大容量为MAXQSIZE的数组空间
//  //Q.base = (int*)malloc(MAXQSIZE * sizeof(int));
//	if (!Q.base)
//		exit(OVERFLOW);      //如果开辟失败就退出程序
//	Q.front = Q.rear = 0;    //头尾指针指向0，表示队列为空
//	return OK;
//}
//
//// 销毁队列
//Status DestroyQueue(SqQueue& Q)
//{
//	if (Q.base) {
//		delete[] Q.base;
//		Q.base = NULL;
//		Q.front = Q.rear = 0;
//	}
//	return OK;
//}
//
////求队列长度
//int QueueLength(SqQueue Q)
//{
//	//返回队列元素个数
//	return (Q.rear - Q.front + MAXQSIZE) % MAXQSIZE;
//}
//
////入队
//Status EnQueue(SqQueue& Q, QElemType e)
//{
//	//插入元素e为Q的新的队尾元素
//	if ((Q.rear + 1) % MAXQSIZE == Q.front)
//		return ERROR;    //若尾指针在循环意义上加1后等于头指针，表明队满
//	Q.base[Q.rear] = e;     //新元素插入队尾
//	Q.rear = (Q.rear + 1) % MAXQSIZE;     //队尾指针加1
//	return OK;
//}
//
////出队
//Status DeQueue(SqQueue& Q, QElemType& e)
//{
//	//删除队头元素，用e返回其值
//	if (Q.front == Q.rear)
//		return ERROR;  //队空
//	e = Q.base[Q.front];
//	Q.front = (Q.front + 1) % MAXQSIZE;
//	return OK;
//}
//
////取队头元素
//QElemType GetHead(SqQueue Q)
//{
//	//返回队头元素，不改变头指针
//	if (Q.front != Q.rear)  //队列非空
//		return Q.base[Q.front];
//}
//
////输出队列元素
//void PrintQueue(SqQueue& Q)
//{
//	printf("(front) ");
//	int i;
//	for (i = Q.front; i != Q.rear; i++)
//	{
//		printf("%d ", Q.base[i]);
//	}
//	printf("(rear)\n");
//}
//
//int main()
//{
//	SqQueue Q;
//	QElemType e;
//
//	cout << "初始化队列..." << endl;
//	if (InitQueue(Q) == OK)
//		cout << "队列初始化成功！" << endl;
//	else
//		cout << "队列初始化失败！" << endl;
//
//	cout << "\n测试入队操作：" << endl;
//	for (int i = 1; i <= 5; i++)
//	{
//		if (EnQueue(Q, i) == OK)
//			cout << i << " 入队成功" << endl;
//		else
//			cout << i << " 入队失败" << endl;
//	}
//
//	cout << "\n当前队列：" << endl;
//	PrintQueue(Q);
//
//	cout << "\n队列长度：" << QueueLength(Q) << endl;
//
//	cout << "\n测试出队操作：" << endl;
//	for (int i = 0; i < 3; i++)
//	{
//		if (DeQueue(Q, e) == OK)
//			cout << e << " 出队成功" << endl;
//		else
//			cout << "出队失败，队列可能为空" << endl;
//	}
//
//	cout << "\n当前队列：" << endl;
//	PrintQueue(Q);
//
//	cout << "\n队列长度：" << QueueLength(Q) << endl;
//
//	cout << "\n队头元素：" << GetHead(Q) << endl;
//
//	cout << "\n销毁队列..." << endl;
//	if (DestroyQueue(Q) == OK)
//		cout << "队列销毁成功！" << endl;
//	else
//		cout << "队列销毁失败！" << endl;
//
//	return 0;
//}