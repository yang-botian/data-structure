//7-6 重排链表
#define _CRT_SECURE_NO_WARNINGS

#include<stdio.h>

typedef struct stu
{
	int data;
	int next;
}stu;

stu site[100001];

int main()
{
	int head, n;
	scanf("%d %d", &head, &n);
	int f_node, num, l_node;
	for (int i = 0; i < n; i++)
	{
		scanf("%d %d %d", &f_node, &num, &l_node);
		site[f_node].data = num;
		site[f_node].next = l_node;
	}

	int arr[6];
	int k = 0;//用来记录数组下标
	for (int i = head; i != -1; i = site[i].next)
	{
		arr[k] = i;  //按照位置排好序
		k++;
	}
	int i = 0, j = k - 1;
	while (i <= j) 
	{
		if (j == i || j - i == 1) 
		{
			if (j == i)
				printf("%05d %d -1\n", arr[i], site[arr[i]].data);
			else 
			{
				printf("%05d %d %05d\n", arr[j], site[arr[j]].data, arr[i]);
				printf("%05d %d -1\n", arr[i], site[arr[i]].data);
			}
		}
		else 
		{
			printf("%05d %d %05d\n", arr[j], site[arr[j]].data, arr[i]);
			printf("%05d %d %05d\n", arr[i], site[arr[i]].data, arr[j - 1]);
		}
		i++;
		j--;
	}
	
}