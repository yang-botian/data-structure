//#define _CRT_SECURE_NO_WARNINGS
//
//#include <stdio.h>
//#include <stdlib.h>
//#include <stdbool.h>
//
//typedef struct ListNode {
//    int val;
//    struct ListNode* next;
//}ListNode;
//
//ListNode* deleteDuplicates(struct ListNode* head) 
//{
//    if (!head || !head->next) 
//    {
//        return head;
//    }
//
//    ListNode* dummy = (ListNode*)malloc(sizeof(ListNode));
//    dummy->next = head;
//
//    ListNode* current = dummy;
//    ListNode* to_delete_head = NULL;
//    ListNode* to_delete_tail = NULL;
//
//    bool duplicates[100001] = { false };
//
//    while (current->next) {
//        if (duplicates[abs(current->next->val)]) {
//            // Add the node to the to_delete list
//            ListNode* temp = current->next;
//            current->next = current->next->next;
//            temp->next = NULL;
//
//            if (!to_delete_head) {
//                to_delete_head = temp;
//                to_delete_tail = temp;
//            }
//            else {
//                to_delete_tail->next = temp;
//                to_delete_tail = temp;
//            }
//        }
//        else {
//            duplicates[abs(current->next->val)] = true;
//            current = current->next;
//        }
//    }
//
//    // Free the dummy node
//    ListNode* result = dummy->next;
//    free(dummy);
//
//    // Print the unique list
//    while (result) {
//        printf("%d\n", result->val);
//        result = result->next;
//    }
//
//    // Print the to_delete list
//    while (to_delete_head) {
//        printf("%d\n", to_delete_head->val);
//        ListNode* temp = to_delete_head;
//        to_delete_head = to_delete_head->next;
//        free(temp);
//    }
//
//    return dummy->next;
//}
//
//int main() {
//    // Input your test cases here
//    return 0;
//}
