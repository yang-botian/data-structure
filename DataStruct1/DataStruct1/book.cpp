#include<iostream>
#include <cstdlib>
using namespace std;

//控制最大值
#define MAXSiZE 1000
//声明Status用于记录返回结果
typedef int Status;
#define OK 1
#define ERROR 0

//定义书的基本数据项
typedef struct
{
	char no[20];
	char name[50];
	float peice;
}Book;
//定义顺序表的数据元素
typedef struct
{
	Book* elem;
	int length;
}SqList;

//声明顺序表
SqList L;



//创建
Status InitList(SqList &L)
{
	L.elem = new Book[MAXSiZE];
	if (!L.elem)
		exit(EXIT_FAILURE);
	L.length = 0;
	return OK;
}

//销毁
void DestoryList(SqList& L)
{
	if (L.elem)
		delete[]L.elem; //回收指针空间
}

//清空
void Clear(SqList& L)
{
	L.length = 0; //将线性表的表长重置
}

//求线性表的长度
int GetLength(SqList L)
{
	return L.length;
}

//判空
int IsEmpty(SqList L)
{
	if (L.length == 0)
		return 1;
	return 0;
}

//取值
Status GetElem(SqList L, int i, Book& e)
{
	if (i < 1 || i > L.length)
		return ERROR;
	e = L.elem[i - 1];
	return OK;
}

//查找
int LocateElem(SqList L, Book e)
{
	for (int i = 0; i < L.length; i++)
	{
		if (strcmp(L.elem[i].no, e.no) == 0)
			return i + 1;
	}
	return OK;
}

//插入
Status ListInsert(SqList& L, int i, Book e)
{
	if (i < 1 || i > L.length + 1)//插入位置可以是最后一个元素的后面
		return ERROR;
	if (L.length == MAXSiZE)
		return ERROR;
	for (int j = L.length - 1; j >= i - 1; j--)
	{
		L.elem[j + 1] = L.elem[j];
	}
	L.elem[i - 1] = e;
	L.length++;
	return OK;
}

//删除
Status ListDelete(SqList& L, int i, Book& e)
{
	if (i<1 || i>L.length)//判断位置是否合法
	{
		return ERROR;
	}
	e = L.elem[i - 1]; //将删除的值赋予e,达到一个返回的作用
	for (int j = i; j <= L.length - 1; j++)
	{
		L.elem[j - 1] = L.elem[j];
	}
	L.length--;
	return OK;
}

int main()
{
	InitList(L);
	Book book1 = { "001","数据结构",59 };
	ListInsert(L, 1, book1);

	Book book2 = { "002","计算机网络",23.6 };
	ListInsert(L, 1, book2);

	int length = GetLength(L);

	Book book3;
	GetElem(L, 2, book3);
	cout << book3.name<<"  "<< book3.peice << endl;
	cout << length << endl;
}