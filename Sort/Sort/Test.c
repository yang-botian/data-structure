#include"Sort.h"

void TestInsertSort()
{
	int a[] = { 2,5,7,6,8,16,1,30 };
	int size = sizeof(a) / sizeof(a[0]);
	PrintArray(a, size);
	
	InsertSort(a, size);
	PrintArray(a, size);
	printf("============================\n");
}

void TestShellSort()
{
	int a[] = { 2,5,7,6,8,16,1,30 };
	int size = sizeof(a) / sizeof(a[0]);
	PrintArray(a, size);

	ShellSort(a, size);
	PrintArray(a, size);
	printf("============================\n");
}

void TestSelectSort()
{
	int a[] = { 2,5,7,6,8,16,1,30 };
	int size = sizeof(a) / sizeof(a[0]);
	PrintArray(a, size);

	SelectSort(a, size);
	PrintArray(a, size);
	printf("============================\n");
}

void TestBubbleSort()
{
	int a[] = { 2,5,7,6,8,16,1,30 };
	int size = sizeof(a) / sizeof(a[0]);
	PrintArray(a, size);

	BubbleSort(a, size);
	PrintArray(a, size);
	printf("============================\n");
}

void TestHeapSort()
{
	int a[] = { 2,5,7,6,8,16,1,30 };
	int size = sizeof(a) / sizeof(a[0]);
	PrintArray(a, size);

	HeapSort(a, size);
	PrintArray(a, size);
	printf("============================\n");
}

void TestQuickSort()
{
	int a[] = { 2,5,7,6,8,16,1,30 };
	int size = sizeof(a) / sizeof(a[0]);
	PrintArray(a, size);

	QuickSort(a, 0, size - 1);
	PrintArray(a, size);
	printf("============================\n");
}

int main()
{
	TestInsertSort();
	TestShellSort();
	TestSelectSort();
	TestBubbleSort();
	TestHeapSort();
	TestQuickSort();
	return 0;
}