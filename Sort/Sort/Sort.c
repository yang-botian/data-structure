#include"Sort.h"
//打印数据
void PrintArray(int* a, int n)
{
	for (int i = 0; i < n; i++)
	{
		printf("%d ", a[i]);
	}
	printf("\n");
}

void Swap(int* a, int* b)
{
	int tmp = *a;
	*a = *b;
	*b = tmp;
}

//插入排序
//时间复杂度: O(N^2);
void InsertSort(int* a, int n)
{
	for (int i = 0; i < n - 1; i++)//外循环
	{
		//假设[0, end]的数据有序
		int end = i;
		int tmp = a[end + 1];
		//升序排列
		while (end >= 0)
		{
			if (tmp < a[end])
			{
				//向后移位
				a[end + 1] = a[end];
				end--;
			}
			else
			{
				break;
			}
		}
		a[end + 1] = tmp;
	}
}

//希尔排序
void ShellSort(int* a, int n)
{
	int gap = n;
	while (gap > 1)
	{
		//  int gap /= 2;
		gap = gap / 3 + 1;    //神之一手

		//预排序
		// 全局
		for (int i = 0; i < n - gap; i++)
		{
			//单趟
			int end = i;
			int tmp = a[end + gap];
			while (end >= 0)
			{
				if (tmp < a[end])
				{
					a[end + gap] = a[end];
					end -= gap;
				}
				else
				{
					break;
				}
			}
			a[end + gap] = tmp;
		}
	}
}

//选择排序
void SelectSort(int* a, int n)
{
	int begin = 0;
	int end = n - 1;
	while (begin < end)
	{
		int min = begin;
		int max = begin;
		//选出最大值和最小值的下标
		for (int i = begin + 1; i <= end; i++)
		{
			if (a[i] < a[min])
			{
				min = i;
			}
			if (a[i] > a[max])
			{
				max = i;
			}
		}
		Swap(&a[begin], &a[min]);
		//这里需要一个判断，如果最大值和最小值的下标正好交换后重叠，会导致交换出现问题
		if (max == begin)
		{
			max = min;
		}
		Swap(&a[end], &a[max]);
		begin++;
		end--;
	}
}

//冒泡排序
//时间复杂度: O(N^2)
void BubbleSort(int* a, int n)
{
	//全局
	for (int j = 1; j < n; j++)
	{
		//单趟排
		for (int i = 0; i < n - 1; i++)
		{
			if (a[i + 1] < a[i])
			{
				Swap(&a[i + 1], &a[i]);
			}
		}
	}
}

//向下调整算法
void AdjustDown(int* a, int n, int parent)
{
	//假设法，寻找儿子结点
	int child = parent * 2 + 1;
	while (child < n)
	{
		if (child + 1 < n && a[child + 1] > a[child])
		{
			child = parent * 2 + 2;
		}
		if (a[child] > a[parent])
		{
			Swap(&a[child], &a[parent]);
			parent = child;
			child = parent * 2 + 1;
		}
		else
		{
			break;
		}
	}
}

// 堆排序
// 时间复杂度: O(N*logN)
void HeapSort(int* a, int n)
{
	// 建堆 
	// 时间复杂度: O(N)
	// 升序，建大堆
	for (int i = (n - 1 - 1) / 2; i >= 0; i--)
	{
		AdjustDown(a, n, i);
	}
	//进行排序
	//交换首尾数据
	int end = n - 1;
	while (end > 0)
	{
		Swap(&a[end], &a[0]);
		AdjustDown(a, end, 0);
		end--;
	}
}

//三数取中法  left  mid  right(大小为中间的值)
int getMid(int* a, int left, int right)
{
	int mid = (left + right) / 2;
	if (a[left] < a[mid])
	{
		if (a[right] > a[mid])
		{
			return mid;
		}
		else if (a[left] > a[right])
		{
			return left;
		}
		else
		{
			return right;
		}
	}
	else //a[left] > a[mid]
	{
		if (a[right] < a[mid])
		{
			return mid;
		}
		else if(a[right] > a[left])
		{ 
			return left;
		}
		else
		{
			return right;
		}
	}
}

//快排
void QuickSort(int* a, int left, int right)
{
	//只有一个值或者不存在就是最小子问题
	if (left >= right)
		return;

	//小区间优化
	if (right - left + 1 < 10)
	{
		//数据少于十个就走插入排序，减少递归
		InsertSort(a + left, right - left + 1);
	}
	else
	{

		int begin = left;
		int end = right;
		//循环结束的条件 
		//快排的思想: 找key值
		//然后左右同时走，左边的找到大于key的值，右边的找到小于key的值，然后交换
		//不断递归

		//选[left, right]之间中的随机数作为key
		/*int randi = rand() % (right - left);
		randi += left;
		Swap(&a[right], &a[left]);*/

		//三数取中法
		int mid = getMid(a, left, right);
		Swap(&a[left], &a[right]);

		int key = left;
		while (left < right)
		{
			//先让右侧的走，找小值
			while (left < right && a[right] >= a[key])
			{
				right--;
			}
			//之后让左侧的走，找大值
			while (left < right && a[left] <= a[key])
			{
				left++;
			}
			Swap(&a[left], &a[right]);//都找到各自的大值和小值后，交换
		}
		//循环结束后，交换key值和相遇时的值
		Swap(&a[left], &a[key]);
		key = left; //交换后，则key的下标也需要改变

		//递归走下去
		//[begin, left - 1     left + 1 , end]
		QuickSort(a, begin, left - 1);
		QuickSort(a, left + 1, end);
	}
}

//快排 双指针法
void QuickSort2(int* a, int left, int right)
{
	if (left >= right)
		return;
	int key = left;
	int prev = left;
	int cur = left + 1;
	while (cur <= right)
	{
		if (a[cur] < a[key] && ++prev != cur)
		{
			Swap(&a[cur], &a[prev]);
		}
		++cur;
	}
	Swap(&a[prev], &a[key]);
	key = prev;

	//[left, key - 1] key [key + 1, right]
	QuickSort2(a, left, key - 1);
	QuickSort2(a, key + 1, right);
}