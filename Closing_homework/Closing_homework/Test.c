#include<stdio.h>
#include<time.h>
#include"sort.h"
#include"other.h"


#define ARRAY_SIZE 10000  //数组大小
#define TEST_GROUPS 5     //测试次数

int main()
{
	int arr[ARRAY_SIZE];  //测试数组
	int temp[ARRAY_SIZE]; //备份数组
	long compare_count, move_count;
    //  关键字比较次数    关键字移动次数
	clock_t start_time, end_time;
	double times;
	//用于计算排序时间

	printf("排序算法性能比较");

	for (int i = 0; i < TEST_GROUPS; i++)
	{
		printf("\n Test Group %d: \n", i + 1);

		//生成随机数
		random_array(arr, ARRAY_SIZE, i);

		//备份数组
		for (int i = 0; i < ARRAY_SIZE; i++)
		{
			temp[i] = arr[i];
		}

		//一、测试直接插入排序
		//获取排序前的时间
		start_time = clock();

		compare_count = 0;
		move_count = 0;
		Insert_sort(arr, ARRAY_SIZE, &compare_count, &move_count);

		// 获取排序后的时间
		end_time = clock();
		times = ((double)(end_time - start_time)) / CLOCKS_PER_SEC;

		print_results("Insert_sort", compare_count, move_count, times);

		//恢复数组
		for (int i = 0; i < ARRAY_SIZE; i++)
		{
			arr[i] = temp[i];
		}

		//二、测试折半插入排序
		
		//获取排序前的时间
		start_time = clock();

		compare_count = 0;
		move_count = 0;
		Bin_insert_sort(arr, ARRAY_SIZE, &compare_count, &move_count);
		
		// 获取排序后的时间
		end_time = clock();
		times = ((double)(end_time - start_time)) / CLOCKS_PER_SEC;

		print_results("Bin_insert_sort", compare_count, move_count, times);

		//恢复数组
		for (int i = 0; i < ARRAY_SIZE; i++)
		{
			arr[i] = temp[i];
		}

		//三、测试希尔排序
		//获取排序前的时间
		start_time = clock();

		compare_count = 0;
		move_count = 0;
		Shell_sort(arr, ARRAY_SIZE, &compare_count, &move_count);
		
		// 获取排序后的时间
		end_time = clock();
		times = ((double)(end_time - start_time)) / CLOCKS_PER_SEC;

		print_results("Shell_sort", compare_count, move_count, times);

		//恢复数组
		for (int i = 0; i < ARRAY_SIZE; i++)
		{
			arr[i] = temp[i];
		}

		//四、测试二路插入排序
		//获取排序前的时间
		start_time = clock();

		compare_count = 0;
		move_count = 0;
		Two_way_insert_sort(arr, ARRAY_SIZE, &compare_count, &move_count);

		// 获取排序后的时间
		end_time = clock();
		times = ((double)(end_time - start_time)) / CLOCKS_PER_SEC;

		print_results("Two_way_insert_sort", compare_count, move_count, times);

		//恢复数组
		for (int i = 0; i < ARRAY_SIZE; i++)
		{
			arr[i] = temp[i];
		}

		//五、测试冒泡排序

		//获取排序前的时间
		start_time = clock();

		compare_count = 0;
		move_count = 0;
		Bubble_sort(arr, ARRAY_SIZE, &compare_count, &move_count);

		// 获取排序后的时间
		end_time = clock();
		times = ((double)(end_time - start_time)) / CLOCKS_PER_SEC;

		print_results("Bubble_sort", compare_count, move_count, times);

		//恢复数组
		for (int i = 0; i < ARRAY_SIZE; i++)
		{
			arr[i] = temp[i];
		}

		//六、测试快速排序算法

		//获取排序前的时间
		start_time = clock();

		compare_count = 0;
		move_count = 0;
		Quick_sort(arr, 0, ARRAY_SIZE - 1, &compare_count, &move_count);

		// 获取排序后的时间
		end_time = clock();
		times = ((double)(end_time - start_time)) / CLOCKS_PER_SEC;

		print_results("Quick_sort", compare_count, move_count, times);

		//恢复数组
		for (int i = 0; i < ARRAY_SIZE; i++)
		{
			arr[i] = temp[i];
		}

		//七、测试简单选择排序算法
		
		//获取排序前的时间
		start_time = clock();

		compare_count = 0;
		move_count = 0;
		Select_sort(arr, ARRAY_SIZE, &compare_count, &move_count);

		// 获取排序后的时间
		end_time = clock();
		times = ((double)(end_time - start_time)) / CLOCKS_PER_SEC;

		print_results("Select_sort", compare_count, move_count, times);

		//恢复数组
		for (int i = 0; i < ARRAY_SIZE; i++)
		{
			arr[i] = temp[i];
		}

		//八、测试堆排序算法
		
		//获取排序前的时间
		start_time = clock();

		compare_count = 0;
		move_count = 0;
		Heap_sort(arr, ARRAY_SIZE, &compare_count, &move_count);

		// 获取排序后的时间
		end_time = clock();
		times = ((double)(end_time - start_time)) / CLOCKS_PER_SEC;

		print_results("Heap_sort", compare_count, move_count, times);

		//恢复数组
		for (int i = 0; i < ARRAY_SIZE; i++)
		{
			arr[i] = temp[i];
		}

		//九、测试归并排序算法
		//获取排序前的时间
		start_time = clock();

		compare_count = 0;
		move_count = 0;
		Merge_sort(arr, ARRAY_SIZE, &compare_count, &move_count);

		// 获取排序后的时间
		end_time = clock();
		times = ((double)(end_time - start_time)) / CLOCKS_PER_SEC;

		print_results("Merge_sort", compare_count, move_count, times);

		//恢复数组
		for (int i = 0; i < ARRAY_SIZE; i++)
		{
			arr[i] = temp[i];
		}

		//十、测试基数排序算法  比较次数：(n-1)+d*n   移动次数：2*d*n
		//获取排序前的时间
		start_time = clock();

		compare_count = 0;
		move_count = 0;
		Radix_sort(arr, ARRAY_SIZE, &compare_count, &move_count);

		// 获取排序后的时间
		end_time = clock();
		times = ((double)(end_time - start_time)) / CLOCKS_PER_SEC;

		print_results("Radix_sort", compare_count, move_count, times);
	}
	return 0;
}