#include"Sort.h"

//直接插入排序
void Insert_sort(int arr[], int n, long* compare_count, long* move_count)
{
	//外循环
	for (int i = 1; i < n; i++)
	{
		int key = arr[i];//要插入元素
		int j = i - 1;//已经排序的最后一个元素
		//内循环
        while (j >= 0) 
        {
            (*compare_count)++;
            if (arr[j] > key) 
            {
                arr[j + 1] = arr[j];
                (*move_count)++;
                j--;
            }
            else 
            {
                break;
            }
        }
		arr[j + 1] = key;
		(*move_count)++;
	}
}

//折半插入排序
void Bin_insert_sort(int arr[], int n, long* compare_count, long* move_count)
{
    for (int i = 1; i < n; i++)
    {
        int key = arr[i];  //待插入元素
        int low = 0, high = i - 1;

        //折半查找
        while (low <= high)
        {
            (*compare_count)++;
            int mid = (low + high) / 2;
            if (arr[mid] > key)
            {
                high = mid - 1;
            }
            else
            {
                low = mid + 1;
            }
        }

        //最后low指向的位置就是元素应该插入的位置
        for (int j = i - 1; j >= low; j--)
        {
            arr[j + 1] = arr[j];
            (*move_count)++;
        }
        arr[low] = key;
        (*move_count)++;
    }
}

//二分插入排序
void Two_way_insert_sort(int arr[], int n, long* compare_count, long* move_count) 
{
    int* temp = (int*)malloc(n * sizeof(int));   //创建辅助数组
    int first = 0, final = 0;                    //初始化头尾指针
    temp[0] = arr[0];                            //初始化辅助数组第一个元素
    (*move_count)++;                             //第一次移动计数

    for (int i = 1; i < n; i++) 
    {
        if (arr[i] < temp[first]) 
        { //插入到辅助数组头部
            first = (first - 1 + n) % n;
            temp[first] = arr[i];
            (*move_count)++;
        }
        else if (arr[i] > temp[final]) 
        { //插入到辅助数组尾部
            final = (final + 1) % n;
            temp[final] = arr[i];
            (*move_count)++;
        }
        else 
        { //插入到中间，需移动部分元素
            int j = final; //从尾部开始查找插入位置
            while (arr[i] < temp[j]) {
                (*compare_count)++;
                temp[(j + 1) % n] = temp[j];
                j = (j - 1 + n) % n;
                (*move_count)++;
            }
            temp[(j + 1) % n] = arr[i];
            (*move_count)++;
            final = (final + 1) % n; //更新尾部指针
        }
        (*compare_count)++; //每次插入都会有一次比较计数
    }

    // 将辅助数组按顺序拷回原数组
    for (int i = 0; i < n; i++) 
    {
        arr[i] = temp[(first + i) % n];
        (*move_count)++;
    }

    free(temp); //释放辅助数组
}

//希尔排序
void Shell_sort(int arr[], int n, long* compare_count, long* move_count) 
{
    for (int d = n / 2; d > 0; d /= 2) 
    {
        // 对每一组进行插入排序
        for (int i = d; i < n; i++) 
        {
            int key = arr[i];  // 待插入的元素
            int j = i - d;

            // 对每一组进行插入排序，直到找到合适的位置
            while (j >= 0) 
            {
                (*compare_count)++;  //每次比较一次
                if (arr[j] > key) 
                {
                    arr[j + d] = arr[j];   //后移元素
                    (*move_count)++;
                    j -= d;
                }
                else 
                {
                    break;  // 如果不需要移动，提前退出
                }
            }
            arr[j + d] = key;
            (*move_count)++;
        }
    }
}
//交换函数
void Swap(int* a, int* b)
{
    int temp = *a;
    *a = *b;
    *b = temp;
}

//冒泡排序
void Bubble_sort(int arr[], int n, long* compare_count, long* move_count)
{
    for (int i = 0; i < n - 1; i++)//遍历次数
    {
        for (int j = 0; j < n - 1 - i; j++)//每次遍历都将最大的数放到最后
        {
            (*compare_count)++; // 每次比较一次
            if (arr[j] > arr[j + 1])
            {
                Swap(&arr[j], &arr[j + 1]);
                (*move_count) += 3;  //每次交换后都移动3位
            }
        }
    }
}


//快速排序
//分区操作
int partition(int arr[], int left, int right, long* compare_count, long* move_count)
{
    // 随机选择一个基准元素
    int pivotIndex = left + rand() % (right - left + 1);
    Swap(&arr[left], &arr[pivotIndex]);  // 将基准元素移到最左边
    int pivot = arr[left];//基准
    int i = left;
    int j = right;

    //结束条件是i == j
    while (i < j)
    {
        while (j > i && arr[j] >= pivot)
        {
            (*compare_count)++;
            j--;  //从右向左遍历，直到找到一个小于pivot的数
        }
        if (i < j)
        {
            arr[i] = arr[j];
            (*move_count) += 3;
            i++;
        }
        //寻找一次小数后寻找大数
        while (i < j && arr[i] <= pivot)
        {
            (*compare_count)++;
            i++;
        }
        if (i < j)
        {
            arr[j] = arr[i];
            (*move_count) += 3;
            j--;
        }
    }
    //循环结束后i == j的位置就是pivot的位置
    arr[i] = pivot;
    (*move_count) += 3;
    return i;
}
//递归快排
void Quick_sort(int arr[], int left, int right, long* compare_count, long* move_count)
{
    int i;
    if (left < right)
    {
        //基准元素的索引
        i = partition(arr, left, right, compare_count, move_count);
        //左递归
        Quick_sort(arr, left, i - 1, compare_count, move_count);
        //右递归
        Quick_sort(arr, i + 1, right, compare_count, move_count);
    }
}


//简单选择排序
void Select_sort(int arr[], int n, long* compare_count, long* move_count)
{
    for (int i = 0; i < n - 1; i++)  //n-1轮排序
    {
        int k = i;
        for (int j = i + 1; j < n; j++)//j进行遍历后续的无序数组，找出最小值
        {
            (*compare_count)++;
            if (arr[j] < arr[k])
            {
                k = j;//用k记录最小值下标
            }
        }
        if (k != i)
        {
            Swap(&arr[i], &arr[k]);
            (*move_count) += 3;
        }
    }
}

//堆排序
//调整算法
void Heapify(int arr[], int n, int root, long* compare_count, long* move_count)
{
    int largest = root;
    //这里需要考虑数组下标为0
    int lchild = root * 2 + 1;
    int rchild = root * 2 + 2;
    //若左孩子存在且大于当前最大值
    if (lchild < n && arr[lchild] > arr[largest])
    {
        (*compare_count)++;
        largest = lchild;
    }
    //同理，如果右孩子存在且大于当前最大值
    if (rchild < n && arr[rchild] > arr[largest])
    {
        (*compare_count)++;
        largest = rchild;
    }
    //此时已经找到了最大值下标
    //如果最大值发生变化，交换并递归调整
    if (largest != root)
    {
        Swap(&arr[root], &arr[largest]);
        (*move_count) += 3;
        Heapify(arr, n, largest, compare_count, move_count);
    }
}
//真正堆排
void Heap_sort(int arr[], int n, long* compare_count, long* move_count)
{
    //建堆 时间复杂度O(n)
    //直接从非叶子节点进行向下调整算法建堆
    for (int i = n / 2 - 1; i >= 0; i--)
    {
        Heapify(arr, n, i, compare_count, move_count);
    }
    //排序 O(nlogn)
    //从堆中逐个取出元素并调整堆
    for (int i = n - 1; i > 0; i--)  //n-1轮
    {
        Swap(&arr[i], &arr[0]);//堆顶元素和堆底元素交换
        (*move_count) += 3;
        Heapify(arr, i, 0, compare_count, move_count);  //调整剩余部分
    }
}


//归并排序
//合并两个有序子数组
void Merge(int arr[], int left, int mid, int right, long* compare_count, long* move_count)
{
    int i = left, j = mid + 1, k = 0;
    //i和j分别作为1，2段下标，k作为tmp_arr下标
    int* tmp_arr;
    tmp_arr = (int*)malloc((right - left + 1) * sizeof(int));
    while (i <= mid && j <= right) //左右都没有扫描结束
    {
        (*compare_count)++;
        if (arr[i] < arr[j])  //谁小谁进入新数组
        {
            (*move_count) ++;
            arr[k] = arr[i];
            k++;
            i++;
        }
        else
        {
            (*move_count) ++;
            arr[k] = arr[j];
            k++;
            j++;
        }
    }
    //有一方扫描结束，那么就需要把另一组中的剩余元素添加到新数组中
    while (i <= mid)
    {
        (*move_count) ++;
        arr[k] = arr[i];
        k++;
        i++;
    }
    while (j <= right)
    {
        (*move_count) ++;
        arr[k] = arr[j];
        k++;
        j++;
    }
    //将tmp数组复制到原数组中
    for (k = 0, i = left; i <= right; k++, i++)
    {
        (*move_count) ++;
        arr[i] = tmp_arr[k];
    }
    free(tmp_arr);
}
//对整个排序序列进行一次归并
void MergePass(int arr[], int length, int n, long* compare_count, long* move_count)
{
    int i = 0;
    while (i + 2 * length - 1 < n) // 每次归并两个完整的子序列
    {
        Merge(arr, i, i + length - 1, i + 2 * length - 1, compare_count, move_count); //归并length长的相邻两子表
        i += 2 * length; // 跳到下一个待归并的位置
    }
    // 如果剩余部分无法完整分为两部分，进行最后的归并
    if (i + length - 1 < n - 1)  //剩余两个子表，后者长度不足length
    {
        Merge(arr, i, i + length - 1, n - 1, compare_count, move_count);  //归并这两个子表
    }
}
//二路归并排序
void Merge_sort(int arr[], int n, long* compare_count, long* move_count)
{
    for (int length = 1; length < n; length *= 2)
    {
        MergePass(arr, length, n, compare_count, move_count);
    }
}

//基数排序
//确定数组中的最大数
//用来确定位数
int get_Max(int arr[], int n, long* compare_count, long* move_count)
{
    int max = arr[0];
    for (int i = 1; i < n; i++)
    {
        (*compare_count)++;
        if (arr[i] > max)
        {
            max = arr[i];
        }
    }
    return max;
}
//依次对每一位进行排序
void count_sort(int arr[], int n, int exp, long* compare_count, long* move_count)
{
    //临时存储一次排序后的数据
    int* output = (int*)malloc(n * sizeof(int));
    int count[MAXR] = { 0 }; //基数的数组
    //统计每个数字的出现次数
    for (int i = 0; i < n; i++)
    {
        int digit = (arr[i] / exp) % MAXR;  //获取数字当前比较位次的数
        count[digit]++;
        (*compare_count)++;   //数组中的每个元素都需要计算digit，这算一次操作
    }
    //累次相加基数数组，确定每个数字在排序后应该存储的位置
    for (int i = 1; i < MAXR; i++)
    {
        count[i] += count[i - 1];
        (*compare_count)++;     //每次累加也算一次操作
    }
    //从后往前扫描，保证先进入数组中的数在排序的前面
    for (int i = n - 1; i >= 0; i--)
    {
        int digit = (arr[i] / exp) % MAXR;
        output[(count[digit] - 1)] = arr[i];   //根据计数结果确定位置
        count[digit]--;
        (*move_count)++;     //每次将元素放到输出数组中算一次移动
    }
    //将排序后的结果复制到原数组中
    for (int i = 0; i < n; i++)
    {
        arr[i] = output[i];
        (*move_count)++;    //从临时数组复制回原数组也算一次移动
    }
    free(output);
}
//基数排序
void Radix_sort(int arr[], int n, long* compare_count, long* move_count)
{
    int max = get_Max(arr, n, compare_count, move_count);
    (*compare_count)++;
    //依次循环比较
    for (int exp = 1; max / exp > 0; exp *= 10)
    {
        (*compare_count)++;   //每次进入循环前算一次比较
        count_sort(arr, n, exp, compare_count, move_count);
    }
}