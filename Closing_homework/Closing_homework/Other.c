#include"Other.h"

//生成随机数
void random_array(int arr[], int n, int seed)
{
	srand(seed);
	for (int i = 0; i < n; i++)
	{
		arr[i] = rand() % 100000;
		//生成随机数[0, 99999]
	}
}

//输出结果
void print_results(const char* name, long compare_count, long move_count, double times)
{
	printf("%-20s: Comparisons = %-10ld, Moves = %-10ld, Times = %-10.6lf\n", name, compare_count, move_count, times);
}