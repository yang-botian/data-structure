#include<iostream>
using namespace std;
#define OK 1
#define ERROR 0
#define OVERFLOW -2
typedef int Status;

#define MAXSIZE 100  //顺序表的存储范围
#define STACK_INCREMENT 2  //存储空间分配增量

//这里假定SElemType是int类型
typedef int SElemType;

typedef struct
{
	SElemType* base;   //栈底指针
	SElemType* top;    //栈顶指针
	int stacksize;     //栈可用的最大容量
}SqStack;

//SqStack S;  //声明该栈

//初始化栈
Status InitStack(SqStack& S)
{
	S.base = new SElemType[MAXSIZE];
	if (!S.base)
		exit(OVERFLOW);
	S.top = S.base;  //top初始化为base，空栈
	S.stacksize = MAXSIZE;  //stacksize的最大容量为MAXSIZE
	return OK;
}

//销毁栈
Status DestroyStack(SqStack& S)
{
	free(S.base);
	S.base = NULL;
	S.top = NULL;  //规范指针
	S.stacksize = 0;
	return OK;
}

//清除栈
Status CleanStack(SqStack& S)
{
	S.top = S.base; //让top指针指回栈底即可
	return OK;
}

//判空
Status StackEmpty(SqStack S)
{
	if (S.top == S.base) //如果栈顶和栈底指向同一个位置则证明栈为NULL
		return OK;
	else
		return ERROR;
}

//获取栈内元素数量
int StackLength(SqStack S)
{
	return S.top - S.base;
}

//获取栈顶元素
SElemType GetTop(SqStack S)
{
	//判断栈是否为空
	if (!StackEmpty(S))//不为空即可获取元素
	{
		return *(--S.top);
	}
	else
	{
		return ERROR;
	}
}

//入栈
Status Push(SqStack& S, SElemType e)
{
	//插入元素为新的栈顶元素
	if (S.top - S.base == S.stacksize)//满栈
	{
		S.base = (SElemType*)realloc(S.base, (S.stacksize + STACK_INCREMENT) * sizeof(SElemType));
		//判断是否扩容成功
		if (!S.base)
			exit(OVERFLOW);//如果扩容失败，退出程序
		S.top = S.base + S.stacksize;
	}
	*(S.top)++ = e;  //这里是先对S.top解引用后存入数据e，随后将top指针向后移动一位
}

//出栈
Status Pop(SqStack& S, SElemType &e)
{
	//删除栈顶元素，并返回其值
	if (!StackEmpty(S)) //栈不为空进行操作
	{
		e = *(--S.top);
		return OK;
	}
	else
		return ERROR;
}

// 定义一个函数visit，用于打印元素
void visit(SElemType e)
{
	std::cout << e << " ";
}

// 定义一个函数用于遍历栈中的元素并对每个元素执行visit函数
void StackTraverse(SqStack S, void(*visit)(SElemType)) 
{
	SElemType* p = S.base;
	while (S.top > p) //p指向栈元素
		visit(*p++); //对该栈调用visit()，p指针上移一个存储单元
	printf("\n");
}

int main() {
	int j;
	SqStack s;
	SElemType e;
	InitStack(s);
	for (j = 1; j <= 12; j++)
		Push(s, j);
	printf("栈中元素依次为\n");
	StackTraverse(s, visit);
	Pop(s, e);
	printf("弹出的栈顶元素e = %d\n", e);
	printf("栈空否? %d (1:空 0:否)\n", StackEmpty(s));
	e = GetTop(s);
	printf("栈顶元素e = %d,栈的长度为%d\n", e, StackLength(s));
	CleanStack(s);
	printf("清空栈后，栈空否? %d (1:空 0:否)\n", StackEmpty(s));
	DestroyStack(s);
	printf("销毁栈后，s.top = %u,s.base = %u,s.stacksize = %d\n", s.top, s.base, s.stacksize);
}